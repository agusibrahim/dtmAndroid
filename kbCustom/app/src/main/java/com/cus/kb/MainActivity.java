package com.cus.kb;

import android.app.*;
import android.os.*;
import android.inputmethodservice.Keyboard;
import android.widget.*;
import android.media.*;
import android.view.ViewGroup.*;
import android.view.*;

public class MainActivity extends Activity 
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		EditText ed=(EditText) findViewById(R.id.txt22);
		ed.requestFocus();
		
		final KbView Kbv=(KbView) findViewById(R.id.keyboard_view);
		final MyKey kb = new MyKey(this, R.xml.kbox2);
		final LinearLayout kbl=(LinearLayout) findViewById(R.id.kbl);
		MyKey kbnum = new MyKey(this, R.xml.keyboard);
		Kbv.setKeyboard(kb);
		Kbv.setPreviewEnabled(false);
		Kbv.setlistener();
		Kbv.setActivity(this);
		SeekBar sek=(SeekBar) findViewById(R.id.seek);
		final ViewGroup.LayoutParams lp=kbl.getLayoutParams();
		sek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
				@Override
				public void onProgressChanged(SeekBar p1, int p2, boolean p3) {
					/*kb.setTinggi(kb.getHeight()+(p2*10));
					Kbv.setKeyboard(kb);*/
					setTitle(""+p2);
					lp.height=(p2*10);
					Kbv.minn=p2;
					
					kbl.setLayoutParams(lp);
					//kbl.refreshDrawableState();
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar p1) {
					
				}

				@Override
				public void onStopTrackingTouch(SeekBar p1) {
					// TODO: Implement this method
				}
			});
    }
}
