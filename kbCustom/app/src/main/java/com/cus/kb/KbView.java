package com.cus.kb;

import android.inputmethodservice.*;
import android.content.*;
import android.util.*;
import android.graphics.*;
import java.util.*;

import android.inputmethodservice.Keyboard.Key;
import android.graphics.drawable.*;
import android.inputmethodservice.KeyboardView.*;
import android.view.*;
import android.app.*;
import android.media.*;
import java.io.*;
import android.widget.*;

public class KbView extends KeyboardView
{
	private Context mContext;
    private int heightPixels;
    private float density;
    private static Keyboard mKeyBoard;
	private Keyboard kb;
	public Canvas mets;
	public Key metskey;
	Activity act;
	boolean sif=false;
	List<Integer> numerik=new ArrayList<Integer>();
	public static int minn=0;
	
	private SoundPool sp;

	private int music;
    public KbView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        heightPixels = 100;//mContext.getResources().getDisplayMetrics().heightPixels;
        density = mContext.getResources().getDisplayMetrics().density;
    }

    public KbView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
        heightPixels = 100;//mContext.getResources().getDisplayMetrics().heightPixels;
        density = mContext.getResources().getDisplayMetrics().density;
    }

	
	public void setlistener(){
		
		setOnKeyboardActionListener(new myListeneer());
	}
	
	@Override
	public void setKeyboard(Keyboard keyboard) {
		kb=keyboard;
		
		for(int i=7;i<17;i++){
			numerik.add(i);
		}
		
		super.setKeyboard(keyboard);
	}
	public void setActivity(Activity aa){
		act=aa;
		sp= new SoundPool(10, AudioManager.STREAM_SYSTEM, 5);
		music = sp.load(act, R.raw.keypress_standard, 1);
	}
	@Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
       // mKeyBoard = KeyboardUtil.getKeyBoardType();
        List<Key> keys = kb.getKeys();

        for (Key key : keys) {
            // 数字键盘的处理
            drawABCSpecialKey(key, canvas);
        }
    }
	private void drawABCSpecialKey(Key key, Canvas canvas) {
        //TODO 待添加特殊处理
		
		
        if (key.codes[0] == 67) {
            drawKeyBackgroundd(R.drawable.btn_yellow, canvas, key);
            drawText(canvas, key);
        }
		else if (key.codes[0] == 140) {
            drawKeyBackgroundd(R.drawable.btn_yellow, canvas, key);
            drawText(canvas, key);
        }
		else if (key.codes[0] == 100) {
            drawKeyBackgroundd(R.drawable.btn_yellow, canvas, key);
            drawText(canvas, key);
        }
		else if (key.codes[0] == 69) {
            drawKeyBackgroundd(R.drawable.btn_yellow, canvas, key);
            drawText(canvas, key);
        }
		else if (key.codes[0] == 600||key.codes[0] == 160) {
            //drawKeyBackground(R.drawable.upkey_selector, canvas, key);
            //drawText(canvas, key);
        }
        else if (key.codes[0] == 66||key.codes[0] == 621) {
            //drawKeyBackground(R.drawable.btn_keyboard_key_123, canvas, key);
            drawText(canvas, key);
        }
        else if (key.codes[0] == 62) {
            drawKeyBackgroundd(R.drawable.btn_yellow, canvas, key);
			drawText(canvas, key);
        }
		else if(numerik.indexOf(key.codes[0])>=0){
			drawKeyBackgroundd(R.drawable.btn_123, canvas, key);
			drawText(canvas, key);
		}
		else{
			drawKeyBackgroundd(R.drawable.btn_abc, canvas, key);
			drawText(canvas, key);
		}
		//drawText(canvas, key);

    }
	public void drawKeyBackgroundd(int drawableId, Canvas canvas, Key key) {
		Drawable npd;
		try{
			 npd =  (NinePatchDrawable) mContext.getResources().getDrawable(
				drawableId);
		}catch(Exception e){ 
			npd =   mContext.getResources().getDrawable(
				drawableId);
		}
	
       int[] drawableState = key.getCurrentDrawableState();
        if (key.codes[0] != 0) {
            npd.setState(drawableState);
        }
		Rect npdBounds = new Rect(key.x, key.y, key.x + key.width, key.y
								  + key.height);
        npd.setBounds(npdBounds);
        npd.draw(canvas);
    }
	private void drawText(Canvas canvas, Key key) {
        Rect bounds = new Rect();
        Paint paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(30);
		paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setAntiAlias(true);
       // paint.setColor(Color.BLUE);
        if (key.label != null) {
			paint.setColor(mContext.getResources().getColor(android.R.color.black));
			paint.getTextBounds(key.label.toString(), 0, key.label.toString()
								.length(), bounds);
			canvas.drawText(key.label.toString(), key.x + (key.width / 2),
							(key.y + key.height / 2) + bounds.height() / 2, paint);
		}
    }

	public class myListeneer implements OnKeyboardActionListener
	{
		
		@Override
		public void onPress(int p1) {
			// TODO: Implement this method
		}

		@Override
		public void onRelease(int p1) {
			// TODO: Implement this method
		}

		@Override
		public void onKey(int primaryCode, int[] keyCodes) {
			long eventTime = System.currentTimeMillis();
			KeyEvent event = new KeyEvent(eventTime, eventTime,
										  KeyEvent.ACTION_DOWN, primaryCode, 0, 0, 0, 0,
										  KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE);
			//int vibr=MainFragment.pref.getInt("vibrator", 50);
			//if(vibr>0) MainFragment.vib.vibrate(vibr);
			if(primaryCode==600){
				Keyboard kbo = new Keyboard(act, R.xml.keyboard);
				setKeyboard(kbo);
			}else if(primaryCode==160){
				Keyboard kbo = new Keyboard(act, R.xml.kb3);
				setKeyboard(kbo);
			}
			sp.play(music, 1, 1, 0, 0, 1);   
			performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
			//audiom.playSoundEffect(AudioManager.FX_KEY_CLICK, 100);
			act.setTitle(""+primaryCode);
			if(primaryCode==100){
				Toast.makeText(act, "Ditambahkan", 0).show();
				return;
			}
			if(primaryCode==140||primaryCode==120){
				Toast.makeText(act, "Belum fungsi om", 0).show();
				return;
			}
			act.dispatchKeyEvent(event);
		}

		@Override
		public void onText(CharSequence p1) {
			// TODO: Implement this method
		}

		@Override
		public void swipeLeft() {
			// TODO: Implement this method
		}

		@Override
		public void swipeRight() {
			// TODO: Implement this method
		}

		@Override
		public void swipeDown() {
			setVisibility(View.GONE);
		}

		@Override
		public void swipeUp() {
			// TODO: Implement this method
		}

	}
}
