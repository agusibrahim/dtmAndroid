package dtm.id.login.Activity;
import android.support.v7.app.*;
import android.os.*;
import dtm.id.login.R;
import android.content.*;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import android.net.*;
import java.io.*;
import dtm.id.login.*;
import android.support.annotation.*;
import com.google.firebase.auth.*;
import android.view.*;
import android.widget.*;
import java.util.ArrayList;
import java.util.*;

public class UploadActivity extends AppCompatActivity
{
	private StorageReference folderRef;
	private String furl="";
	private UploadTask mUploadTask;
	String cid;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.blank);
		StorageReference storageRef = FirebaseStorage.getInstance().getReference();
		FirebaseAuth auth=FirebaseAuth.getInstance();
		cid=auth.getCurrentUser().getUid();
		folderRef = storageRef.child("user_upload").child(auth.getCurrentUser().getEmail());
		try{
			String ii=getIntent().getStringExtra("path");
			String ix=getIntent().getStringExtra("db");
			if(ii==null) ii="";
			if(ix==null) ix="";
			File f=new File(ii);
			File ff=new File(ix);
			if(f.exists()&&f.isFile()){
				uploadFromFile(f.getPath(), false);
			}
			if(ff.exists()&&ff.isFile()){
				folderRef = storageRef.child("user_backup");
				uploadFromFile(ff.getPath(), true);
			}
		}catch(NullPointerException e){
			
		}
	}
	
	private void uploadFromFile(String path , final boolean isDb) {
		Uri file = Uri.fromFile(new File(path));
		StorageReference imageRef = folderRef.child(cid+"-"+new Date().getTime());
		if(!isDb){
			imageRef = folderRef.child(file.getLastPathSegment());
			//StorageMetadata metada
		}
		//StorageMetadata metadata = new StorageMetadata.Builder().setContentType("image/jpg").build();
		//UploadTask uploadTask = imageRef.putFile(file, metadata);
		mUploadTask = imageRef.putFile(file);

		Helper.initProgressDialog(this);
		Helper.mProgressDialog.setCancelable(false);
		/*AlertDialog.Builder d=new AlertDialog.Builder(this);
		d.setTitle("Upload Result");
		d.setOnCancelListener(new DialogInterface.OnCancelListener(){
				@Override
				public void onCancel(DialogInterface p1) {
					finish();
				}
			});
		d.setPositiveButton("Copy URL", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					setClipboard(furl);
				}
			});
		d.setNeutralButton("Bagikan", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					Intent i = new Intent(android.content.Intent.ACTION_SEND);
					i.setType("text/plain");  
					i.putExtra(android.content.Intent.EXTRA_TEXT, furl);
					startActivity(i);
				}
			});
		d.setNegativeButton("Lihat", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(furl));
					startActivity(i);
				}
			});
		final AlertDialog dlg=d.create();*/
		/*Button lihatbtn=dlg.getButton(DialogInterface.BUTTON_NEGATIVE);
		Button sharebtn=dlg.getButton(DialogInterface.BUTTON_NEUTRAL);
		 lihatbtn.setVisibility(View.GONE);https://firebasestorage.googleapis.com/v0/b/dtmdev-5722b.appspot.com/o/user_upload%2Fwakil%40mail.com%2F20161105_230304.jpg?alt=media&token=230d579c-4ff9-4175-8e6b-83b6ee628c0e
		sharebtn.setVisibility(View.GONE);*/
		Helper.mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					mUploadTask.cancel();
					finish();
				}
			});
		Helper.mProgressDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Pause", (DialogInterface.OnClickListener) null);
		/*dlg.setOnCancelListener(new DialogInterface.OnCancelListener(){
				@Override
				public void onCancel(DialogInterface p1) {
					finish();
				}
			});
		dlg.setOnDismissListener(new DialogInterface.OnDismissListener(){
				@Override
				public void onDismiss(DialogInterface p1) {
					finish();
				}
			});*/
		/*Helper.mProgressDialog.setOnShowListener(new DialogInterface.OnShowListener(){
				@Override
				public void onShow(DialogInterface dialog) {
					// TODO: Implement this method
				}
			});*/
		//Helper.mProgressDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "",null);
		Helper.mProgressDialog.show();
		final Button pauseBtn=Helper.mProgressDialog.getButton(DialogInterface.BUTTON_NEUTRAL);
		pauseBtn.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					if(mUploadTask.isPaused()){
						pauseBtn.setText("Pause");
						mUploadTask.resume();
					}else{
						pauseBtn.setText("Resume");
						mUploadTask.pause();
					}
				}
			});
		mUploadTask.addOnFailureListener(new OnFailureListener() {
				@Override
				public void onFailure(@NonNull Exception exception) {
					Helper.dismissProgressDialog();
					setResult(660);
					finish();
					//mTextView.setText(String.format("Failure: %s", exception.getMessage()));
				}
			}).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
				@Override
				public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
					Helper.dismissProgressDialog();
					Intent ten=new Intent();
					ten.putExtra("url", taskSnapshot.getDownloadUrl().toString());
					ten.putExtra("md5", taskSnapshot.getMetadata().getMd5Hash());
					ten.putExtra("name", taskSnapshot.getMetadata().getName());
					ten.putExtra("size", taskSnapshot.getTotalByteCount());
					if(!isDb) setResult(661, ten);
					else setResult(663, ten);
					//furl=taskSnapshot.getDownloadUrl().toString();
					//dlg.setMessage("Upload Sukses\n");
					//dlg.show();
					finish();
					//findViewById(R.id.button_upload_resume).setVisibility(View.GONE);
					//mTextView.setText(taskSnapshot.getDownloadUrl().toString());
				}
			}).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
				@Override
				public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
					int progress = (int) ((100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
					Helper.setProgress(progress);
				}
			}).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
				@Override
				public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
					setResult(662);
					//findViewById(R.id.button_upload_resume).setVisibility(View.VISIBLE);
					//mTextView.setText(R.string.upload_paused);
				}
			});
	}
}
