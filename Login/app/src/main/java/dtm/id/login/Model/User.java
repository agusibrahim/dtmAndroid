package dtm.id.login.Model;

import java.util.Date;
import java.text.SimpleDateFormat;
import android.content.Context;
import im.delight.android.location.SimpleLocation;

public class User
{
	public String email, readableLastLogin, lokasi, device;
	public long lastlogin;

	public User(String email, String device, SimpleLocation sl) {
		this.email=email;
		Date d=new Date();
		this.lastlogin=d.getTime();
		this.device=device;
		this.readableLastLogin=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(d);
		this.lokasi=sl.getLatitude()+" "+sl.getLongitude();
	}
}
