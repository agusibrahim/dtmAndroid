package dtm.id.login.Activity;
import android.support.v7.app.*;
import android.os.*;
import dtm.id.login.R;
import android.widget.TextView;
import android.content.Intent;
import android.net.Uri;
import com.thefinestartist.finestwebview.FinestWebView;
import android.util.*;

public class NotifMain extends AppCompatActivity
{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle b = getIntent().getExtras();
		String url = b.getString("url");
		if(url!=null){
			if(url.startsWith("www")){
				url="http://"+url;
			}else if(url.toLowerCase().matches("app:.+")){
				launchApp( url.split("app:")[1]);
				return;
			}
			if(!url.toLowerCase().startsWith("http")){
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				try{
					startActivity(browserIntent);
				}catch(Exception e){}
			}else new FinestWebView.Builder(this).show(url);
		}
		finish();
	}
	private void launchApp(String pn){
		Intent i = getPackageManager().getLaunchIntentForPackage( pn );
		try{
			startActivity(i);
		}catch(Exception e){}
		finish();
	}
}
