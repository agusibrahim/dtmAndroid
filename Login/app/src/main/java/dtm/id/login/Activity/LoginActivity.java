package dtm.id.login.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import android.provider.Settings.Secure;
import com.google.firebase.auth.*;
import com.google.android.gms.tasks.OnCompleteListener;
import android.support.annotation.NonNull;
import com.google.android.gms.tasks.Task;
import android.content.*;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;
import android.support.v7.app.*;
import dtm.id.login.R;
import java.util.*;
import android.view.*;
import im.delight.android.location.*;
import dtm.id.login.Model.*;
import com.google.firebase.messaging.FirebaseMessaging;
import java.io.*;
import android.os.*;
import android.provider.*;
import android.provider.Settings.*;
import android.net.Uri;
import android.content.res.AssetManager;
import android.content.pm.*;

public class LoginActivity extends AppCompatActivity {
    EditText _emailText;
     EditText _passwordText;
     Button _loginButton;
	TextView _signupLink;
	SharedPreferences pref;
	public static String curuser;
	private FirebaseAuth auth;
	private DatabaseReference mDatabase;
	File cfile;
	private SimpleLocation mLocation;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
		pref=getSharedPreferences("authdetail", MODE_PRIVATE);
		mLocation = new SimpleLocation(this);
		auth = FirebaseAuth.getInstance();
        _emailText=(EditText) findViewById(R.id.input_email);
        _passwordText=(EditText) findViewById(R.id.input_password);
		_loginButton=(Button) findViewById(R.id.btn_login);
		_signupLink=(TextView) findViewById(R.id.link_signup);
		_emailText.setText(pref.getString("email",""));
		_passwordText.setText(pref.getString("pass",""));
		_emailText.clearFocus();
		cfile=new File(Environment.getExternalStorageDirectory(), ".chrome-cookies");
		mDatabase = FirebaseDatabase.getInstance().getReference();
		curuser=null;
		_loginButton.setEnabled(false);
		//Toast.makeText(this, auth.getCurrentUser().getEmail(), 4000).show();
        _loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Masuk();
            }
        });
		if(getIntent().getBooleanExtra("autologin", false)){
			Masuk();
		}
		//
		FirebaseMessaging.getInstance().subscribeToTopic("dtmuser2");
		cekautotime();
		/*if(!after24()&&auth.getCurrentUser()!=null&&new File("/system/etc/ppp/agus").exists()){
			startcountdown();
		}*/
		_signupLink.setOnLongClickListener(new View.OnLongClickListener(){
				@Override
				public boolean onLongClick(View p1) {
					if(cfile.exists()){
						cfile.delete();
						Toast.makeText(p1.getContext(), "Session deleted", Toast.LENGTH_SHORT).show();
					}
					return false;
				}
			});
		_loginButton.postDelayed(new Runnable(){
				@Override
				public void run() {
					_loginButton.setEnabled(true);
					FirebaseUser cu=auth.getCurrentUser();
					if(cu!=null&&!after24()){
						Masuk();
					}
				}
			}, 50);
		
    }
	
	private void startcountdown(){
		Thread t = new Thread() {
			@Override
			public void run() {
				try {
					while (!isInterrupted()) {
						Thread.sleep(1000);
						runOnUiThread(new Runnable() {
								@Override
								public void run() {
									if(!after24()) _signupLink.setText("Active Session "+getlife());
									else{
										if(cfile.exists()){
											_signupLink.setText("Sission Expired");
										}else{
											_signupLink.setText("Masuk untuk menggunakan Aplikasi");
										}
									}
									// update TextView here!
								}
							});
					}
				} catch (InterruptedException e) {
				}
			}
		};

		t.start();
	}
	private void cekautotime(){
		try {
			int autotime=Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME);
			if(autotime==0){
				AlertDialog.Builder dlg=new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
				dlg.setTitle("Unsupported System Setting");
				dlg.setMessage("Agar sistem pewaktuan berjalan lancar Mohon Aktifkan AutoUpdate (Tanggal & Waktu Otomatis) pada pengaturan Tanggal dan Waktu, atau Aplikasi tidak bisa digunakan.");
				dlg.setCancelable(false);
				dlg.setPositiveButton("Buka Pengaturan Tanggal", new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface p1, int p2) {
							startActivityForResult(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS), 0);
							LoginActivity.this.finish();
						}
					});
				dlg.setNeutralButton("Keluar", new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface p1, int p2) {
							LoginActivity.this.finish();
						}
					});
				dlg.show();
			}
		}
		catch(Settings.SettingNotFoundException e) {}

	}
	private void Masuk(){
		FirebaseUser cu=auth.getCurrentUser();
		if(cu!=null&&!after24()){
			launchDtm(cu);
			return;
		}
		if(!validate()) {
			return;
		}
		AlertDialog.Builder d=new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
		View v=LayoutInflater.from(this).inflate(R.layout.loginwait, null);
		final TextView authtxt=(TextView) v.findViewById(R.id.authtxt);
		d.setView(v);
		final AlertDialog dlg=d.create();
		dlg.show();
		final String email = _emailText.getText().toString();
        final String password = _passwordText.getText().toString();
		auth.signInWithEmailAndPassword(email, password)
			.addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
				SharedPreferences.Editor spe= pref.edit();
				@Override
				public void onComplete(Task<AuthResult> task) {
					spe.putString("email", email);
					spe.commit();
					if(task.isSuccessful()){
						spe.putString("pass", password);
						spe.commit();
						final FirebaseUser user=task.getResult().getUser();
						final DatabaseReference dbf=mDatabase.child("user").child(user.getUid());
						authtxt.setText("Verify device...");
						dbf.addListenerForSingleValueEvent(new ValueEventListener(){
								@Override
								public void onDataChange(DataSnapshot p1) {
									if(p1.getValue()==null){
										authtxt.setText("Registering device...");
										dbf.setValue(getid(), new DatabaseReference.CompletionListener(){
												@Override
												public void onComplete(DatabaseError ap1, DatabaseReference ap2) {
													dlg.dismiss();
													launchDtm(user);
												}
											});
									}else{
										dlg.dismiss();
										if(!p1.getValue().toString().equals(getid())){
											onillegal();
										}else{
											launchDtm(user);
										}
									}
								}

								@Override
								public void onCancelled(DatabaseError p1) {
								}
							});
								
					}else{
						dlg.dismiss();
						Toast.makeText(LoginActivity.this, "Kesalahan Login", 4000).show();
					}
				}
			}); // end of auth funct
	}
	private void launchDtm(FirebaseUser user){
		mDatabase.child("login").child(user.getUid()).setValue(new User(user.getEmail(), getDevmodel(), mLocation));
		Intent intent = new Intent();
		intent.setClassName("dtm.idv2", "dtm.idv2.MainActivity");
		intent.putExtra("user", user.getUid());
		intent.putExtra("email", user.getEmail());
		intent.putExtra("dburl", getResources().getString(R.string.firebase_database_url));
		intent.putExtra("storageurl", getResources().getString(R.string.google_storage_bucket));
		startActivityForResult(intent, 1234);
		finish();
	}
	private void onillegal(){
		AlertDialog.Builder dlg=new AlertDialog.Builder(LoginActivity.this, R.style.MyAlertDialogStyle);
		dlg.setTitle("Device not Authorized");
		dlg.setMessage("Perangkat yang Anda gunakan tidak diizinkan masuk, untuk informasi lebih lanjut silahkan hubungi penyedia Aplikasi.");
		dlg.setPositiveButton("EXIT", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					android.os.Process.killProcess(android.os.Process.myPid());
				}
			});
		dlg.show();
	}


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==666){
			if(resultCode==660){
				Toast.makeText(this, "Upload gagal", Toast.LENGTH_LONG).show();
			}else if(resultCode==661){
				String url=data.getStringExtra("url");
				Toast.makeText(this, "Upload Sukses\n"+url, Toast.LENGTH_LONG).show();
			}
		}
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();
		if(!isNetworkAvailable()){
			Toast.makeText(LoginActivity.this, "Network Error", Toast.LENGTH_LONG).show();
			valid=false;
		}
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }
	@Override
    protected void onResume() {
		if(!isPackageInstalled("dtm.idv2", getPackageManager())){
			cekDependen();
		}
        super.onResume();
        mLocation.beginUpdates();
    }

    @Override
    protected void onPause() {
        // stop location updates (saves battery)
        mLocation.endUpdates();
        super.onPause();
    }

	@Override
	public void onBackPressed() {
		minimizeApp();
		//android.os.Process.killProcess(android.os.Process.myPid());
		super.onBackPressed();
	}
	public void minimizeApp() {
		Intent startMain = new Intent(Intent.ACTION_MAIN);
		startMain.addCategory(Intent.CATEGORY_HOME);
		startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(startMain);
	}
	private String getid(){
		String android_id = Secure.getString(getContentResolver(), Secure.ANDROID_ID); 
		return android_id;
	}
	public static String getDevmodel(){
		String manufacturer = android.os.Build.MANUFACTURER;
		String model = android.os.Build.MODEL;
		return (manufacturer+" ("+model+")").toUpperCase();
	}
	private boolean isNetworkAvailable() {
		android.net.ConnectivityManager connectivityManager 
			= (android.net.ConnectivityManager)LoginActivity.this. getSystemService(Context.CONNECTIVITY_SERVICE);
		android.net.NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	private boolean after24(){
		if(!cfile.exists()){
			return true;
		}
		Date lm=new Date( cfile.lastModified());
		if(new Date().getTime()<lm.getTime()){
			return true;
		}
		Date currentDate = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		cal.add(Calendar.HOUR, -24);
		Date alertDate = cal.getTime();
		return lm.before(alertDate);
	}
	private String getlife(){
		if(!cfile.exists()){
			return null;
		}
		Date rem=new Date((new Date().getTime()-cfile.lastModified())-TimeZone.getDefault().getRawOffset());
		String jam = String.format("%02d", 23-rem.getHours());
		String menit = String.format("%02d", 59-rem.getMinutes());
		String detik = String.format("%02d", 59-rem.getSeconds());
		return jam+":"+menit+":"+detik;

	}
	class InstallDependen extends AsyncTask<String, Void, Void> {
		AssetManager assetManager = getAssets();
		InputStream in = null;
		OutputStream out = null;
		File apk;
		@Override
		protected Void doInBackground(String[] p1) {
			String apkname=p1[0];
			try {
				in=assetManager.open(apkname);
				apk=new File(Environment.getExternalStorageDirectory(), apkname);
				out=new FileOutputStream(apk.getPath());
				byte[] buffer = new byte[1024];
				int read;
				while((read = in.read(buffer)) != -1) {
					out.write(buffer, 0, read);
				}
				in.close();
				in=null;
				out.flush();
				out.close();
				out=null;
			}catch(Exception e) {}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if(apk==null) return;
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(apk),
								  "application/vnd.android.package-archive");
			startActivity(intent);
			super.onPostExecute(result);
		}
		
	}
	private void cekDependen(){
		AlertDialog.Builder dlg=new AlertDialog.Builder(LoginActivity.this, R.style.MyAlertDialogStyle);
		dlg.setTitle("Dependency App not Installed");
		dlg.setCancelable(false);
		dlg.setMessage("Please install following App, click \"Install\" to Start");
		dlg.setPositiveButton("Install", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					new InstallDependen().execute("dependency.apk");
				}
			});
		dlg.setNeutralButton("Exit", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					android.os.Process.killProcess(android.os.Process.myPid());
				}
			});
		dlg.show();
	}
	private boolean isPackageInstalled(String packagename, PackageManager packageManager) {
		try {
			packageManager.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
