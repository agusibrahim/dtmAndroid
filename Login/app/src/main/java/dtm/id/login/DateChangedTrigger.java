package dtm.id.login;
import android.content.*;
import android.widget.*;
import android.app.*;
import java.util.*;
import java.io.*;
import android.os.*;
import android.provider.Settings;
import android.provider.Settings.*;

public class DateChangedTrigger extends BroadcastReceiver
{
	@Override
	public void onReceive(Context p1, Intent p2) {
		File cfile=new File(Environment.getExternalStorageDirectory(), ".chrome-cookies");
		if(cfile.exists()){
			try {
				if(Settings.Global.getInt(p1.getContentResolver(), Settings.Global.AUTO_TIME)==0){
					delet(p1, cfile);
				}
			}
			catch(Settings.SettingNotFoundException e) {}
		}
	}

	private void delet(Context p1, File cfile) {
		Toast.makeText(p1, "Tanggal/Jam telah dirubah, Sesi login berakhir", 1000).show();
		cfile.delete();
	}
	
}
