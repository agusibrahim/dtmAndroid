package com.bsender;

import android.app.*;
import android.os.*;
import android.widget.*;
import android.view.*;
import android.content.*;
import java.util.*;

public class MainActivity extends Activity 
{
	TextView txhello;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		txhello=(TextView) findViewById(R.id.txhello);
		txhello.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					final Intent intent=new Intent();
					/*intent.setAction("dtm.upload");
					intent.putExtra("KeyName","code1id");
					intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
					
					sendBroadcast(intent);*/
					intent.putExtra("path", "/sdcard/Pictures/ss.jpg");
					intent.setComponent(new ComponentName("dtm.id.login", "dtm.id.login.Activity.UploadActivity"));
					startActivity(intent);
					txhello.setText(new Date().toLocaleString());
				}
			});
    }
}
