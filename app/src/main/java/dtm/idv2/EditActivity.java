package dtm.idv2;
import android.support.v7.app.*;
import android.os.*;
import android.content.*;
import dtm.idv2.Model.*;
import android.view.*;
import android.widget.*;
import dtm.idv2.Fragment.*;
import android.text.*;
import java.util.regex.*;
import android.content.SharedPreferences.*;
import dtm.idv2.R;
import java.util.*;
import java.text.*;
import android.text.style.*;
import android.graphics.*;

public class EditActivity extends AppCompatActivity
{
	EditText ff_nopol, ff_mobil, ff_lesing, ff_ovd, ff_saldo, ff_cabang, ff_nama, ff_noka, ff_nosin;
	// 1005TFC ~ Livina+Adrw01-5]Rp87) 1000FA ~ Odissey+Otow01[2293) ~Rp208)  10 ~ Avanza+Kbgn6-4  1001BFO ~ Avznza+Kbgn6-9)
	Button actsimpan;
	Button acthapus;
	EditText fvalue;
	int editmode=0;
	int MODE_UPDATE=1;
	int MODE_ADD=0;
	int MODE_PREADD=2;

	private MenuItem sharemenu;

	private Unit m;

	//private String[] lesings;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO: Implement this method
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_form);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		ff_nopol=(EditText) findViewById(R.id.frm_nopol);
		ff_mobil=(EditText) findViewById(R.id.frm_mobil);
		ff_lesing=(EditText) findViewById(R.id.frm_lesing);
		ff_ovd=(EditText) findViewById(R.id.frm_ovd);
		ff_saldo=(EditText) findViewById(R.id.frm_saldo);
		ff_nama=(EditText) findViewById(R.id.frm_nama);
		ff_cabang=(EditText) findViewById(R.id.frm_cabang);
		ff_noka=(EditText) findViewById(R.id.frm_noka);
		ff_nosin=(EditText) findViewById(R.id.frm_nosin);
		actsimpan=(Button) findViewById(R.id.act_simpan);
		acthapus=(Button) findViewById(R.id.act_hapus);
		//leshelp=(TextView) findViewById(R.id.lesinghelper);
		m=MainActivity.mselected;
		/*if(!MainActivity.isEditorInit){
			new AsyncTask(){
				@Override
				protected Object doInBackground(Object[] p1) {
					MainFragment.engine.getGroup2();
					return null;
				}
			}.execute();
			MainActivity.isEditorInit=true;
		}*/
		boolean nosinfocus=getIntent().getBooleanExtra("nosinfocus", false);
		//moneyEditor(ff_ovd);
		moneyEditor(ff_saldo);
		if(m==null){
			ff_nopol.setText(MainFragment.query.getText().toString());
			actsimpan.setText(R.string.add);
			actsimpan.setEnabled(false);
			editmode=MODE_ADD;
			acthapus.setVisibility(View.GONE);
			setTitle("Tambahkan Data");
		}else{
			editmode=MODE_UPDATE;
			ff_nopol.setText(m.nopol);
			ff_mobil.setText(Caps(m.mobil));
			ff_lesing.setText(Caps(m.lesing));
			ff_saldo.setText(m.saldo);
			ff_ovd.setText(m.overdue);
			ff_cabang.setText(Caps((m.cabang==null?"":m.cabang).replace("$","")));
			ff_nama.setText((m.nama==null?"":m.nama).toUpperCase());
			ff_noka.setText((m.noka==null?"":m.noka).toUpperCase());
			ff_nosin.setText((m.nosin==null?"":m.nosin).toUpperCase());
			actsimpan.setText(R.string.update);
			setTitle("Perbarui Data");
		}
		if(!nosinfocus){
			ff_nopol.requestFocus();
			ff_nopol.setSelection(ff_nopol.getText().length());
		}else{
			ff_noka.requestFocus();
			ff_noka.setSelection(ff_noka.getText().length());
		}
		
		if(ff_nopol.length()>1){
			actsimpan.setEnabled(true);
		}
		TextWatcher watcher=new TextWatcher(){
			@Override
			public void beforeTextChanged(CharSequence p1, int p2, int p3, int p4) {
				// TODO: Implement this method
			}

			@Override
			public void onTextChanged(CharSequence p1, int p2, int p3, int p4) {
				if(ff_nopol.getText().length()>1){
					actsimpan.setEnabled(true);
				}else{
					actsimpan.setEnabled(false);
				}
				/*if(fvalue.isFocused()){
					StringBuilder sb=new StringBuilder();
					for(String l:MainActivity.lesingdb){
						if(l.toLowerCase().startsWith(p1.toString().toLowerCase())&&p1.length()>0){
							sb.append(l+"\n");
						}
					}
					leshelp.setText(sb.toString());
				}else{
					leshelp.setText("");
				}*/
			}

			@Override
			public void afterTextChanged(Editable p1) {
				// TODO: Implement this method
			}
			
		};
		ff_nopol.addTextChangedListener(watcher);
		//fvalue.addTextChangedListener(watcher);
		/*
		 ff1=(EditText) findViewById(R.id.frm_nopol);
		 ff2=(EditText) findViewById(R.id.frm_mobil);
		 ff3=(EditText) findViewById(R.id.frm_lesing);
		 ff5=(EditText) findViewById(R.id.frm_ovd);
		 ff4=(EditText) findViewById(R.id.frm_saldo);
		 ff7=(EditText) findViewById(R.id.frm_nama);
		 ff6=(EditText) findViewById(R.id.frm_cabang);
		 ff8=(EditText) findViewById(R.id.frm_noka);
		 ff9=(EditText) findViewById(R.id.frm_nosin);
		*/
		actsimpan.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					String kay=ff_nopol.getText().toString();
					kay=nopolupper(kay);
					Unit dat=new Unit( ff_nopol.getText().toString().toUpperCase(), ff_mobil.getText().toString(), ff_lesing.getText().toString(),
									  ff_ovd.getText().toString(), ff_saldo.getText().toString().replaceAll("[^0-9]",""),
									  ff_cabang.getText().toString()+"$", ff_nama.getText().toString(),
									  ff_noka.getText().toString().toUpperCase(),ff_nosin.getText().toString().toUpperCase() );
					dat.mobil=Caps(dat.mobil);
					dat.cabang=Caps(dat.cabang);
					dat.nama=(dat.nama).toUpperCase();
					dat.noka=dat.noka.toUpperCase();
					dat.nosin=dat.nosin.toUpperCase();
					dat.lesing=Caps(dat.lesing);
					if(editmode==MODE_UPDATE){
						//MainFragment.dataset.set(MainFragment.dataset.indexOf(m), dat);
						MainActivity.engine.update(m, dat);
						//MainFragment.adapt.notifyItemChanged(MainFragment.dataset.indexOf(val));
						if(MainFragment.adapt!=null) MainFragment.adapt.notifyDataSetChanged();
						msg(R.string.updated);
					}else{
						
						MainActivity.engine.getExists(dat);
						//MainFragment.dataset.add(0,dat);
						//MainFragment.engine.tambah(dat);
						//MainFragment.adapt.notifyItemInserted(0);
						//MainFragment.adapt.notifyDataSetChanged();
						//msg(R.string.added);
					}
					finish();
				}
			});
		acthapus.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					if(!MainActivity.pref.getBoolean("delwithhold", true)){
						MainActivity.engine.hapus(m);
						if(MainFragment.dataset!=null) MainFragment.dataset.remove(m);
						if(MainFragment.adapt!=null) MainFragment.adapt.notifyDataSetChanged();
						msg(R.string.deleted);
						finish();
						return;
					}
					AlertDialog.Builder dlg = new AlertDialog.Builder(p1.getContext(), R.style.myDlg);
					//dlg.setTitle("Delete");
					dlg.setMessage("Yakin mau menghapus?");
					dlg.setPositiveButton("Ya", new DialogInterface.OnClickListener(){
							@Override
							public void onClick(DialogInterface p1, int p2) {
								MainActivity.engine.hapus(m);
								if(MainFragment.dataset!=null) MainFragment.dataset.remove(m);
								if(MainFragment.adapt!=null) MainFragment.adapt.notifyDataSetChanged();
								msg(R.string.deleted);
								finish();
								// Ntahlah, gw belum nemu cara meng close firebase saat exit
								// cara yg gw tau hanya mengkill semua proses saat app exit
								//android.os.Process.killProcess(android.os.Process.myPid());
							}
						});
					dlg.setNegativeButton("Tidak", null);
					dlg.show();
				}
			});
		initFontSize(0);
	}
	private void setFontSize(){
		LinearLayout.LayoutParams params;
		LinearLayout layout = new LinearLayout(this);
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.setPadding(6,6,6,6);
		final TextView mValueText = new TextView(this);
		mValueText.setGravity(Gravity.CENTER_HORIZONTAL);
		mValueText.setTextSize(32);
		params = new LinearLayout.LayoutParams(
			LinearLayout.LayoutParams.FILL_PARENT, 
			LinearLayout.LayoutParams.WRAP_CONTENT);
		layout.addView(mValueText, params);

		SeekBar mSeekBar = new SeekBar(this);
		mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
				@Override
				public void onProgressChanged(SeekBar p1, int p2, boolean p3) {
					mValueText.setText(p2+"");
					initFontSize(p2);
				}

				@Override
				public void onStartTrackingTouch(SeekBar p1) {
					// TODO: Implement this method
				}

				@Override
				public void onStopTrackingTouch(SeekBar p1) {
					// TODO: Implement this method
				}
			});
		layout.addView(mSeekBar, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		mSeekBar.setMax(100);
		mSeekBar.setProgress(MainActivity.pref.getInt("edsize", 30));
		AlertDialog.Builder dd=new AlertDialog.Builder(this);
		dd.setView(layout);
		dd.setTitle("Font Size");
		dd.setPositiveButton("Save", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					SharedPreferences.Editor ex=MainActivity.pref.edit();
					ex.putInt("edsize", Integer.parseInt( mValueText.getText().toString()));
					ex.commit();
					
				}
			});
		dd.setNegativeButton("Batal", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					initFontSize(0);
				}
			});
		dd.setOnCancelListener(new DialogInterface.OnCancelListener(){
				@Override
				public void onCancel(DialogInterface p1) {
					initFontSize(0);
				}
			});
		AlertDialog dlg=dd.create();
		dlg.setCanceledOnTouchOutside(false);
		dlg.show();
	}
	private void initFontSize(int p2) {
		if(p2==0){
			p2=MainActivity.pref.getInt("edsize", 30);
		}
		ff_nopol.setTextSize(p2);
		ff_mobil.setTextSize(p2);
		ff_lesing.setTextSize(p2);
		ff_ovd.setTextSize(p2);
		ff_saldo.setTextSize(p2);
		ff_cabang.setTextSize(p2);
		ff_nama.setTextSize(p2);
		ff_noka.setTextSize(p2);
		ff_nosin.setTextSize(p2);
	}
	
	@Override
	protected void onDestroy() {
		if(MainFragment.rview!=null&&MainFragment.adapt!=null){
			MainFragment.rview.setSelection(MainFragment.adapt.getPosition(MainActivity.mselected));
		}
		MainActivity.mselected=null;
		MainActivity.lastquery="s";
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		MainActivity.mselected=null;
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.editor_menu, menu);
		sharemenu=menu.getItem(0);
		if(editmode==MODE_ADD){
			sharemenu.setVisible(false);
		}
		return super.onCreateOptionsMenu(menu);
	}
	
	
	@Override                
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId()==android.R.id.home){
			onBackPressed();
		}
		if(item.getItemId()==R.id.sharebtn){
			String kay=ff_nopol.getText().toString()+" ~ "+ff_mobil.getText().toString()+"+"+ff_lesing.getText().toString()+"["+ff_ovd.getText().toString()+")"+((ff_saldo.getText().toString()!=null&&(""+ff_saldo.getText().toString()).length()>0&&!(""+ff_saldo.getText().toString()).equals("null"))?" +Rp"+ff_saldo.getText().toString()+")":"");
			kay=kay.trim().replaceAll("(~ \\+$|\\+$|\\+\\)$)","").trim();
			MainFragment.imm.hideSoftInputFromWindow(ff_nopol.getWindowToken(), 0);
			Intent i = new Intent(android.content.Intent.ACTION_SEND);
			i.setType("text/plain");  
			i.putExtra(android.content.Intent.EXTRA_TEXT, kay);
			startActivity(i);
		}
		if(item.getItemId()==R.id.setsizebtn){
			setFontSize();
		}
		return super.onOptionsItemSelected(item);
	}
	private static String nopolupper(String ori){
		String h=ori.split("~")[0];
		ori=ori.replace(h,h.toUpperCase());
		return ori;
	}
	private void msg(int i){
		msg_(getResources().getString(i));
	}
	
	private void msg_(String s){
		Toast t=Toast.makeText(this, s, Toast.LENGTH_SHORT);
		t.setGravity(Gravity.TOP|Gravity.CENTER,0,0);
		t.show();
	}
	private String Caps(String name){
		if(name==null) return "";
		if(name.length()<1) return name;
		name = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
		return name;
	}
	private static String _priceFormat(String s){
		double parsed = Double.parseDouble(s);
		String formatted = NumberFormat.getCurrencyInstance().format(parsed);
		return formatted;
	}
	public static String priceFormat(String s){
		return _priceFormat(s);
	};
	public static String priceFormat(long s){
		return _priceFormat(""+s);
	};
	public static String priceFormat2(String s){
		return s.replaceAll("(\\d)(?=(\\d{3})+(?!\\d))", "$1.");
	}
	public static void priceEdit(EditText et, CharSequence p1, TextWatcher watcher){
		et.removeTextChangedListener(watcher);
		String cleanString = p1.toString().replaceAll("[a-zA-Z\\.]", "").replace("$","").replace("Rp","");
		if(cleanString.length()<1){
			et.addTextChangedListener(watcher);
			return;
		}

		String formatted=priceFormat2(cleanString);
		if(formatted.startsWith("0")&&formatted.length()>1) formatted=formatted.substring(1);
		et.setText(formatted);

		//et.setText("Rp" + formatted, BufferType.SPANNABLE);
		/*Spannable s=new SpannableString(formatted);
		s.setSpan(new ForegroundColorSpan(Color.parseColor("#F9D1CB")), 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		et.setText(s);*/
		et.setSelection(formatted.length());
		et.addTextChangedListener(watcher);
	}
	private void moneyEditor(final EditText tt){
		boolean isIndo=Locale.getDefault().getLanguage().equals(new Locale("in").getLanguage());
		if(!isIndo) return;
		tt.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(CharSequence p1, int p2, int p3, int p4) {
					// TODO: Implement this method
				}

				@Override
				public void onTextChanged(CharSequence p1, int p2, int p3, int p4) {
					priceEdit(tt, p1, this);
					//setTitle(p1.toString().replaceAll("[^0-9]",""));
				}

				@Override
				public void afterTextChanged(Editable p1) {
					if(tt.getText().toString().matches(".*\\d.*")){

					}else{
						tt.setText("0");

					}
				}
			});
		//tt.setText("120000");
	}
}
