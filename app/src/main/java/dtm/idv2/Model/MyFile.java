package dtm.idv2.Model;

public class MyFile
{
	public String name, url, hash;
	public long size;

	public MyFile(String name, String url, String hash, long size) {
		this.name=name;
		this.url=url;
		this.hash=hash;
		this.size=size;
	}
}
