package dtm.idv2.Model;
import java.io.*;
import java.text.*;

public class Backupz
{
	private File file;

	public Backupz(File file) {
		this.file = file;
	}
	public String getTime(){
		String n=file.getName().split("-")[1];
		SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy hh:mm");
		String date = sdf.format(Long.parseLong(n));
		return date;
	}
	public int getDbCount(){
		String n=file.getName().split("-")[0];
		return Integer.parseInt(n);
	}
	public String getName(){
		return file.getName();
	}
	public File getFile(){
		return this.file;
	}
}
