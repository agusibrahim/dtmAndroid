package dtm.idv2.Fragment;

import android.support.v4.app.Fragment;
import android.view.*;
import android.os.*;
import dtm.idv2.*;
import android.widget.*;
import android.text.*;
import android.content.res.*;
import java.io.*;
import android.webkit.WebView;

public class HelpFragment extends Fragment
{
	//tView ketr;
	WebView web;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO: Implement this method
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		MainActivity.apptitle.setText("Bantuan");
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.frag1, container, false);
		web=(WebView) v.findViewById(R.id.webv);
		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		String[] kete=getResources().getStringArray(R.array.lesing);
		String[] kontak=getResources().getStringArray(R.array.kontak);
		String htmllesing="";
		String htmlkontak="";
		for(String s:kete){
			String[] k=s.split("=");
			String a=k[0].trim();String b=k[1].trim();
			htmllesing=htmllesing+("<font color='#e32424'>"+a+"</font> ~ <b>"+b+"</b><br>");
		}
		for(String s:kontak){
			String[] k=s.split("=");
			String a=k[0].trim();String b=k[1].trim();
			htmlkontak=htmlkontak+("<font color='#e32424'>"+a+"</font> ~ <b>"+b.replace("/","<font color='#e32424'>/</font>")+"</b><br>");
		}
		try {
			Resources res = getResources();
			InputStream in_s = res.openRawResource(R.raw.help);

			byte[] b = new byte[in_s.available()];
			in_s.read(b);
			String xx=new String(b).replace("%lesing%", htmllesing).replace("%kontak%",htmlkontak);
			web.loadData(xx, "text/html; charset=utf-8", "UTF-8");
		} catch (Exception e) {
			// e.printStackTrace();
			//ketr.setText("Error: can't show help.");
		}
		//ketr.setText(Html.fromHtml( getResources().getString(R.string.helptext).replace("\n","<br>").replace("%lesing%", htmllesing).replace("%kontak%",htmlkontak)));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId()==R.id.upmenu2){
			getActivity().finish();
			MainActivity.exit();
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.upload_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onDestroyView() {
		
		/*MainActivity.fragMgr.beginTransaction()
			.replace(R.id.konten, MainActivity.afterinfo).commit();
		MainActivity.fragclass=MainActivity.afterinfo.getClass();*/
		super.onDestroyView();
	}
	
}
