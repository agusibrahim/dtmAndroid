package dtm.idv2.Fragment;
import android.support.v4.app.Fragment;
import android.view.*;
import android.os.*;
import dtm.idv2.*;
import dtm.idv2.Adapter.*;
import dtm.idv2.Model.*;
import java.util.*;
import android.widget.*;
import android.view.inputmethod.*;
import android.content.*;
import android.content.res.*;
import android.support.design.widget.*;
import dtm.idv2.Utils.*;
import android.preference.*;
import android.app.*;
import android.text.*;
import android.inputmethodservice.*;
import android.support.v7.widget.AppCompatCheckBox;
import android.graphics.drawable.Drawable.*;
import android.graphics.drawable.*;
import android.util.*;
import android.graphics.*;
import android.view.View.*;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;

public class MainFragment extends Fragment
{
	public static ListView rview;
	public static DataAdapter adapt;
	public static List<Unit> dataset=new ArrayList<Unit>();
	public static InputMethodManager imm;
	public static KeyboardlessEditText2 query;
	public static SharedPreferences pref;
	public static Resources res;
	//private static FloatingActionButton fab;
	public static Context ctx;
	public static QueryEngine engine;
	//public static RelativeLayout notfoundview;
	public static LinearLayout notfoundview;
	public static AppCompatCheckBox methodswitch;
	public static RelativeLayout rootlayout;
	//LinearLayout bottombar;
	//public static Vibrator vib;
	
	int limit=50;
	public static LinearLayout kbl;
	private Keyboard mKeyboard;
	Button fab;

	public static KbView keyboardView;
	ImageButton clearntn;
	ImageButton addbtn;
	//public static String MainActivity.curlesing=null;

	private Menu cekMenu;
	public static MenuItem filtr;
	final boolean[] isLongPress = {false};
	final int duration = 5000;
	final Handler someHandler = new Handler();
	Runnable someCall;
	public static boolean isclear=false;
	public static String lastkw="";
	long awal=0;
	private Drawable.ConstantState hashOff;
	Button tvlesing;

	private Drawable.ConstantState hashOn;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO: Implement this metho
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		//((AppCompatActivity)getActivity()).getSupportActionBar().show();
	}
	private int getKbRes(){
		int res=0;
		int ks=Integer.parseInt( pref.getString("keysize", "2"));
		switch(ks){
			case 1:
				res=R.xml.kbox1;
				break;
			case 2:
				res=R.xml.kbox2;
				break;
			case 3:
				res=R.xml.kbox3;
				break;
			case 4:
				res=R.xml.kbox4;
				break;
			default:
				res=R.xml.kbox2;
		}
		return res;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.main, container, false);
		ctx=getActivity();
		pref=MainActivity.pref;
		res=getResources();
		limit=pref.getInt("limit", 50 );
		MainActivity.apptitle.setText(R.string.app_name);
        //keyboard = new Keyboard(getActivity(), R.xml.keyboards);
        
		imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		//bottombar=(LinearLayout) v.findViewById(R.id.bottom_bar);
		mKeyboard = new Keyboard(getActivity(), getKbRes());
		keyboardView = (KbView)v.findViewById(R.id.keyboard_view);
		if(!pref.getBoolean("khide", true)) keyboardView.setVisibility(View.GONE);
        keyboardView.setPreviewEnabled(false);
		
		keyboardView.setKeyboard(mKeyboard);
        keyboardView.setActivity(getActivity());
		rootlayout=(RelativeLayout) v.findViewById(R.id.coordinatorLayout);
		kbl=(LinearLayout) v.findViewById(R.id.kbl);
		query=(KeyboardlessEditText2) v.findViewById(R.id.inputq);
		methodswitch=(AppCompatCheckBox) v.findViewById(R.id.methodswitch);
		addbtn=(ImageButton) v.findViewById(R.id.add_btn);
		rview=(ListView) v.findViewById(R.id.rview);
		clearntn=(ImageButton) v.findViewById(R.id.clearBtn);
		//final RelativeLayout ll= (RelativeLayout) v.findViewById(R.id.lsbody);
		fab=(Button) v.findViewById(R.id.addtonew);
		notfoundview=(LinearLayout) v.findViewById(R.id.nofoundlayout);
		//dataset=new ArrayList<Matel>();//new QueryEngine(this).query("",0,limit,"false");
		engine=MainActivity.engine;//new QueryEngine(getActivity());
		//engine.populateData("",0,limit);
		//adapt=new DataAdapter(getActivity(), dataset);
		//rview.setAdapter(adapt);
		query.requestFocus();
		query.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
		query.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
		//keyboardView.setOnKeyboardActionListener(new KbListener(getActivity()));
		keyboardView.setlistener();
		if(!pref.getBoolean("khide", true)) imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
		else{imm.hideSoftInputFromWindow(query.getWindowToken(), 0);
		getActivity().getWindow().setSoftInputMode(
			WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
		);
		}
		hashOn=getResources().getDrawable(R.mipmap.checkbox_marked_circle_outline).getConstantState();
		hashOff=getResources().getDrawable(R.mipmap.checkbox_blank_circle_outline).getConstantState();
		
		return v;
	}
	public static void setTotal(String s){
		if(s==null) s=res.getString(R.string.app_name);
		else s=res.getString(R.string.app_name)+" ("+s+")";
		((Activity) ctx).setTitle(s);

	}
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		fab.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					//Matel m=new Matel(query.getText().toString().toUpperCase(),"");
					MainActivity.mselected=null;
					startActivity(new Intent(getActivity(), EditActivity.class));
					//new FormInput(p1.getContext()).show(m, false);
				}
			});
		addbtn.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					MainActivity.mselected=null;
					startActivity(new Intent(getActivity(), EditActivity.class));
				}
			});
		/*addbtn.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					int eventAction = event.getAction();
					if(eventAction == MotionEvent.ACTION_DOWN){
						isLongPress[0] = true;
						someHandler.postDelayed(someCall, duration);
					}
					else if (eventAction == MotionEvent.ACTION_UP) {
						isLongPress[0] = false;
						someHandler.removeCallbacks(someCall);
					}
					return false;
				}
			});*/
		addbtn.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(!v.isPressed()){
						awal=0;
						addbtn.setTag(false);
						return false;
					}
					if(awal==0) awal=System.currentTimeMillis();
					long eventDuratin = System.currentTimeMillis()-awal;//- event.getDownTime();
					//numr.setText(""+eventDuratin+"++"+event.getAction());
					if ( event.getAction() == 2&&MainActivity. isAdmin) {
						if (eventDuratin > duration&&(boolean)addbtn.getTag()==false) {
							//onLongClick(view);
							MainActivity.vib.vibrate(100);
							Intent ii=new Intent(getActivity(), BackupActivity.class);
							ii.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
							startActivity(ii);
							addbtn.setTag(true);
						} else {

						}
					}
					if(event.getAction()==1){
						addbtn.setTag(false);
						awal=0;
					}
					return false;
				}
			});
			
		MainActivity.curlesing.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(!v.isPressed()){
						awal=0;
						MainActivity.curlesing.setTag(false);
						return false;
					}
					if(awal==0) awal=System.currentTimeMillis();
					long eventDuratin = System.currentTimeMillis()-awal;//- event.getDownTime();
					//numr.setText(""+eventDuratin+"++"+event.getAction());
					if ( event.getAction() == 2) {
						if (eventDuratin > duration&&(boolean)MainActivity.curlesing.getTag()==false) {
							//onLongClick(view);
							MainActivity.vib.vibrate(100);
							Intent ii=new Intent(getActivity(), HelpActivity.class);
							ii.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
							startActivity(ii);
							MainActivity.curlesing.setTag(true);
						} else {

						}
					}
					if(event.getAction()==1){
						MainActivity.curlesing.setTag(false);
						awal=0;
					}
					return false;
				}
			});
		query.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					if(pref.getBoolean("khide", true)) keyboardView.setVisibility(View.VISIBLE);
				}
			});
		
		methodswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(CompoundButton p1, boolean p2) {
					//Toast.makeText(ctx, ""+p2, Toast.LENGTH_LONG).show();
					if(!p2) return;
					Toast.makeText(getActivity(), "Pencarian mode lanjut diaktifkan",0).show();
					dataset.clear();
					rview.setSelectionAfterHeaderView();
					engine.populateData(MainActivity.curlesing.getText().toString(), query.getText().toString(),0,limit);
				}
			});
		clearntn.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					lastkw=query.getText().toString();
					isclear=true;
					query.setText("");
				}
			});
		query.setOnEditorActionListener(new EditText.OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_DONE) {
						dataset.clear();
						rview.setSelectionAfterHeaderView();
						engine.populateData(MainActivity.curlesing.getText().toString(), query.getText().toString(), 0, limit);
						return true;
					}
					return false;
				}
			});
		rview.setOnScrollListener(new EndlessScrollListener() {
				@Override
				public boolean onLoadMore(int page, int totalItemsCount) {
					if(totalItemsCount<limit||page<2){
						//dataset.clear();
						//mydata.setSelectionAfterHeaderView();
						return false;
					}
					//Toast.makeText(MainActivity.this, "Load more "+((page-1)*limit)+",limit", Toast.LENGTH_LONG).show();
					if(!isclear) engine.populateData(MainActivity.curlesing.getText().toString(), query.getText().toString(), ((page-1)*limit), limit);
					else engine.populateData(MainActivity.curlesing.getText().toString(),lastkw, ((page-1)*limit), limit);
					return true; // ONLY if more data is actually being loaded; false otherwise.
				}
			});
		query.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(CharSequence p1, int p2, int p3, int p4) {
					// TODO: Implement this method
				}

				@Override
				public void onTextChanged(CharSequence p1, int p2, int p3, int p4) {
					if(p1.length()<0) return;
					
					if(p1.toString().contains("@")){
						lastkw=p1.toString();
						isclear=true;
						query.setText("");
					}else{
						if(p1.length()>0) isclear=false;
					}
					
					//if(methodswitch.isChecked()) return;
					//if(!pref.getBoolean("autosearch", true)) return;
					dataset.clear();
					rview.setSelectionAfterHeaderView();
					if(!isclear)
					engine.populateData(MainActivity.curlesing.getText().toString(), query.getText().toString(),0,limit);
					else engine.populateData(MainActivity.curlesing.getText().toString(),lastkw,0,limit);
				}

				@Override
				public void afterTextChanged(Editable p1) {
					//dataset.clear();
					//rview.setSelectionAfterHeaderView();
					//engine.populateData(p1.toString(),0,limit);
				}
			});
		rview.setOnItemClickListener(new AdapterView.OnItemClickListener(){
				@Override
				public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4) {
					//Toast.makeText(MainActivity.this, "Load"+notfoundview, Toast.LENGTH_LONG).show();
					//new FormInput(ctx).show((Matel) p1.getItemAtPosition(p3), true);
					Intent i=new Intent(getActivity(), EditActivity.class);
					/*Matel oo=(Matel) p1.getItemAtPosition(p3);
					MainActivity.mselected=oo;
					
					i.putExtra("key", oo.getKey());
					i.putExtra("value", oo.getValue());*/
					i.putExtra("nosinfocus", false);
					MainActivity.lastquery="s";//query.getText().toString();
					Unit uu=(Unit) p1.getItemAtPosition(p3);
					MainActivity.mselected=uu;
					startActivity(i);
					//notfoundview.setVisibility(View.VISIBLE);
				}
			});
	}

	@Override
	public void onResume() {
		//MainActivity.afterinfo=this;
		
		//if(MainActivity.lastquery!=null) query.setText(MainActivity.lastquery);
		MainActivity.mselected=null;
		//dataset=new ArrayList<Matel>();//new QueryEngine(this).query("",0,limit,"false");
		//engine=new QueryEngine(getActivity());
		if(MainActivity.lastquery.equals("s")){
			MainActivity.lastquery="";
			//adapt.notifyDataSetChanged();
			query.setText(query.getText().toString());
			query.setSelection(query.getText().length());
			super.onResume();
			return;
		}
		dataset.clear();
		query.setText("");
		rview.setSelectionAfterHeaderView();
		engine.populateData(MainActivity.curlesing.getText().toString(), "",0,limit);
		adapt=new DataAdapter(getActivity(), dataset);
		rview.setAdapter(adapt);
		query.requestFocus();
		if(!pref.getBoolean("khide", true)){
			keyboardView.setVisibility(View.GONE);
			imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
		}else keyboardView.setVisibility(View.VISIBLE);
		//Keyboard k=new Keyboard(getActivity(), kbv);
		//keyboardView.setKeyboard(k);
		
		super.onResume();
	}

	@Override
	public void onDestroyView() {
		MainActivity.lastquery="";
		super.onDestroyView();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
				/*case R.id.item2:
				 Intent i=new Intent(this, Settings.class);
				 startActivity(i);
				 break;*/
			case R.id.filterbtn:
				startActivity(new Intent(getActivity(), FilterActivity2.class));
				break;
			case R.id.item1:
				if(cekMenu.getItem(1).getIcon().getConstantState().equals(hashOff)) {
					cekMenu.getItem(1).setIcon(getResources().getDrawable(R.mipmap.checkbox_marked_circle_outline));
					methodswitch.setChecked(true);
				}else {
					cekMenu.getItem(1).setIcon(getResources().getDrawable(R.mipmap.checkbox_blank_circle_outline));
					methodswitch.setChecked(false);
				}
				//new FormInput(ctx).show(null, true);
				//Matel m=new Matel(query.getText().toString().toUpperCase(),"");
				//MainActivity.mselected=null;
				//startActivity(new Intent(getActivity(), EditActivity.class));
				//new FormInput(getActivity()).show(m, false);
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.main_menu, menu);
		cekMenu=menu;
		
		
		filtr=menu.getItem(0);
		//if(MainActivity.curlesing.gett) filtr.setTitle("Semua Lesing");
		//else filtr.setTitle(MainActivity.curlesing);
		cekMenu.getItem(1).setIcon(getResources().getDrawable(R.mipmap.checkbox_blank_circle_outline));
		menu.getItem(0).setActionView(MainActivity.curlesing);
		menu.getItem(0).setVisible(false);
		super.onCreateOptionsMenu(menu, inflater);
	}
}
