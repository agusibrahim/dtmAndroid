package dtm.idv2.Fragment;

import android.support.v4.app.Fragment;
import android.view.*;
import android.os.*;
import dtm.idv2.*;
import android.widget.*;
import android.text.*;
import android.content.res.*;
import java.io.*;
import android.webkit.WebView;
//import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import dtm.idv2.Utils.*;
import android.content.*;
import android.content.SharedPreferences.*;
import java.util.*;
import java.text.*;
import java.net.*;
import android.util.*;
import android.app.ProgressDialog;
import android.net.*;
import android.graphics.*;
import dtm.idv2.R;
import android.content.pm.*;
import dtm.idv2.Model.*;
import dtm.idv2.Adapter.*;
//import android.support.design.widget.*;
import java.util.zip.*;
import android.support.v4.app.*;
import android.support.design.widget.*;
import org.oucho.filepicker.FilePicker;
import org.oucho.filepicker.FilePickerParcelObject;
import android.support.v7.app.AlertDialog.*;

public class UserBackupFragment extends Fragment
{
	//tView ketr;
	
	private File dbsd;
	public final static int WHITE = 0xFFFFFFFF;
	public final static int BLACK = 0xFF000000;
	private String SYNCSERVER;
	private String bucket_url;
	private String bucket_url_base;
	private TextView notif_aja;
	private ListView mydata_lst;
	List<Backupz> datalist=new ArrayList<Backupz>();
	private Button bekap, impor;
	private String dbs_correnctname;
	private BackupAdapter adapt;

	private File bakup_loc;
	
	public static final String VERIFY_SUCCESS="verify.ok";
	public static final String VERIFY_FAILED="verify.fail";
	public static final String VERIFY_PREPARE="verify.begin";
	private BroadcastReceiver recv;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO: Implement this method
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		// https://firebasestorage.googleapis.com/v0/b/dtmdev-5722b.appspot.com/o/user_backup%2FYOttEZn39DRGeDV3GfrUhLQqKX62?alt=media
		SYNCSERVER=MainActivity.storageurl.replace("gs:","").replace("/","");
		bucket_url_base="https://firebasestorage.googleapis.com/v0/b/"+SYNCSERVER+"/o/user_backup%2F$MYFILE?alt=media";
		bucket_url="https://firebasestorage.googleapis.com/v0/b/"+SYNCSERVER+"/o/user_backup%2F"+MainActivity.uid+"?alt=media";
		dbsd=new File(Environment.getExternalStorageDirectory(), "Android/data/.datastreamagusibrahim");
		bakup_loc=new File(Environment.getExternalStorageDirectory(), "Android/data/ref"+MainActivity.uid);
		if(!bakup_loc.exists()) bakup_loc.mkdirs();
		
		MainActivity.apptitle.setText("User Backup");
		recv = new BroadcastReceiver(){
			private AlertDialog dd;
			@Override
			public void onReceive(Context p1, Intent p2) {
				int frm=p2.getIntExtra("from",0);
				if (p2.getAction().equals(VERIFY_SUCCESS) || p2.getAction().equals(VERIFY_FAILED)) {
					if(dd!=null) dd.dismiss();
					if (frm == 0) {
						AlertDialog.Builder dd=new AlertDialog.Builder(getActivity());
						dd.setTitle("Backup Berhasil");
						dd.setMessage("Data telah diamankan, kamu dapat restore kapanpun.");
						dd.show();
						toGzip(dbsd.getPath(), dbs_correnctname);
						datalist.add(new Backupz(new File(dbs_correnctname)));
						adapt.notifyDataSetChanged();
						notif_aja.setText("Data Backup Kamu:");
					} else {
						new RestoreNow(getActivity(), 0).importCsv(p2.getStringExtra("file"), "no");
					}
				}else if(p2.getAction().equals(VERIFY_PREPARE)){
					AlertDialog.Builder d=new AlertDialog.Builder(getActivity());
					d.setMessage("Memeriksa Kesalahan...");
					dd=d.create();
					dd.show();
				}
			}
		};
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.userbackup_frag, container, false);
		notif_aja=(TextView) v.findViewById(R.id.userbackupfragTextView1);
		mydata_lst=(ListView) v.findViewById(R.id.backup_list_2);
		bekap=(Button)v.findViewById(R.id.backup_btn_2);
		impor=(Button) v.findViewById(R.id.import_btn_user);
		
		adapt=new BackupAdapter(getActivity(), datalist);
		mydata_lst.setAdapter(adapt);
		IntentFilter iff=new IntentFilter();
		iff.addAction(VERIFY_SUCCESS);
		iff.addAction(VERIFY_FAILED);
		iff.addAction(VERIFY_PREPARE);
		getActivity().registerReceiver(recv, iff);
		return v;
	}

	@Override
	public void onDestroyView() {
		getActivity().unregisterReceiver(recv);
		super.onDestroyView();
	}
	
	@Override
	public void onViewCreated(final View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		for(File f:bakup_loc.listFiles()){
			Backupz bb=new Backupz(f);
			datalist.add(bb);
		}
		adapt.notifyDataSetChanged();
		if(datalist.size()<1) notif_aja.setText("Tidak Ada Backup, silahkan buat baru.");
		bekap.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					if(!isNetworkAvailable()){
						AlertDialog.Builder dlz=new AlertDialog.Builder(getActivity());
						dlz.setMessage("Backup Data memerlukan jaringan Internet. Aktifkan Data Seluler atau Sambungkan ke Wi-Fi untuk melanjutkan");
						dlz.show();
						return;
					}
					AlertDialog.Builder dl=new AlertDialog.Builder(getActivity());
					dl.setTitle("Backup Data");
					dl.setMessage("Apakah kamu yakin ingin membackup data?");
					dl.setPositiveButton("YA", new DialogInterface.OnClickListener(){
							@Override
							public void onClick(DialogInterface p1, int p2) {
								new ExportNowAndUpload().execute();
							}
						});
					dl.setNegativeButton("Batal", null);
					dl.show();

				}
			});
		impor.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					if(!isNetworkAvailable()){
						AlertDialog.Builder dlz=new AlertDialog.Builder(getActivity());
						dlz.setMessage("Import CSV memerlukan jaringan Internet. Aktifkan Data Seluler atau Sambungkan ke Wi-Fi untuk melanjutkan");
						dlz.show();
						return;
					}
					// hello
					csvChooser();
				}
			});
		mydata_lst.setOnItemClickListener(new AdapterView.OnItemClickListener(){

				private AlertDialog dlg;
				@Override
				public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4) {
					final Backupz b=(Backupz) p1.getItemAtPosition(p3);
					AlertDialog.Builder d=new AlertDialog.Builder(getActivity());
					d.setMessage(Html.fromHtml("<b>Ukuran File: </b>"+readableFileSize( b.getFile().length())+"<br><b>Waktu Backup: </b>"+b.getTime()+"<br><b>Jumlah Data: </b>"+b.getDbCount()+"<hr>"));
					d.setTitle("Database Info");
					d.setPositiveButton("Restore", new DialogInterface.OnClickListener(){
							@Override
							public void onClick(DialogInterface p1, int p2) {
								dlg.dismiss();
								doRestore(b);
							}
						});
					d.setNegativeButton("Hapus", new DialogInterface.OnClickListener(){
							@Override
							public void onClick(DialogInterface p1, int p2) {
								Snackbar s=Snackbar.make(view.findViewById(R.id.rootfrag24), "Tekan HAPUS untuk menkonfirmasi", Snackbar.LENGTH_LONG);
								s.setAction("HAPUS", new View.OnClickListener(){
										@Override
										public void onClick(View p1) {
											b.getFile().delete();
											datalist.remove(b);
											adapt.notifyDataSetChanged();
											if(datalist.size()<1) notif_aja.setText("Tidak Ada Backup, silahkan buat baru.");
										}
									});
								s.show();
							}
						});
					dlg=d.create();
					dlg.show();

				}
			});
		//ketr.setText(Html.fromHtml( getResources().getString(R.string.helptext).replace("\n","<br>").replace("%lesing%", htmllesing).replace("%kontak%",htmlkontak)));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId()==R.id.upmenu2){
			getActivity().finish();
			MainActivity.exit();
		}else if(item.getItemId()==R.id.clean_data_menu){
			AlertDialog.Builder dl=new AlertDialog.Builder(getActivity());
			dl.setTitle("Bersihkan Data");
			dl.setMessage("Data yang ada akan Terhapus. Untuk Data Backup masih Aman, tidak akan terhapus. Setelah dibersihkan App akan Restart. Lanjutkan?");
			dl.setPositiveButton("YA", new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface p1, int p2) {
						try {
							xAssets(AssetDatabaseOpenHelper.dbFile, "image.jpg");
							restartApp();
							
						} catch (IOException e) {
							Toast.makeText(getActivity(), "Terjadi Kesalahan, "+e.getMessage(), 1).show();
						}
					}
				});
			dl.setNegativeButton("Batal", null);
			dl.show();
		}
		return super.onOptionsItemSelected(item);
	}

	private void csvChooser(){
		Intent intent = new Intent(getActivity(), org.oucho.filepicker.FilePickerActivity.class);
		intent.putExtra(FilePicker.DISABLE_SORT_BUTTON, true);
		intent.putExtra(FilePicker.SET_FILTER_LISTED, new String[] { "csv"});
		intent.putExtra(FilePicker.SET_ONLY_ONE_ITEM, true);
		intent.putExtra(FilePicker.SET_CHOICE_TYPE, FilePicker.CHOICE_TYPE_FILES);
		intent.putExtra(FilePicker.DISABLE_NEW_FOLDER_BUTTON, true);
		intent.putExtra(FilePicker.ENABLE_QUIT_BUTTON, true);
		startActivityForResult(intent, 669);
	}
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.backup_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
	private class ExportNowAndUpload extends AsyncTask<Void, Boolean, String> {
		AlertDialog dlg;
		String dbs;
		Button cancelbtn;
		boolean hasData=true;
		@Override
		protected String doInBackground(Void[] p1) {
			//dbsd.delete();
			long unixTime = new Date().getTime();//System.currentTimeMillis() / 1000L;
			dbs=bakup_loc.getPath()+"/9999999999-"+unixTime;
			//dbs=dbsd.getPath();
			int dbc=QueryEngine.writeCsv(dbsd.getPath(), true);
			dbs_correnctname=dbs.replace("9999999999",""+dbc);
			if(dbc<1) hasData=false;
			return dbs;
		}

		@Override
		protected void onPreExecute() {
			AlertDialog.Builder d=new AlertDialog.Builder(getActivity());
			d.setTitle("Backup data");
			d.setMessage("Mohon tunggu...");
			d.setCancelable(false);
			d.setNegativeButton("Close", null);
			d.setCancelable(false);
			dlg=d.create();
			dlg.show();
			cancelbtn=dlg.getButton(DialogInterface.BUTTON_NEGATIVE);
			cancelbtn.setVisibility(View.GONE);
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(String result) {
			
			dlg.dismiss();
			/*new Handler().postDelayed(new Runnable(){
					@Override
					public void run() {
						dlg.dismiss();
						//if(hasData) Toast.makeText(getActivity(), "Selesai", 1).show();
					}
				}, 1000);*/
			
			if(!hasData){
				Toast.makeText(getActivity(), "Tidak ada Data User",1).show();
				dbsd.delete();
				return;
			}else{
				
				
				UploadDb(dbsd.getPath());
			}
			//new File(dbs).renameTo(new File(dbs_correnctname));
			//UploadDb(result);
			super.onPostExecute(result);
		}
	}
	private void doRestore(final Backupz b){
		AlertDialog.Builder dl=new AlertDialog.Builder(getActivity());
		dl.setTitle("Restore Data");
		dl.setMessage("Restore Data akan menimpa/memperbarui data yang sama. Pastikan apa yang kamu lakukan ini benar. Ingin Lanjutkan?");
		dl.setPositiveButton("YA", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					unGzip(b.getFile().getPath(), dbsd.getPath());
					RestoreNow dbs=new RestoreNow(getActivity(),1);
					dbs.importCsv(dbsd.getPath(), "yes");
				}
			});
		dl.setNegativeButton("Batal", null);
		dl.show();
		
		
	}
	private void UploadDb(String path){
		/*final Intent intent=new Intent();
		intent.putExtra("db", path);
		intent.setComponent(new ComponentName("dtm.id.login", "dtm.id.login.Activity.UploadActivity"));
		startActivityForResult(intent,666);*/
		Intent t=new Intent(getActivity(), DocVerify.class);
		t.putExtra("msg", MainActivity.email+" memBackup CSV ini");
		t.putExtra("file", path);
		t.putExtra("from",0);
		getActivity().startService(t);
	}
	/*private class CekDataAndRestore extends AsyncTask<String, Void, Void> {
		boolean ada=false;
		private String dburk;
		@Override
		protected Void doInBackground(String[] p1) {
			dburk=p1[0];
			ada=remotExists(dburk);
			return null;
		}

		@Override
		protected void onPreExecute() {
			Toast.makeText(getActivity(), "Mengecek ketersediaan data...", 0).show();
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Void result) {
			if(ada){
				downloadData(dburk);
			}else{
				Toast.makeText(getActivity(), "Tidak ada Data Backup", 1).show();
			}
			super.onPostExecute(result);
		}

	}*/

	/*private void downloadData(String downloadUri){
		downloadManager2=new ThinDownloadManager();
		final ProgressDialog dialog = new ProgressDialog(getActivity());
		dialog.setTitle("Download Data Backup");
		dialog.setCancelable(false);
		dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					downloadManager2.cancel(downloadid);
					Toast.makeText(getActivity(), "Restore dibatalkan",Toast.LENGTH_LONG).show();
				}
			});
		//dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setVisibility(View.GONE);
		dialog.show();
		dialog.setIndeterminate(true);

		dialog.setMessage("Mohon tunggu...");
		//dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		dialog.setIndeterminate(false);
		//dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setVisibility(View.VISIBLE);
		DownloadRequest downloadRequest = new DownloadRequest(Uri.parse(downloadUri))
            .setRetryPolicy(new DefaultRetryPolicy())
            .setDestinationURI(Uri.parse(dbsd.getPath())).setPriority(DownloadRequest.Priority.HIGH)
            .setDownloadListener(new DownloadStatusListener() {
				@Override
				public void onDownloadComplete(int p1) {
					dialog.dismiss();
					RestoreNow dbs=new RestoreNow(getActivity(),1);
					dbs.importCsv(dbsd.getPath());
					//Toast.makeText(ctx, "Restart App...",Toast.LENGTH_LONG).show();
					//dbs.restartApp();
					//

				}

				@Override
				public void onDownloadFailed(int p1, int p2, String p3) {
					dialog.dismiss();
					Toast.makeText(getActivity(), "Download data Gagal, "+p3,Toast.LENGTH_LONG).show();
				}

				@Override
				public void onProgress(int id, long total, long loaded, int prog) {
					// TODO: Implement this method
					dialog.setMessage("Mengambil data Backup ("+readableFileSize(total)+") ");
					dialog.setProgress(prog);
				}
			});
		//dialog.setProgress(0);

		downloadid=downloadManager2.add(downloadRequest);
		//dialog.show();
	}*/
	public static String readableFileSize(long size) {
		if(size <= 0) return "0";
		final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
		int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}
	private boolean cekKolom(String f){
		boolean iscompat=false;
		try {
			BufferedReader brTest = new BufferedReader(new FileReader(f));
			String clm=brTest.readLine().replace(" ","").trim();
			if(clm.split(",").length==9) iscompat=true;
			//Toast.makeText(getActivity(), clm, Toast.LENGTH_LONG).show();
		}
		catch(Exception e) {}
		return iscompat;
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==666){
			if(resultCode==660){
				Toast.makeText(getActivity(), "Backup Gagal", Toast.LENGTH_LONG).show();
			}else if(resultCode==663){
				//long now=new Date().getTime();
				/*AlertDialog.Builder dd=new AlertDialog.Builder(getActivity());
				dd.setTitle("Backup Berhasil");
				dd.setMessage("Data telah diamankan, kamu dapat restore kapanpun.");
				dd.show();
				toGzip(dbsd.getPath(), dbs_correnctname);
				datalist.add(new Backupz(new File(dbs_correnctname)));
				adapt.notifyDataSetChanged();
				notif_aja.setText("Data Backup Kamu:");*/
				
			}
		}/*else if (requestCode == 23) {
            if (resultCode == getActivity().RESULT_OK) {
				String contents = data.getStringExtra("SCAN_RESULT"); //this is the result
				//pubkey.setText(contents);
				String uu=bucket_url_base.replace("$MYFILE", contents);
				new CekDataAndRestore().execute(uu);
            } else 
            if (resultCode == getActivity().RESULT_CANCELED) {
				// Handle cancel
            }
        }*/
		if (requestCode == 669) {
			if (data != null) {
				FilePickerParcelObject object = data.getParcelableExtra(FilePickerParcelObject.class.getCanonicalName());
				File root=new File(object.path);
				if (object.count > 0) {
					for (int i = 0; i < object.count; i++) {
						String path=new File(root, object.names.get(i)).getPath();
						boolean compat=cekKolom(path);
						if(!compat){
							Toast.makeText(getActivity(), "Database kolom tidak sesuai, harus 9 Kolom", Toast.LENGTH_SHORT).show();
							return;
						}
						
						Intent t=new Intent(getActivity(), DocVerify.class);
						t.putExtra("msg", MainActivity.email+" mengImport CSV ini");
						t.putExtra("file", path);
						t.putExtra("from", 1);
						getActivity().startService(t);
						//Toast.makeText(this, "Selected "+path, Toast.LENGTH_LONG).show();
						break;
					}
				}
			}
		}
	}
	/*public static boolean remotExists(String URLName){
		Log.d("cekurl", URLName+"---");
		try {
			HttpURLConnection.setFollowRedirects(false);
			// note : you may also need
			//        HttpURLConnection.setInstanceFollowRedirects(false)
			HttpURLConnection con =
				(HttpURLConnection) new URL(URLName).openConnection();
			//con.setRequestMethod("HEAD");
			Log.d("cekurl", URLName+"---"+con.getResponseCode());
			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		}
		catch (Exception e) {
			Log.d("cekurl", URLName+"---"+e.getMessage());
			e.printStackTrace();
			return false;
		}
	}*/
	/*private boolean isPackageInstalled(String packagename, PackageManager packageManager) {
		try {
			packageManager.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	private void setClipboard(String text) {
		if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
			android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
			clipboard.setText(text);
		} else {
			android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
			android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
			clipboard.setPrimaryClip(clip);
		}
	}*/
	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager 
			= (ConnectivityManager)getActivity(). getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	public void restartApp() {
		FragmentActivity act=getActivity();
		act.finish();
		MainActivity.exit();
		Intent intent = new Intent();
		intent.setClassName("dtm.id.login", "dtm.id.login.Activity.LoginActivity");
		intent.putExtra("autologin", true);
		act.startActivity(intent);
	}
	private void xAssets(File out,String  asset) throws IOException {
        InputStream is =getActivity(). getAssets().open(asset);
        OutputStream os = new FileOutputStream(out);
        byte[] buffer = new byte[1024];
        while (is.read(buffer) > 0) {
            os.write(buffer);
        }
        os.flush();
        os.close();
        is.close();
    }
	public void toGzip(String SOURCE_FILE, String OUTPUT_GZIP_FILE){
		byte[] buffer = new byte[1024];
		try{
			GZIPOutputStream gzos =
				new GZIPOutputStream(new FileOutputStream(OUTPUT_GZIP_FILE));
			FileInputStream in =
				new FileInputStream(SOURCE_FILE);

			int len;
			while ((len = in.read(buffer)) > 0) {
				gzos.write(buffer, 0, len);
			}
			in.close();
			gzos.finish();
			gzos.close();
			System.out.println("Done");
		}catch(IOException ex){
			ex.printStackTrace();
		}
	}
	public void unGzip(String INPUT_GZIP_FILE, String OUTPUT_FILE){
		byte[] buffer = new byte[1024];
		try{
			GZIPInputStream gzis =
				new GZIPInputStream(new FileInputStream(INPUT_GZIP_FILE));
			FileOutputStream out =
				new FileOutputStream(OUTPUT_FILE);
			int len;
			while ((len = gzis.read(buffer)) > 0) {
				out.write(buffer, 0, len);
			}
			gzis.close();
			out.close();
			System.out.println("Done");
		}catch(IOException ex){
			ex.printStackTrace();
		}
	}
}
