package dtm.idv2.Fragment;
import dtm.idv2.Utils.*;
import dtm.idv2.Model.*;
import android.os.*;
import android.support.v7.app.*;
import java.io.IOException;
import java.io.*;
import android.content.Context;
import android.app.Activity;
import dtm.idv2.*;
import android.content.*;
//import android.database.sqlite.SQLiteDatabase;
import java.util.*;
import android.database.DatabaseUtils.InsertHelper;
//import org.sqlite.database.sqlite.*;
import io.requery.android.database.sqlite.SQLiteDatabase;
import android.app.ProgressDialog;

public class RestoreNow {
	AlertDialog dlg;
	boolean restartReq=false;
	Context mContext;
	int mymodes;

	private boolean wipeData=false;
	public RestoreNow(Context ctx, int mode) {
		mContext=ctx;
		mymodes=mode;
	}
	public class restore_db extends AsyncTask<Backupz, Boolean, Boolean> {
		@Override
		protected Boolean doInBackground(Backupz[] p1) {
			Backupz b=p1[0];
			try {
				copy(b.getFile(), AssetDatabaseOpenHelper.dbFile);
				return true;
			}
			catch(IOException e) {
				return false;
			}
		}
		@Override
		protected void onPreExecute() {
			AlertDialog.Builder d=new AlertDialog.Builder(mContext);
			if(mymodes==0)
			d.setMessage("Restore Database. Mohon tunggu...");
			else d.setMessage("Sync Database. Mohon tunggu...");
			dlg=d.create();
			dlg.setCancelable(false);
			dlg.show();
			super.onPreExecute();
		}
		@Override
		protected void onPostExecute(Boolean result) {
			if(result) {
				if(mymodes==0) dlg.setMessage("Restore Berhasil. Restart Aplikasi...");
				else dlg.setMessage("Sync Berhasil. Restart Aplikasi...");
				new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							restartApp();                     
						}
					}, 2000);
			}
			else {
				dlg.setCancelable(true);
				dlg.setMessage("Backup Gagal");
			}
			super.onPostExecute(result);
		}
	}
	public void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);
		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}
	public void restartApp() {
		Activity act=((Activity)mContext);
		act.finish();
		MainActivity.exit();
		Intent intent = new Intent();
		intent.setClassName("dtm.id.login", "dtm.id.login.Activity.LoginActivity");
		intent.putExtra("autologin", true);
		act.startActivity(intent);
	}

	// hello
	public void importCsv(String path, String mydata) {
		new import_csv2().execute(path, mydata);
		//new injectData().execute(new File(path), AssetDatabaseOpenHelper.dbFile);
	}
	public void importCsvWithRestart(String path) {
		restartReq=true;
		new import_csv2().execute(path);
	}
	public void importCsvWithRestartWipe(String path) {
		restartReq=true;
		wipeData=true;
		new import_csv2().execute(path);
	}
	public void restoreDb(Backupz b){
		new restore_db().execute(b);
	}
	public void mergeDatabase(String dest){
		new mergeData().execute(dest);
	}
	public class mergeData extends AsyncTask<String, Void, Void> {
		Exception error=null;
		@Override
		protected Void doInBackground(String[] p1) {
			String dest=p1[0];
			try{
				QueryEngine.db.execSQL("attach '"+dest+"' as baru;");
				QueryEngine.db.execSQL("create table if not exists 'data2' ('suggest_text_1' text unique on conflict ignore, 'suggest_text_2' text);");
				QueryEngine.db.execSQL("insert into data2 select * from data;");
				QueryEngine.db.execSQL("insert into data2 select * from baru.data;");
				QueryEngine.db.execSQL("drop table data");
				QueryEngine.db.execSQL("alter table data2 rename to data");
			}catch(Exception er){
				error=er;
			}
			
			return null;
		}

		@Override
		protected void onPreExecute() {
			AlertDialog.Builder d=new AlertDialog.Builder(mContext);
			d.setMessage("Sync Database...");
			dlg=d.create();
			dlg.show();
			dlg.setCancelable(false);
			super.onPreExecute();
		}
		
		@Override
		protected void onPostExecute(Void result) {
			dlg.setCancelable(true);
			if(error==null){
				dlg.setMessage("Sync Done. Restart App...");
				new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							restartApp();
						}
					}, 2000);
			}else{
				dlg.setMessage("Sync Error: "+error.getMessage());
			}
			super.onPostExecute(result);
		}
		
	}
	
	
	public class import_csv2 extends AsyncTask<String, Integer, Integer> {
		AlertDialog dlg;
		Exception error;
		String ygerror;
		private SQLiteDatabase db;
		@Override
		protected Integer doInBackground(String[] p1) {
			//InputStream inputStream = null;
			boolean mydata=p1[1].equals("yes");
			try {
				//inputStream = new FileInputStream(p1[0]);
				//CSVReader reader = new CSVReader(new InputStreamReader(inputStream));
				//String[] nextLine=reader.readNext();
				//List<String> al=Arrays.asList(nextLine);
				//String result = al.toString().replaceAll("(^\\[|\\]$)", "").replace(", ", " TEXT,")+" TEXT";
				db = QueryEngine.db;
				if(wipeData) db.delete(QueryEngine.tableName, null, null);
				//db.setLockingEnabled(false);
				io.requery.android.database.sqlite.SQLiteStatement stat=db.compileStatement("insert into data values(?,?,?,?,?,?,?,?,?)");
				db.beginTransaction();
				/*InsertHelper ih = new InsertHelper(db, QueryEngine.tableName);

				 // Get the numeric indexes for each of the columns that we're updating
				 final int nopol_ = ih.getColumnIndex(QueryEngine.Colm_nopol);
				 final int mobil_ = ih.getColumnIndex(QueryEngine.Colm_mobil);
				 final int lesing_ = ih.getColumnIndex(QueryEngine.Colm_lesing);
				 final int ovd_ = ih.getColumnIndex(QueryEngine.Colm_ovd);
				 final int ket_ = ih.getColumnIndex(QueryEngine.Colm_ket);
				 final int cabang_ = ih.getColumnIndex(QueryEngine.colm_cabang);*/
				int ii=1;
				String line = "";
				BufferedReader br = new BufferedReader(new FileReader(p1[0]));
				br.readLine();
				while ((line = br.readLine()) != null) {
					// use comma as separator
					String[] nextLine = (line.replace("\"","")+" ").split(",");
					if(nextLine.length<9) continue;
					if(nextLine[0].trim().toLowerCase().startsWith("nopol"))continue;
					//android.util.Log.d("cssv", line);
					/*ContentValues dd=new ContentValues();
					dd.put(QueryEngine.Colm_nopol, nextLine[0].trim().toUpperCase());
					 dd.put(QueryEngine.Colm_mobil, Caps( nextLine[1].trim()));
					 dd.put(QueryEngine.Colm_lesing, Caps(nextLine[2].trim()));
					 dd.put(QueryEngine.Colm_ovd, nextLine[3].trim());
					 dd.put(QueryEngine.Colm_saldo, Caps( nextLine[4].trim()));
					 dd.put(QueryEngine.colm_cabang, Caps(nextLine[5].trim())+(mydata?"$":""));
					dd.put(QueryEngine.Colm_nama, Caps(nextLine[6].trim()));
					dd.put(QueryEngine.Colm_noka, Caps(nextLine[7].trim()));
					dd.put(QueryEngine.Colm_nosin, Caps(nextLine[8].trim()));
					db.insert("data", null, dd);*/
					stat.clearBindings();
					stat.bindString(1, nextLine[0].trim().toUpperCase());
					stat.bindString(2, Caps( nextLine[1].trim()));
					stat.bindString(3, Caps(nextLine[2].trim()));
					stat.bindString(4, nextLine[3].trim());
					stat.bindString(5, Caps( nextLine[4].trim()));
					stat.bindString(6, Caps(nextLine[5].trim())+(mydata?"$":""));
					stat.bindString(7, Caps(nextLine[6].trim()));
					stat.bindString(8, Caps(nextLine[7].trim()));
					stat.bindString(9, Caps(nextLine[8].trim()));
					stat.execute();
					
					//android.util.Log.d("cssv", data[0]);
					//publishProgress(ii);
					//ii++;
				}

				//db.setLockingEnabled(true);
				//db.add_index(al.get(0));
				//db.execSQL("CREATE UNIQUE INDEX agusibrahim on data ("+al.get(0)+")");
				db.setTransactionSuccessful();
				db.endTransaction();
				return -1;
			} catch (Exception e) {
				error = e;
				android.util.Log.d("cssv", ""+e.getMessage());
				return -2;
			}
		}

		@Override
		protected void onProgressUpdate(Integer[] values) {
			if (mymodes == 0) dlg.setMessage("Please wait...\nMemasukan data " + values[0]);
			else dlg.setMessage("Please wait...\nSync data " + values[0]);
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPreExecute() {
			AlertDialog.Builder b=new AlertDialog.Builder(mContext);
			b.setMessage("Please wait...");
			b.setCancelable(false);
			dlg = b.create();
			dlg.show();
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Integer hasil) {
			dlg.setCancelable(true);
			if (hasil == -1) {
				if (restartReq) {
					dlg.setMessage("Sync Berhasil. Restart Aplikasi...");
				} else {
					dlg.setMessage("Data Sucessfully Imported");
				}
			} else {
				dlg.setMessage("Error Bro, kirim pesan ini ke pembuat:\n" + getStackTrace(error) + "@@" + ygerror);
			}
			//db.close();
			//db.close();
			new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if (error == null && restartReq == false) dlg.dismiss();
						if (restartReq) {
							restartApp();
						}
					}
				}, 2000);
			super.onPostExecute(hasil);
		}
	}
	/*public class import_csv_old extends AsyncTask<String, Integer, Integer> {
		AlertDialog dlg;
		Exception error;
		String ygerror;
		private SQLiteDatabase db;
		@Override
		protected Integer doInBackground(String[] p1) {
			InputStream inputStream = null;

			try {
				inputStream=new FileInputStream(p1[0]);
				CSVReader reader = new CSVReader(new InputStreamReader(inputStream));
				String[] nextLine=reader.readNext();
				List<String> al=Arrays.asList(nextLine);
				//String result = al.toString().replaceAll("(^\\[|\\]$)", "").replace(", ", " TEXT,")+" TEXT";
				db=QueryEngine.db;
				db.beginTransaction();
				int ii=1;
				while((nextLine = reader.readNext()) != null) {
					if(nextLine != null) {
						int ke=0;
						ContentValues dd=new ContentValues();
						ygerror=java.util.Arrays.toString(nextLine).substring(1).replaceAll("\\]$", "");
						for(String s:nextLine) {
							if(ke>1) continue;
							if(al.get(ke).equals(QueryEngine.Column1)&&s.contains("~")) {
								//s=s.toUpperCase();
								String h=s.split("~")[0];
								//s=s.replace(h, h.toUpperCase());
							}
							dd.put(al.get(ke), s);
							ke++;
						}
						//db.add(dd);
						db.insert("data", null, dd);
					}
					publishProgress(ii);
					ii++;
				}
				//db.add_index(al.get(0));
				//db.execSQL("CREATE UNIQUE INDEX agusibrahim on data ("+al.get(0)+")");
				db.setTransactionSuccessful();
				db.endTransaction();
				return -1;
			}
			catch(Exception e) {
				error=e;
				return -2;
			}
		}

		@Override
		protected void onProgressUpdate(Integer[] values) {
			if(mymodes==0) dlg.setMessage("Please wait...\nMemasukan data " + values[0]);
			else dlg.setMessage("Please wait...\nSync data " + values[0]);
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPreExecute() {
			AlertDialog.Builder b=new AlertDialog.Builder(mContext);
			b.setMessage("Please wait...");
			b.setCancelable(false);
			dlg=b.create();
			dlg.show();
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Integer hasil) {
			dlg.setCancelable(true);
			if(hasil == -1) {
				if(restartReq){
					dlg.setMessage("Sync Berhasil. Restart Aplikasi...");
				}else{
					dlg.setMessage("Data Sucessfully Imported");
				}
			}
			else {
				dlg.setMessage("Error Bro, kirim pesan ini ke pembuat:\n" + getStackTrace(error)+"@@"+ygerror);
			}
			//db.close();
			//db.close();
			new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if(error == null&&restartReq==false) dlg.dismiss();
						if(restartReq){
							restartApp();
						}
					}
				}, 2000);
			super.onPostExecute(hasil);
		}
	}*/
	public static String getStackTrace(final Throwable throwable) {
		final StringWriter sw = new StringWriter();
		final PrintWriter pw = new PrintWriter(sw, true);
		throwable.printStackTrace(pw);
		return sw.getBuffer().toString();
	}
	private String Caps(String name){
		if(name.length()<1) return name;
		name = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
		return name;
	}
}
