package dtm.idv2.Fragment;
import android.support.v7.app.*;
import android.os.*;
import android.support.design.widget.*;
import android.widget.Button;
import android.view.*;
import android.support.v4.app.Fragment;
import dtm.idv2.R;

import java.io.File;
import android.content.*;
import dtm.idv2.MainActivity;
import android.widget.ImageButton;
import android.graphics.Bitmap;

import android.widget.*;
import org.oucho.filepicker.*;
import dtm.idv2.Adapter.*;
import java.util.*;
import dtm.idv2.Model.MyFile;
import android.net.Uri;
import dtm.idv2.Utils.*;
import android.text.*;
import android.support.v4.app.*;

public class UpFragment extends Fragment
{
	Button fabu;
	ListView myfiles;
	View appview;
	FileAdapter adapt;
	private File dbsd;
	ArrayList<MyFile> files=new ArrayList<MyFile>();
	Iterator iter;
	private static final int CAMERA_REQUEST = 1888;
	ArrayList<String> selected=new ArrayList<String>();
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.upload_activity, container, false);
		fabu=(Button) v.findViewById(R.id.uploadbtn);
		//opencam=(ImageButton) v.findViewById(R.id.fab_uploadcam);
		myfiles=(ListView) v.findViewById(R.id.myfiles);
		adapt=new FileAdapter(getActivity(), files);
		myfiles.setAdapter(adapt);
		MainActivity.apptitle.setText("Sumbang Data");
		setHasOptionsMenu(true);
		return v;
	}
	@Override
	public void onViewCreated(final View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		appview=view;
		
		fabu.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					/*for(int i=0;i<20;i++){
						files.add(new MyFile("Nama File"+i+".zip", "http://jjj"+i+".com","171h27dhdh272772jdjd", 12345));
					}
					adapt.notifyDataSetChanged();*/
					doUpload2(view);
				}
			});
		// Matikan fungsi view, copy url dan share jika bukan Admin
		myfiles.setOnItemClickListener(new AdapterView.OnItemClickListener(){
				@Override
				public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4) {
					if(!MainActivity.isAdmin){
						return;
					}
					final MyFile p=(MyFile) p1.getItemAtPosition(p3);
					final PopupMenu popupMenu = new PopupMenu(getActivity(), p2);
					final Menu menu = popupMenu.getMenu();
					popupMenu.getMenuInflater().inflate(R.menu.filepopup, menu);
					popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener(){
							@Override
							public boolean onMenuItemClick(MenuItem item) {
								//Toast.makeText(getActivity(), item.getTitle()+"\n"+p.name, Toast.LENGTH_LONG).show();
								switch(item.getItemId()){
									case R.id.pop_copy:
										setClipboard(p.url);
										Toast.makeText(getActivity(), "Sudah di Copy", Toast.LENGTH_SHORT).show();
										break;
									case R.id.pop_share:
										sharE(p.url);
										break;
									case R.id.pop_view:
										viewUrl(p.url);
										break;
								}
								return false;
							}
						});
					//popupMenu.setGravity(Gravity.CENTER_HORIZONTAL);
					popupMenu.show();
					
				}
			});
	}
	private void doUpload2(final View v){
		Intent intent = new Intent(getActivity(), org.oucho.filepicker.FilePickerActivity.class);
		intent.putExtra(FilePicker.DISABLE_SORT_BUTTON, true);
		intent.putExtra(FilePicker.SET_CHOICE_TYPE, FilePicker.CHOICE_TYPE_FILES);
		intent.putExtra(FilePicker.DISABLE_NEW_FOLDER_BUTTON, true);
		intent.putExtra(FilePicker.ENABLE_QUIT_BUTTON, true);
		startActivityForResult(intent, 669);
	}
	
	/*private void doUpload(View view){
		new ChooserDialog().with(view.getContext())
			.withStartFile(Environment.getExternalStorageDirectory().getPath())
			//.withFilter(false, ".csv")
			.withChosenListener(new ChooserDialog.Result() {
				@Override
				public void onChoosePath(String path, File pathFile) {
					Upload(path);
				}
			})
			.build()
			.show();
	}*/
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
			/*case R.id.upmenu1:
				doUpload2(appview);
				break;*/
			case R.id.upmenu2:
				getActivity().finish();
				MainActivity.exit();
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.upload_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
	
	private void Upload(String path){
		final Intent intent=new Intent();
		intent.putExtra("path", path);
		intent.setComponent(new ComponentName("dtm.id.login", "dtm.id.login.Activity.UploadActivity"));
		startActivityForResult(intent,666);
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 669) {
			if (data != null) {
				FilePickerParcelObject object = data.getParcelableExtra(FilePickerParcelObject.class.getCanonicalName());
				File root=new File(object.path);
				selected.clear();
				if (object.count > 0) {
					for (int i = 0; i < object.count; i++) {
						String path=new File(root, object.names.get(i)).getPath();
						selected.add(path);
					}
					iter=selected.iterator();
					new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								if(iter.hasNext()) Upload((String)iter.next());                    
							}
						}, 800);
				}
			}
		}else if(requestCode==666){
			if(resultCode==660){
				Toast.makeText(getActivity(), "Upload Gagal", Toast.LENGTH_LONG).show();
			}else if(resultCode==661){
				String url=data.getStringExtra("url");
				String nama=data.getStringExtra("name");
				long size=data.getLongExtra("size",0);
				String hash=data.getStringExtra("md5");
				files.add(new MyFile(nama, url,hash, size));
				adapt.notifyDataSetChanged();
				if(iter.hasNext()) Upload((String)iter.next());
				Toast.makeText(getActivity(), "Upload OK\n"+nama, Toast.LENGTH_LONG).show();
			}else if(resultCode==663){
				AlertDialog.Builder dd=new AlertDialog.Builder(getActivity());
				dd.setTitle("Backup Success");
				dd.setMessage("Data Berhasil dibackup ke Penyimpanan Online. Restore kapanpun");
				dd.show();
			}
		}
	}
	private void setClipboard(String text) {
		if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
			android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
			clipboard.setText(text);
		} else {
			android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
			android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
			clipboard.setPrimaryClip(clip);
		}
	}
	private void viewUrl(String url){
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}
	private void sharE(String url){
		Intent i = new Intent(android.content.Intent.ACTION_SEND);
		i.setType("text/plain");  
		i.putExtra(android.content.Intent.EXTRA_TEXT, url);
		startActivity(i);
	}
}
