package dtm.idv2.Fragment;

import android.support.v4.app.Fragment;
import android.view.*;
import android.os.*;
import java.io.*;
import android.widget.*;
import java.util.*;
import dtm.idv2.Model.*;
import dtm.idv2.*;
import android.support.v7.app.*;
import android.content.*;
import dtm.idv2.Utils.*;
import android.text.*;
import android.support.design.widget.*;
import java.text.*;
import dtm.idv2.Adapter.*;

import android.database.sqlite.*;
import java.util.regex.*;
import android.database.*;
import org.oucho.filepicker.FilePicker;
import org.oucho.filepicker.FilePickerParcelObject;
/*
Perbaikan (12Nov16 12:10):
* import csv atau tambah data, nopol di Kapital otomatis
* boleh mengisi cabang di VALUE
* di build tanpa db, harus tambahkan/import csv/restore untuk mengisi data
* bersihkan data dengan cara clear data di info aplikasi

*/

public class BackupActivity extends AppCompatActivity
{
	String BACKUPDIR="";
	String EXPORTDIR="";
	Button backupbtn;
	Button importbtn, exportbtn;
	ListView backuplist;
	List<Backupz> datalist=new ArrayList<Backupz>();
	BackupAdapter adapt;
	AlertDialog dlg=null;
	LinearLayout rootfrag2;
	Context ctx;
	private static final String[] Q = new String[]{"", "K", "M", "G", "T", "P", "E"};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO: Implement this method
		super.onCreate(savedInstanceState);
		setContentView(R.layout.frag2);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		BACKUPDIR=Environment.getExternalStorageDirectory().getPath()+"/DMBackup";
		EXPORTDIR=Environment.getExternalStorageDirectory().getPath()+"/DMExport";
		//View v=inflater.inflate(R.layout.frag2, container, false);
		backupbtn= (Button) findViewById(R.id.backup_btn);
		importbtn=(Button) findViewById(R.id.importbtn);
		backuplist=(ListView) findViewById(R.id.backup_list);
		exportbtn=(Button) findViewById(R.id.exportbtn);
		File fl=new File(BACKUPDIR);
		if(!fl.exists()) fl.mkdirs();
		
		File flo=new File(EXPORTDIR);
		if(!flo.exists()) flo.mkdirs();
		for(File f:fl.listFiles()){
			Backupz bb=new Backupz(f);
			datalist.add(bb);
		}
		rootfrag2=(LinearLayout) findViewById(R.id.rootfrag2);
		ctx=this;
	
		adapt=new BackupAdapter(this, datalist);
		backuplist.setAdapter(adapt);
		
		exportbtn.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					//new ExportNow().execute();
					new FastCSVWriter().execute();
				}
			});
		backupbtn.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					doBackup();
					//restartApp();
				}
			});
		importbtn.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View p1) {
					
					csvChooser(p1);
					/*new ChooserDialog().with(ctx)
						.withStartFile(Environment.getExternalStorageDirectory().getPath())
						//.withFilter(false, ".csv")
						.withChosenListener(new ChooserDialog.Result() {
							@Override
							public void onChoosePath(String path, File pathFile) {
								if(!path.toLowerCase().endsWith(".csv")){
									Toast.makeText(p1.getContext(), "Hanya file CSV", Toast.LENGTH_SHORT).show();
									return;
								}
								boolean compat=cekKolom(path);
								if(!compat){
									Toast.makeText(p1.getContext(), "Database kolom tidak sesuai", Toast.LENGTH_SHORT).show();
									return;
								}
								new RestoreNow(BackupActivity.this,0).importCsv(path);
							}
						})
						.build()
						.show();*/
				}
			});
		
		backuplist.setOnItemClickListener(new AdapterView.OnItemClickListener(){
				@Override
				public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4) {
					final Backupz b=(Backupz) p1.getItemAtPosition(p3);
					AlertDialog.Builder d=new AlertDialog.Builder(ctx);
					d.setMessage(Html.fromHtml("<b>Ukuran File: </b>"+readableFileSize( b.getFile().length())+"<br><b>Waktu Backup: </b>"+b.getTime()+"<br><b>Jumlah Data: </b>"+b.getDbCount()+"<hr>"));
					d.setTitle("Database Info");
					d.setPositiveButton("Restore", new DialogInterface.OnClickListener(){
							@Override
							public void onClick(DialogInterface p1, int p2) {
								dlg.dismiss();
								doRestore(b);
							}
						});
					d.setNegativeButton("Hapus", new DialogInterface.OnClickListener(){
							@Override
							public void onClick(DialogInterface p1, int p2) {
								Snackbar s=Snackbar.make(rootfrag2, "Tekan HAPUS untuk menkonfirmasi", Snackbar.LENGTH_LONG);
								s.setAction("HAPUS", new View.OnClickListener(){
										@Override
										public void onClick(View p1) {
											b.getFile().delete();
											datalist.remove(b);
											adapt.notifyDataSetChanged();
										}
									});
								s.show();
							}
						});
					dlg=d.create();
					dlg.show();

				}
			});
	}
	
	private boolean cekKolom(String f){
		boolean iscompat=false;
		try {
			BufferedReader brTest = new BufferedReader(new FileReader(f));
			String clm=brTest.readLine().replace(" ","").trim();
			if(clm.split(",").length>=9) iscompat=true;
			//Toast.makeText(getActivity(), clm, Toast.LENGTH_LONG).show();
		}
		catch(Exception e) {}
		return iscompat;
	}
	private void csvChooser(final View v){
		Intent intent = new Intent(this, org.oucho.filepicker.FilePickerActivity.class);
		intent.putExtra(FilePicker.DISABLE_SORT_BUTTON, true);
		intent.putExtra(FilePicker.SET_FILTER_LISTED, new String[] { "csv"});
		intent.putExtra(FilePicker.SET_ONLY_ONE_ITEM, true);
		intent.putExtra(FilePicker.SET_CHOICE_TYPE, FilePicker.CHOICE_TYPE_FILES);
		intent.putExtra(FilePicker.DISABLE_NEW_FOLDER_BUTTON, true);
		intent.putExtra(FilePicker.ENABLE_QUIT_BUTTON, true);
		startActivityForResult(intent, 669);
	}
	public static String readableFileSize(long size) {
		if(size <= 0) return "0";
		final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
		int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}

	private void doRestore(final Backupz b){
		AlertDialog.Builder dl=new AlertDialog.Builder(this);
		dl.setTitle("Restore Database");
		dl.setMessage("Restore database akan menimpa database yang tersimpan, disarankan untuk membackup data terlebih dahulu karena semua data akan diganti. Lanjutkan?");
		dl.setPositiveButton("YA", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					new RestoreNow(BackupActivity.this,0).restoreDb(b);
				}
			});
		dl.setNegativeButton("Batal", null);
		dl.show();
	}
	private void doBackup(){
		AlertDialog.Builder dl=new AlertDialog.Builder(this);
		dl.setTitle("Backup Database");
		dl.setMessage("Apakah kamu yakin ingin membackup database?");
		dl.setPositiveButton("YA", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					new BackupNow().execute();
				}
			});
		dl.setNegativeButton("Batal", null);
		dl.show();
	}
	private class FastCSVWriter extends AsyncTask<Void, Boolean, String> {
		AlertDialog dlg;
		String dbs;
		Button cancelbtn;
		@Override
		protected String doInBackground(Void[] p1) {
			int dbcount=new QueryEngine(ctx).getCount(null, "");
			long unixTime = new Date().getTime();//System.currentTimeMillis() / 1000L;
			//Cursor cur=new QueryEngine(ctx).db.rawQuery("SELECT "+QueryEngine.Column1+","+QueryEngine.Column2+" IN "+QueryEngine.tableName, null);
			dbs=EXPORTDIR+"/"+dbcount+"-"+unixTime+".csv";
			try{
				ProcessBuilder pb= new ProcessBuilder(MainActivity.sqlitebin.getPath(), "-noheader", "-csv", AssetDatabaseOpenHelper.dbFile.getPath(), "select * from data");
				//File workingFolder = new File("/sdcard/DMBackup");
				//pb.directory(workingFolder);j
				//android.util.Log.d("dbss", "Start: CWD="+workingFolder+",INPUT="+filename+",OUT="+output.getPath());
				java.lang.Process proc = pb.start();
				BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				String s = null;
				FileOutputStream fos=new FileOutputStream(dbs);
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
				System.out.println("WAIT...");
				while ((s = stdInput.readLine()) != null){
					bw.write(s.replace("\\$",""));
					bw.newLine();
				}
				System.out.println("OK");
				bw.close();
			}catch(Exception e){
				android.util.Log.e("exportc", "" + e);
			}
			return null;
		}
		@Override
		protected void onPreExecute() {
			AlertDialog.Builder d=new AlertDialog.Builder(ctx);
			d.setTitle("Export CSV");
			d.setMessage("Mohon tunggu...");
			d.setCancelable(false);
			d.setNegativeButton("Close", null);
			dlg=d.create();
			dlg.show();
			cancelbtn=dlg.getButton(DialogInterface.BUTTON_NEGATIVE);
			cancelbtn.setVisibility(View.GONE);
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(String result) {
			dlg.setMessage("Export sukses\nTersimpan di "+dbs);
			dlg.setCancelable(true);
			cancelbtn.setVisibility(View.VISIBLE);
			super.onPostExecute(result);
		}
	}
	private class ExportNow extends AsyncTask<Void, Boolean, String> {
		AlertDialog dlg;
		String dbs;
		Button cancelbtn;
		@Override
		protected String doInBackground(Void[] p1) {
			int dbcount=new QueryEngine(ctx).getCount(null, "");
			long unixTime = new Date().getTime();//System.currentTimeMillis() / 1000L;
			//Cursor cur=new QueryEngine(ctx).db.rawQuery("SELECT "+QueryEngine.Column1+","+QueryEngine.Column2+" IN "+QueryEngine.tableName, null);
			dbs=EXPORTDIR+"/"+dbcount+"-"+unixTime+".csv";
			QueryEngine.writeCsv(dbs, false);
			return dbs;
		}

		@Override
		protected void onPreExecute() {
			AlertDialog.Builder d=new AlertDialog.Builder(ctx);
			d.setTitle("Export CSV");
			d.setMessage("Mohon tunggu...");
			d.setCancelable(false);
			d.setNegativeButton("Close", null);
			dlg=d.create();
			dlg.show();
			cancelbtn=dlg.getButton(DialogInterface.BUTTON_NEGATIVE);
			cancelbtn.setVisibility(View.GONE);
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(String result) {
			dlg.setMessage("Export sukses\nTersimpan di "+dbs);
			dlg.setCancelable(true);
			cancelbtn.setVisibility(View.VISIBLE);
			super.onPostExecute(result);
		}
		
	};
	
	
	private class BackupNow extends AsyncTask<Void, Boolean, Boolean> {
		AlertDialog dlg;
		String dbs;
		@Override
		protected Boolean doInBackground(Void[] p1) {
			int dbcount=new QueryEngine(ctx).getCount(null, "");
			long unixTime = new Date().getTime();//System.currentTimeMillis() / 1000L;
			dbs=BACKUPDIR+"/"+dbcount+"-"+unixTime;
			try {
				copy(AssetDatabaseOpenHelper.dbFile, new File(dbs));
				return true;
			} catch (IOException e) {
				return false;
			}
		}

		@Override
		protected void onPreExecute() {
			AlertDialog.Builder d=new AlertDialog.Builder(ctx);
			d.setMessage("Backup Database. Mohon tunggu...");
			dlg=d.create();
			dlg.setCancelable(false);
			dlg.show();
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Boolean result) {
			dlg.setCancelable(true);
			if(result){
				dlg.setMessage("Backup Berhasil");
				datalist.add(new Backupz(new File(dbs)));
				adapt.notifyDataSetChanged();
			}else{
				dlg.setMessage("Backup Gagal");
			}
			new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						dlg.dismiss();                     
					}
				}, 2000);
			super.onPostExecute(result);
		}
	}

	public void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);
		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}
	@Override                
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId()==android.R.id.home){
			onBackPressed();
		}
		return true;
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 669) {
			if (data != null) {
				FilePickerParcelObject object = data.getParcelableExtra(FilePickerParcelObject.class.getCanonicalName());
				File root=new File(object.path);
				if (object.count > 0) {
					for (int i = 0; i < object.count; i++) {
						String path=new File(root, object.names.get(i)).getPath();
						boolean compat=cekKolom(path);
						if(!compat){
							Toast.makeText(this, "Database kolom tidak sesuai", Toast.LENGTH_SHORT).show();
							return;
						}
						new RestoreNow(BackupActivity.this,0).importCsv(path, "no");
						//Toast.makeText(this, "Selected "+path, Toast.LENGTH_LONG).show();
						break;
					}
				}
			}
		}
	}
}
