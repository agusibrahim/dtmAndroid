package dtm.idv2.Fragment;
import android.support.v4.app.Fragment;
import android.os.*;
import android.view.*;
import dtm.idv2.R;
import android.widget.*;
import android.inputmethodservice.*;
import dtm.idv2.Utils.*;
import dtm.idv2.MainActivity;
import android.preference.*;
import android.content.*;
import android.text.*;
import android.view.inputmethod.*;
import dtm.idv2.Model.*;
import java.util.*;
import dtm.idv2.Adapter.*;
import dtm.idv2.EditActivity;
import android.database.*;

public class NosinFragment extends Fragment
{
	public static ListView list;
	private Keyboard mKeyboard;
	private KbView keyboardView;
	private SharedPreferences pref;
	public static LinearLayout notfoundview;
	private ImageButton clearntn;

	private int limit;

	private KeyboardlessEditText2 query;
	public static List<Nosin> dataset=new ArrayList<Nosin>();
	private QueryEngine engine;

	private InputMethodManager imm;
	public static boolean isclear=false;
	public static NosinAdapter adapt;

	private ImageButton addbtn;
	private int getKbRes(){
		int res=0;
		int ks=Integer.parseInt(pref.getString("keysize", "2"));
		switch(ks){
			case 1:
				res=R.xml.kbox1;
				break;
			case 2:
				res=R.xml.kbox2;
				break;
			case 3:
				res=R.xml.kbox3;
				break;
			case 4:
				res=R.xml.kbox4;
				break;
			default:
				res=R.xml.kbox2;
		}
		return res;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.main, container, false);
		pref=PreferenceManager.getDefaultSharedPreferences(getActivity());
		limit=pref.getInt("limit", 50 );
		imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		query=(KeyboardlessEditText2) v.findViewById(R.id.inputq);
		list=(ListView)v.findViewById(R.id.rview);
		clearntn=(ImageButton) v.findViewById(R.id.clearBtn);
		notfoundview=(LinearLayout) v.findViewById(R.id.nofoundlayout);
		mKeyboard = new Keyboard(getActivity(), getKbRes());
		addbtn=(ImageButton) v.findViewById(R.id.add_btn);
		keyboardView = (KbView)v.findViewById(R.id.keyboard_view);
		if(!pref.getBoolean("khide", true)) keyboardView.setVisibility(View.GONE);
        keyboardView.setPreviewEnabled(false);
		keyboardView.setKeyboard(mKeyboard);
        keyboardView.setActivity(getActivity());
		keyboardView.setlistener();
		engine=new QueryEngine(getActivity());
		query.requestFocus();
		query.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
		query.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
		//keyboardView.setOnKeyboardActionListener(new KbListener(getActivity()));
		keyboardView.setlistener();
		if(!pref.getBoolean("khide", true)) imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
		else{imm.hideSoftInputFromWindow(query.getWindowToken(), 0);
			getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
			);
		}
		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		query.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(CharSequence p1, int p2, int p3, int p4) {
					// TODO: Implement this method
				}

				@Override
				public void onTextChanged(CharSequence p1, int p2, int p3, int p4) {
					if(p1.length()<0) return;
					//if(methodswitch.isChecked()) return;
					//if(!pref.getBoolean("autosearch", true)) return;
					dataset.clear();
					list.setSelectionAfterHeaderView();
					engine.populateNosin(p1.toString(),0,limit);
				}

				@Override
				public void afterTextChanged(Editable p1) {
					// TODO: Implement this method
				}
			});
		list.setOnScrollListener(new EndlessScrollListener() {
				@Override
				public boolean onLoadMore(int page, int totalItemsCount) {
					//Toast.makeText(getActivity(), "Page: "+page+"\nTotalitem: "+totalItemsCount,0).show();
					if(totalItemsCount<(limit)||page<2){
						//dataset.clear();
						//mydata.setSelectionAfterHeaderView();
						return false;
					}
					//Toast.makeText(getActivity(), "Load more "+((page-1)*limit)+",limit", Toast.LENGTH_SHORT).show();
					engine.populateNosin(query.getText().toString(), ((page-1)*limit), limit);
					//else engine.populateData(lastkw, ((page-1)*limit), limit);
					return true; // ONLY if more data is actually being loaded; false otherwise.
				}
			});
		addbtn.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					MainActivity.mselected=null;
					startActivity(new Intent(getActivity(), EditActivity.class));
				}
			});
		clearntn.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					query.setText("");
				}
			});
		list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
				@Override
				public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4) {
					Nosin uuo=(Nosin) p1.getItemAtPosition(p3);
					new LaunchEditor().execute(uuo);
					MainActivity.lastquery="s";
				}
			});
		super.onViewCreated(view, savedInstanceState);
	}
	
	private class LaunchEditor extends AsyncTask<Nosin, Void, Void> {
		private Unit uu;
		@Override
		protected Void doInBackground(Nosin[] p1) {
			Nosin nosin=p1[0];
			String sql="";//select * from " + QueryEngine.tableName+" where "+QueryEngine.Colm_noka+" like "+DatabaseUtils.sqlEscapeString(nosin.noka)+" and "+QueryEngine.Colm_nosin+" like "+DatabaseUtils.sqlEscapeString(nosin.nosin)+" limit 1";
			if(nosin.noka.length()>2&&nosin.nosin.length()<2){
				sql="select * from " + QueryEngine.tableName+" where "+QueryEngine.Colm_noka+" like "+DatabaseUtils.sqlEscapeString(nosin.noka)+" limit 1";
			}else if(nosin.noka.length()<2&&nosin.nosin.length()>2){
				sql="select * from " + QueryEngine.tableName+" where "+QueryEngine.Colm_nosin+" like "+DatabaseUtils.sqlEscapeString(nosin.nosin)+" limit 1";
			}else if(nosin.noka.length()>2&&nosin.nosin.length()>2){
				sql="select * from " + QueryEngine.tableName+" where "+QueryEngine.Colm_noka+" like "+DatabaseUtils.sqlEscapeString(nosin.noka)+" and "+QueryEngine.Colm_nosin+" like "+DatabaseUtils.sqlEscapeString(nosin.nosin)+" limit 1";
			}
			try {
				Cursor c=QueryEngine.db.rawQuery(sql, null);
				c.moveToFirst();
				android.util.Log.d("matelsql", sql);
				c.moveToPosition(0);
				String nopol_=c.getString(c.getColumnIndex(QueryEngine.Colm_nopol));
				String mobil_=c.getString(c.getColumnIndex(QueryEngine.Colm_mobil));
				String lesing_=c.getString(c.getColumnIndex(QueryEngine.Colm_lesing));
				String ovd_=c.getString(c.getColumnIndex(QueryEngine.Colm_ovd));
				String nama_=c.getString(c.getColumnIndex(QueryEngine.Colm_nama));
				String noka_=c.getString(c.getColumnIndex(QueryEngine.Colm_noka));
				String nosin_=c.getString(c.getColumnIndex(QueryEngine.Colm_nosin));
				String cabang_=c.getString(c.getColumnIndex(QueryEngine.colm_cabang));
				String saldo_=c.getString(c.getColumnIndex(QueryEngine.Colm_saldo));
				uu = new Unit(nopol_, mobil_, lesing_, ovd_, saldo_, cabang_, nama_, noka_, nosin_);
			} catch (Exception e) {}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if(uu==null){
				Toast.makeText(getActivity(), "Data tidak Ditemukan",1).show();
				return;
			}
			Intent i=new Intent(getActivity(), EditActivity.class);
			MainActivity.mselected=uu;
			i.putExtra("nosinfocus", true);
			startActivity(i);
			super.onPostExecute(result);
		}
		
	}
	
	@Override
	public void onResume() {
		//MainActivity.afterinfo=this;
		MainActivity.apptitle.setText(R.string.app_name);
		if(MainActivity.lastquery.equals("s")){
			MainActivity.lastquery="";
			//adapt.notifyDataSetChanged();
			query.setText(query.getText().toString());
			query.setSelection(query.getText().length());
			super.onResume();
			return;
		}
		dataset.clear();
		query.setText("");
		list.setSelectionAfterHeaderView();
		engine.populateNosin("",0,limit);
		adapt=new NosinAdapter(getActivity(), dataset);
		list.setAdapter(adapt);
		super.onResume();
	}
	@Override
	public void onDestroyView() {
		MainActivity.lastquery="";
		super.onDestroyView();
	}
	
}
