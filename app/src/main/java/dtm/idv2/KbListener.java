package dtm.idv2;

import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.app.*;
import android.view.*;
import dtm.idv2.Utils.*;
import android.graphics.*;
public class KbListener implements OnKeyboardActionListener
{
	Activity mActivity;
	boolean kstate=false;
	public KbListener(Activity act){
		mActivity=act;
	}
	@Override
	public void onPress(int p1) {
		// TODO: Implement this method
	}

	@Override
	public void onRelease(int p1) {
		// TODO: Implement this method
	}

	@Override
	public void onKey(int primaryCode, int[] keyCodes) {
		long eventTime = System.currentTimeMillis();
		KeyEvent event = new KeyEvent(eventTime, eventTime,
									  KeyEvent.ACTION_DOWN, primaryCode, 0, 0, 0, 0,
									  KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE);
		
		mActivity.dispatchKeyEvent(event);
	}

	@Override
	public void onText(CharSequence p1) {
		// TODO: Implement this method
	}

	@Override
	public void swipeLeft() {
		// TODO: Implement this method
	}

	@Override
	public void swipeRight() {
		// TODO: Implement this method
	}

	@Override
	public void swipeDown() {
		// TODO: Implement this method
	}

	@Override
	public void swipeUp() {
		// TODO: Implement this method
	}
	
}
