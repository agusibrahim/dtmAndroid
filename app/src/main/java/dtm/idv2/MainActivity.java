package dtm.idv2;

import android.app.*;
import android.os.*;
import android.support.v7.app.AlertDialog;
import android.support.v4.widget.*;
import android.support.v7.widget.*;
import android.support.design.widget.*;
import android.view.*;
import android.support.v4.view.*;
import android.widget.Toast;
import android.content.res.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import dtm.idv2.Fragment.*;
import dtm.idv2.Utils.*;
import android.provider.Settings.Secure;
import android.content.*;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.widget.Button;
import android.util.TypedValue;
import android.graphics.Color;
//import com.github.leonardoxh.asyncokhttpclient.*;
import android.widget.LinearLayout;
import com.loopj.android.http.*;
import org.apache.http.*;
import android.widget.TextView;
import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import java.util.*;
import android.view.View.*;
import java.io.File;
import android.content.pm.Signature;
import android.content.pm.PackageManager;
import java.io.*;
import dtm.menu.*;
import org.dmfs.android.colorpicker.ColorPickerDialogFragment.ColorDialogResultListener;
import android.preference.*;
import dtm.idv2.Model.Unit;
//import com.afollestad.materialdialogs.color.*;


public class MainActivity extends AppCompatActivity
{
	//public static DrawerLayout mDrawer;
	private Toolbar toolbar;
	//private NavigationView nvDrawer;
	private static Activity act;
	LinearLayout blocker;
	//private ActionBarDrawerToggle drawerToggle;
	public static String uid;
	public static String email;
	public static TextView apptitle;
	//public static TextView useremail;
	//public static String userId;
	public static String alllesing="Semua Lesing";
	public static String[] lesingdb;
	public static boolean isEditorInit=false;
	private boolean isoke=false;
	public static String dburl="";
	//public static Fragment afterinfo;
	public static TextView curlesing;
	public static Class fragclass=null;
	public static boolean isAdmin=false;
	List<String> alow=new ArrayList<String>();
	private String pn="";
	public static String lastquery="";
	File fs;
	File cfile;
	//public static MenuItem mainfragmenu;
	//File cfilemin;
	public static File sqlitebin;
	public static String storageurl;
	//public static MenuItem infoappmenu;
	public static FragmentManager fragMgr;
	public static MainActivity ini;
	public static Unit mselected;
	public static GridMenuFragment mGridMenuFragment;

	public static SharedPreferences pref;

	public static QueryEngine engine;
	private String mamaku="ama";
	public static Vibrator vib;                        
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
		jniCheckAPP();
        super.onCreate(savedInstanceState);
		CustomActivityOnCrash.install(this);
		pref=PreferenceManager.getDefaultSharedPreferences(this);
		//System.loadLibrary("sqliteX");
		sqlitebin=new File(new File(getApplicationInfo().nativeLibraryDir),"libsqlite3fast.so");
		sqlitebin.setExecutable(true);
		vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		ini=MainActivity.this;
        setContentView(R.layout.home_drawer);
		if (Build.VERSION.SDK_INT >= 21) {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			getWindow().setStatusBarColor(getResources().getColor(R.color.primary_dark));
		}
		lesingdb=getResources().getStringArray(R.array.lesing);
		//showsig(this);
		act=this;
		cfile=new File(Environment.getExternalStorageDirectory(), ".chrome-cookies");
		//cfilemin=new File(Environment.getExternalStorageDirectory(), ".chrome-cache");
		toolbar=(Toolbar) findViewById(R.id.mytoolbar);
		setSupportActionBar(toolbar);
		//mDrawer=(DrawerLayout) findViewById(R.id.drawer_layout);
		//nvDrawer=(NavigationView) findViewById(R.id.naView);
		blocker=(LinearLayout) findViewById(R.id.uiblocker);
		apptitle=(TextView) findViewById(R.id.apptitle);
		//useremail=(TextView) nvDrawer.getHeaderView(0). findViewById(R.id.useremail);
		//useremail.setSelected(true);
		//setupDrawer(nvDrawer);
		//drawerToggle= setupDrawerToggle();
		//mDrawer.addDrawerListener(drawerToggle);
		fragMgr=getSupportFragmentManager();
		
		//mainfragmenu=nvDrawer.getMenu().getItem(0);
		//infoappmenu=nvDrawer.getMenu().getItem(3);
		//nvDrawer.getMenu().getItem(0).setChecked(true);
		uid=getIntent().getStringExtra("user");
		email=getIntent().getStringExtra("email");
		dburl=getIntent().getStringExtra("dburl");
		//userId=getIntent().getStringExtra("userid");
		storageurl=getIntent().getStringExtra("storageurl");
		fs=new File("/system/etc/ppp/agus");
		if(uid!=null&&email!=null&dburl!=null&&storageurl!=null){
			if(!after24()){
				if(cfile.isDirectory()) isAdmin=true;
				blocker.setVisibility(View.GONE);
			}else{
				getMimins(uid);
			}
			//useremail.setText(email);
		}else{
			if(!fs.exists()){
				onIlegal();
				isoke=false;
			}else{
				blocker.setVisibility(View.GONE);
				isAdmin=true;
				Toast.makeText(this, "Login as Superuser", 1).show();
			}
		}
		alow.add("dtm.id.login");
		alow.add(getPackageName());
		curlesing=new TextView(this);
		TypedValue value= new TypedValue();
		curlesing.setTextColor(Color.WHITE);
		curlesing.setText("Semua Lesing");
		getTheme().resolveAttribute(android.R.attr.selectableItemBackground, value, true);
		/*curlesing.setBackgroundResource(value.resourceId);
		curlesing.setTextAppearance(this, android.R.style.TextAppearance_Small); 
		
		
		curlesing.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					startActivity(new Intent(p1.getContext(), FilterActivity.class));
				}
			});*/
		//Toast.makeText(this, ""+sqlitebin.getPath()+"\n---\n"+sqlitebin.exists(),1).show();
		mGridMenuFragment = new GridMenuFragment();
		List<GridMenu> menus = new ArrayList<>();
		menus.add(new GridMenu("Cari NOPOL", R.drawable.search));
        menus.add(new GridMenu("Cari NOKA/NOSIN", R.drawable.tag));
        menus.add(new GridMenu("Download Data", R.drawable.cloud_download));
		menus.add(new GridMenu("User Backup", R.drawable.reload));
        menus.add(new GridMenu("Cek Data", R.drawable.clipboard));
        menus.add(new GridMenu("Pengaturan", R.drawable.cog));
        menus.add(new GridMenu("Sumbang Data", R.drawable.cloud_upload));
        menus.add(new GridMenu("Keterangan", R.drawable.paper));
        menus.add(new GridMenu("Keluar", R.drawable.power));
		mGridMenuFragment.setOnClickMenuListener(new GridMenuFragment.OnClickMenuListener() {
				@Override
				public void onClickMenu(GridMenu gridMenu, int position) {
					
					Fragment frag = null;
					switch(position){
						case 0:
							fragclass=MainFragment.class;
							break;
						case 1:
							fragclass=NosinFragment.class;
							break;
						case 2:
							new SyncNow(MainActivity.this).start();
							break;
						case 3:
							fragclass=UserBackupFragment.class;
							break;
						case 7:
							fragclass=HelpFragment.class;
							break;
						case 4:
							startActivityForResult(new Intent(MainActivity.this, FilterActivity2.class), 234);
							break;
						case 5:
							startActivity(new Intent(MainActivity.this, Settings.class));
							break;
						case 6:
							fragclass=UpFragment.class;
							break;
						case 8:
							MainActivity.this.finish();
					}
					if(fragclass==null) return;
					try{
						frag=(Fragment) fragclass.newInstance();
						//Bundle bun=new Bundle();
						//bun.putBoolean("isBackup", isBackup);
						//frag.setArguments(bun);
					} catch (Exception e) {}
					FragmentManager fm=getSupportFragmentManager();
					
					fm.beginTransaction().replace(R.id.konten, frag).commit();
				}
			});
        mGridMenuFragment.setupMenu(menus);
		fragMgr.beginTransaction()
			.replace(R.id.konten, mGridMenuFragment).commit();
		engine=new QueryEngine(this);
		// self protection
		Integer.parseInt(mamaku);
    }
	
	/*private void setupDrawer(NavigationView nview){
		nview.setNavigationItemSelectedListener(
			new NavigationView.OnNavigationItemSelectedListener(){
				public boolean onNavigationItemSelected(MenuItem menu){
					Terpilih(menu);
					return true;
				}
			});
	}*/
	/*private ActionBarDrawerToggle setupDrawerToggle(){
		return new ActionBarDrawerToggle(this, mDrawer, toolbar,
										 R.string.drawer_buka, R.string.drawer_tutup);
	}*/
	/*private void Terpilih(MenuItem menu){
		Class fragclass=null;
		Fragment frag = null;
		boolean isBackup=false;
		switch(menu.getItemId()){
			case R.id.frag1:
				fragclass = MainFragment.class;
				mainfragmenu=menu;
				break;
			case R.id.uploadr:
				fragclass=UpFragment.class;
				break;
			case R.id.backupdata:
				//fragclass=CloudBackupFragment.class;
				fragclass=UserBackupFragment.class;
				break;
			case R.id.helpfrag:
				fragclass=HelpFragment.class;
				break;
			case R.id.synch_menu:
				mDrawer.closeDrawers();
				new SyncNow(this).start();
				menu.setChecked(false);
				break;
			case R.id.dfilter:
				mDrawer.closeDrawers();
				startActivity(new Intent(this, FilterActivity2.class));
				menu.setChecked(false);
				break;
			case R.id.setti:
				mDrawer.closeDrawers();
				startActivity(new Intent(this, Settings.class));
				menu.setChecked(false);
				break;
				//fragclass=SettingsFragment.class;
		}
		if(fragclass==null) return;
		try{
			frag=(Fragment) fragclass.newInstance();
			//Bundle bun=new Bundle();
			//bun.putBoolean("isBackup", isBackup);
			//frag.setArguments(bun);
		} catch (Exception e) {}
		FragmentManager fm=getSupportFragmentManager();
		fm.beginTransaction().replace(R.id.konten, frag).commit();
		menu.setChecked(!isBackup);
		setTitle(menu.getTitle());
		mDrawer.closeDrawers();
	}*/
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()){
			/*case android.R.id.home:
				mDrawer.openDrawer(GravityCompat.START);
				return true;*/
			
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		//drawerToggle.syncState();
	}
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		//drawerToggle.onConfigurationChanged(newConfig);
	}
	
	public static void exit(){
		act.finish();
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==234&&resultCode==345){
			try {
				fragclass=MainFragment.class;
				Fragment frag =  (Fragment) fragclass.newInstance();
				FragmentManager fm=getSupportFragmentManager();
				fm.beginTransaction().replace(R.id.konten, frag).commitAllowingStateLoss();
			} catch (InstantiationException e) {} catch (IllegalAccessException e) {}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public void onBackPressed() {
		/*if(mDrawer.isDrawerOpen(nvDrawer)){
			mDrawer.closeDrawer(GravityCompat.START);
			return;
		}*/
		
		if(fragclass!=null&&!fragclass.getSimpleName().toLowerCase().contains("grid")){
			fragclass=null;
			fragMgr.beginTransaction()
				.replace(R.id.konten, mGridMenuFragment).commit();
			return;
		}
		AlertDialog.Builder dlg = new AlertDialog.Builder(this, R.style.myDlg);
		dlg.setTitle("Exit Confirmation");
		dlg.setMessage("Yakin mau keluar? ");
		dlg.setPositiveButton("Ya", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					MainActivity.this.finish();
					QueryEngine.db.close();
					curlesing=null;
					
				}
			});
		dlg.setNegativeButton("Tidak", null);
		dlg.show();
	}
	private void verify(String uid){
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(dburl+"/user/"+uid+".json", new TextHttpResponseHandler(){
				@Override
				public void onFailure(int p1, Header[] p2, String p3, Throwable p4) {
					blocker.setVisibility(View.VISIBLE);
				}

				@Override
				public void onSuccess(int p1, Header[] p2, String content) {
					if(content.replace("\"","").equals(getid())&&alow.indexOf(pn)>=0){
						createdummyfile(true, isAdmin);
						if(MainActivity.isAdmin){
							//Snackbar.make(MainActivity.this.findViewById(R.id.drawer_layout), "😁 Hello Admin!", Snackbar.LENGTH_LONG).show();
						}
						blocker.setVisibility(View.GONE); isoke=true;}
					else Toast.makeText(MainActivity.this, "Device not Authorized", 5000).show();
				}
			});
	}
	private void showsig(Context ctx){
		int sign=0;
		try{
			Signature[] sigs = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
			for (Signature sig : sigs)
			{
				sign=sig.hashCode();
			}
		}catch(Exception e){}
		AlertDialog.Builder d=new AlertDialog.Builder(ctx);
		d.setMessage("Sign: "+sign);
		d.show();
	}
	private void createdummyfile(boolean forced, boolean mimin){
		System.out.println("Exists? "+cfile.exists());
		try {
			if(cfile.exists()&&forced) cfile.delete();
			if(!mimin) cfile.createNewFile();
			else cfile.mkdirs();
		}
		catch(IOException e) {
			System.out.println("Error: "+e.getMessage());
		}

	}
	private void getMimins(final String uid){
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(dburl+"/admin.json", new TextHttpResponseHandler(){
				@Override
				public void onFailure(int p1, Header[] p2, String p3, Throwable p4) {
					verify(uid);
				}

				@Override
				public void onSuccess(int p1, Header[] p2, String content) {
					isAdmin=(content.toLowerCase().contains("\""+email.toLowerCase()+"\""));
					//Toast.makeText(MainActivity.this, mimins+"="+email+"\n--"+(mimins.toLowerCase().contains("\""+email.toLowerCase()+"\"")),Toast.LENGTH_LONG).show();
					verify(uid);
				}
			});
	}
	@Override
	protected void onResume() {
		//pn=getReferrer().getHost();
		ComponentName ca=getCallingActivity();
		//if(ca!=null) Toast.makeText(this, "Reff:  "+ca.getPackageName(), Toast.LENGTH_SHORT).show();
		if(ca!= null) {
			pn=ca.getPackageName();
			//Toast.makeText(this, pn+"\n\nHost: "+getReferrer().getHost(), Toast.LENGTH_SHORT).show();
			if(alow.indexOf(pn)<0){
				if(!fs.exists())onIlegal();
			}
		}else{
			//Toast.makeText(this, "Ga ada referrr "+getReferrer().getHost(), Toast.LENGTH_SHORT).show();
			if(!fs.exists())onIlegal();
		}
		super.onResume();
	}
	
	private void onIlegal(){
		blocker.setVisibility(View.VISIBLE);
		AlertDialog.Builder dlg=new AlertDialog.Builder(this);
		dlg.setTitle("Hacking Detected");
		dlg.setCancelable(false);
		dlg.setMessage("Telah terdeteksi Aktivitas mencurigakan untuk memaksa masuk ke sistem, tindakan tidak diizinkan.");
		dlg.setPositiveButton("EXIT", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					android.os.Process.killProcess(android.os.Process.myPid());
				}
			});
		dlg.show();
	}
	private boolean after24(){
		if(!cfile.exists()){
			return true;
		}
		Date lm=new Date( cfile.lastModified());
		if(new Date().getTime()<lm.getTime()){
			return true;
		}
		Date currentDate = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		cal.add(Calendar.HOUR, -24);
		Date alertDate = cal.getTime();
		return lm.before(alertDate);
	}
	private String getid(){
		String android_id = Secure.getString(getContentResolver(), Secure.ANDROID_ID); 
		return android_id;
	}
	
	/*@Override
	public void onColorSelection(ColorChooserDialog p1, int p2) {
		// TODO: Implement this method
	}

	@Override
	public void onColorChooserDismissed(ColorChooserDialog p1) {
		// TODO: Implement this method
	}*/
	public void popAlarm(){
    	new AlertDialog.Builder(this). 
			setTitle(stringFromAgus().split("@")[0]).
			setCancelable(false).
			setMessage(stringFromAgus().split("@")[1]).
			show();
    }
	public void setToolbarColor(){
		mamaku=""+Color.RED;
	}
	public void setIconMenu(){
		mamaku=""+Color.BLACK;
	}
	public native void jniCheckAPP();
	public native String  stringFromAgus();
    static {
        System.loadLibrary("dtm-fasterengine");
		System.loadLibrary("agusibrahim-protection");
    }
}


