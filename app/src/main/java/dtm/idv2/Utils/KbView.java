package dtm.idv2.Utils;

import android.inputmethodservice.*;
import android.content.*;
import android.util.*;
import android.graphics.*;
import java.util.*;

import android.inputmethodservice.Keyboard.Key;
import android.graphics.drawable.*;
import dtm.idv2.*;
import android.inputmethodservice.KeyboardView.*;
import android.view.*;
import android.app.*;
import dtm.idv2.Fragment.*;
import android.media.*;
import dtm.idv2.R;

public class KbView extends KeyboardView
{
	private Context mContext;
    private int heightPixels, tuk1;
    private float density;
    //private static Keyboard mKeyBoard;
	private Keyboard kb;
	public Canvas mets;
	public Key metskey;
	Activity act;
	boolean sif=false;
	List<Integer> numerik=new ArrayList<Integer>();

	private SoundPool sp;
	private boolean vibr;
    public KbView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        heightPixels = mContext.getResources().getDisplayMetrics().heightPixels;
        density = mContext.getResources().getDisplayMetrics().density;
		sp = new SoundPool(2, AudioManager.STREAM_MUSIC, 100);
		tuk1 = sp.load(context, R.raw.tuk, 1);
		
		android.util.Log.d("dtml", "Kbview created");
    }

    public KbView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
        heightPixels = mContext.getResources().getDisplayMetrics().heightPixels;
        density = mContext.getResources().getDisplayMetrics().density;
		
    }
	public void setlistener(){
		setOnKeyboardActionListener(new myListener());
	}
	@Override
	public void setKeyboard(Keyboard keyboard) {
		kb=keyboard;
		for(int i=7;i<17;i++){
			numerik.add(i);
		}
		
		super.setKeyboard(keyboard);
	}
	public void setActivity(Activity aa){
		act=aa;
	}
	@Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
		// mKeyBoard = KeyboardUtil.getKeyBoardType();
        List<Key> keys = kb.getKeys();

        for (Key key : keys) {
            // 数字键盘的处理
            drawABCSpecialKey(key, canvas);
        }
    }
	private void drawABCSpecialKey(Key key, Canvas canvas) {
        //TODO 待添加特殊处理

        if (key.codes[0] == 67) {
            drawKeyBackgroundd(R.drawable.btn_yellow, canvas, key);
            drawText(canvas, key, Color.WHITE);
        }
		else if (key.codes[0] == 59) {
            drawKeyBackgroundd(R.drawable.btn_yellow, canvas, key);
            drawText(canvas, key, Color.WHITE);
        }
		else if (key.codes[0] == 69) {
            drawKeyBackgroundd(R.drawable.btn_yellow, canvas, key);
            drawText(canvas, key, Color.WHITE);
        }
        else if (key.codes[0] == 62||key.codes[0]==100) {
            drawKeyBackgroundd(R.drawable.btn_yellow, canvas, key);
			drawText(canvas, key, Color.WHITE);
        }
		else if(numerik.indexOf(key.codes[0])>=0){
			drawKeyBackgroundd(R.drawable.btn_123, canvas, key);
			drawText(canvas, key, Color.WHITE);
		}
		else{
			drawKeyBackgroundd(R.drawable.btn_abc, canvas, key);
			drawText(canvas, key, Color.BLACK);
		}

    }
	public void drawKeyBackgroundd(int drawableId, Canvas canvas, Key key) {

        Drawable npd =  mContext.getResources().getDrawable(
			drawableId);
        int[] drawableState = key.getCurrentDrawableState();
        if (key.codes[0] != 0) {
            npd.setState(drawableState);
        }
        npd.setBounds(key.x, key.y, key.x + key.width, key.y
					  + key.height);
		if(key.codes[0]==670||key.codes[0]==671){
			npd=new ColorDrawable(Color.TRANSPARENT);
		}
        npd.draw(canvas);
    }
	private void drawText(Canvas canvas, Key key, int colr) {
        Rect bounds = new Rect();
        Paint paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(MainActivity.pref.getInt("kbfsize", 50));
		
		if(key.codes[0]==67||key.codes[0]==62||key.codes[0]==59||key.codes[0]==100){
			paint.setTypeface(Typeface.createFromAsset(act.getAssets(), "icomoon.ttf"));
		}else paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setAntiAlias(true);
		//paint.setColor(colr);
        if (key.label != null) {
			paint.setColor(colr);
			paint.getTextBounds(key.label.toString(), 0, key.label.toString()
								.length(), bounds);
			canvas.drawText(key.label.toString(), key.x + (key.width / 2),
							(key.y + key.height / 2) + bounds.height() / 2, paint);
		}
    }

	public class myListener implements OnKeyboardActionListener
	{

		@Override
		public void onPress(int p1) {
			// TODO: Implement this method
		}

		@Override
		public void onRelease(int p1) {
			// TODO: Implement this method
		}

		@Override
		public void onKey(int primaryCode, int[] keyCodes) {
			long eventTime = System.currentTimeMillis();
			KeyEvent event = new KeyEvent(eventTime, eventTime,
										  KeyEvent.ACTION_DOWN, primaryCode, 0, 0, 0, 0,
										  KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE);
			if(primaryCode==670||primaryCode==671) return;
			int vibra=MainActivity.pref.getInt("vibra", 0);
			if(vibra>0)// performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
			{
				MainActivity.vib.vibrate(vibra);
			}
			float vol=(float) MainActivity.pref.getInt("keyvol",50)*1/100;
			sp.play(tuk1, vol, vol, 0, 0, 1);   
			if(primaryCode==59){
				act.startActivity(new Intent(act, Settings.class));
				//MainFragment.query.getText().insert(MainFragment.query.getSelectionStart(), "~");
				return;
			}else if(primaryCode==100){
				act.startActivity(new Intent(act, FilterActivity2.class));
				/*MainFragment.mselected=null;
				act.startActivity(new Intent(act, EditActivity.class));*/
				//MainFragment.query.getText().insert(MainFragment.query.getSelectionStart(), "+");
				return;
			}else if(primaryCode==62){
				MainActivity.fragMgr.beginTransaction()
					.replace(R.id.konten, new HelpFragment()).commit();
				//MainActivity.infoappmenu.setChecked(true);
				//getSupportFragmentManager().beginTransaction()
				//	.replace(R.id.konten, new MainFragment()).commit();
				//MainFragment.query.getText().insert(MainFragment.query.getSelectionStart(), "~");
				return;
			}
			act.dispatchKeyEvent(event);
		}

		@Override
		public void onText(CharSequence p1) {
			// TODO: Implement this method
		}

		@Override
		public void swipeLeft() {
			// TODO: Implement this method
		}

		@Override
		public void swipeRight() {
			// TODO: Implement this method
		}

		@Override
		public void swipeDown() {
			setVisibility(View.GONE);
		}

		@Override
		public void swipeUp() {
			// TODO: Implement this method
		}

	}
}
