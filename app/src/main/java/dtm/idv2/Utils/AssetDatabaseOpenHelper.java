package dtm.idv2.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
//import org.sqlite.database.sqlite.*;
import io.requery.android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteDatabase;

public class AssetDatabaseOpenHelper {

    private String DB_NAME="";

    private Context context;
	public static File dbFile;
    public AssetDatabaseOpenHelper(Context context, String dbname) {
        this.context = context;
		this.DB_NAME=dbname;
    }

    public SQLiteDatabase openDatabase() {
        dbFile = context.getDatabasePath(DB_NAME.replace(".jpg", ".db"));
		
		File par=dbFile.getParentFile();
		if(!par.exists()) par.mkdirs();
        if (!dbFile.exists()) {
            try {
                copyDatabase(dbFile);
            } catch (IOException e) {
                throw new RuntimeException("Error creating source database", e);
            }
        }

        return SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
    }

    private void copyDatabase(File dbFile) throws IOException {
        InputStream is = context.getAssets().open(DB_NAME);
        OutputStream os = new FileOutputStream(dbFile);

        byte[] buffer = new byte[1024];
        while (is.read(buffer) > 0) {
            os.write(buffer);
        }

        os.flush();
        os.close();
        is.close();
    }

}
