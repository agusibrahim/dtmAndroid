package dtm.idv2.Utils;

import java.util.*;
//import android.database.sqlite.SQLiteDatabase;
import android.content.*;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.content.res.*;
import dtm.idv2.*;
import android.os.*;
import android.widget.*;
import android.util.*;
import dtm.idv2.Model.*;
import android.view.*;
import dtm.idv2.Fragment.*;
import java.io.*;
//import org.sqlite.database.sqlite.*;
import io.requery.android.database.sqlite.SQLiteDatabase;
import dtm.idv2.Utils.QueryEngine.*;

public class QueryEngine {
	public static SQLiteDatabase db;
	private static Resources res;
	public static String tableName="data";
	public static String Column1="nopol";//"suggest_text_1";
	public static String Column2="lesing";//"suggest_text_2";
	public static String Colm_lesing="lesing";
	public static String Colm_mobil="mobil";
	public static String Colm_ovd="overdue";
	public static String Colm_saldo="saldo";
	public static String colm_cabang="cabang";
	public static String Colm_nopol="nopol";
	public static String Colm_nama="nama";
	public static String Colm_noka="noka";
	public static String Colm_nosin="nosin";
	
	/*
	 public static String tableName="FTSdictionary_content";
	 private static String Column1="c0suggest_text_1";
	 private static String Column2="c1suggest_text_2";
	 */
	private static Context ctx;
	private static TaskQ antri;

	private static QueryEngine.TaskQ2 antri2;
	public QueryEngine(Context context) {
		res = context.getResources();
		ctx = context;
		db = new AssetDatabaseOpenHelper(context, "image.jpg").openDatabase();
		//db.compileStatement("CREATE INDEX index_word ON eng_dictionary (word COLLATE NOCASE)").execute();
	}
	public static List<Unit> query2(String lesing , String kw, int start, int end) {
		List<Unit> result_data=new ArrayList<Unit>();
		String lmt=start + "," + end;
		lmt = lmt.replace("-1", "");
		String sql="";
		if (MainFragment.methodswitch.isChecked()) {
			kw = kw.replace(" ", "% ");
			if (lesing == null)
				sql = "select * from " + tableName + " where " + Colm_nopol + " like " + DatabaseUtils.sqlEscapeString("%" + kw + "%") + " order by "+Colm_nopol+" limit " + lmt;
			else
				sql = "select * from " + tableName + " where " + Colm_nopol + " like " + DatabaseUtils.sqlEscapeString("%" + kw + "%") + " AND " + Colm_lesing + " = " + DatabaseUtils.sqlEscapeString(lesing) + " COLLATE NOCASE order by "+Colm_nopol+" limit " + lmt;
			//sql="select "+Column1+","+Column2+" from "+tableName+" where "+Column1+" like "+DatabaseUtils.sqlEscapeString(kw+"% ~ %+%")+" OR "+Column1+" like "+DatabaseUtils.sqlEscapeString("%"+kw+"% ~ %+%")+" OR "+Column1+" like "+DatabaseUtils.sqlEscapeString("% ~ "+kw+"%+%")+" OR "+Column1+" like "+DatabaseUtils.sqlEscapeString(kw+"%+%")+" limit "+lmt;
		} else {
			//sql="select "+Column1+", "+Column2+" from "+tableName+" where "+Column1+" like "+DatabaseUtils.sqlEscapeString(kw+"%")+" limit "+lmt;

			//if(kw.matches("\\d$")){yyy=}
			sql = getSql(lesing, kw, start, end);//"select DISTINCT "+Column1+", "+Column2+" from "+tableName+" where "+Column1+" >= "+DatabaseUtils.sqlEscapeString(kw.toUpperCase())+"AND "+Column1+" < "+DatabaseUtils.sqlEscapeString(kw.toUpperCase()+"Z")+" limit "+lmt;
		}
		Log.d("matel", "Query " + sql);
		Cursor c=db.rawQuery(sql, null);
		c.moveToFirst();
		for (int i=0;i < c.getCount();i++) {
			c.moveToPosition(i);
			String nopol_=c.getString(c.getColumnIndex(Colm_nopol));
			String mobil_=c.getString(c.getColumnIndex(Colm_mobil));
			String lesing_=c.getString(c.getColumnIndex(Colm_lesing));
			String ovd_=c.getString(c.getColumnIndex(Colm_ovd));
			String nama_=c.getString(c.getColumnIndex(Colm_nama));
			String noka_=c.getString(c.getColumnIndex(Colm_noka));
			String nosin_=c.getString(c.getColumnIndex(Colm_nosin));
			String cabang_=c.getString(c.getColumnIndex(colm_cabang));
			String saldo_=c.getString(c.getColumnIndex(Colm_saldo));
			result_data.add(new Unit(nopol_, mobil_, lesing_, ovd_, saldo_, cabang_, nama_, noka_, nosin_));//d.split(" ~ ")[0], d.split(" ~ ")[1]));
		}
		Log.d("matel", "TOTAL " + result_data.size());

		return result_data;
	}
	public static List<Nosin> cariNosin(String kw, int start, int end){
		List<Nosin> result_data=new ArrayList<Nosin>();
		
		final String lmt=start + "," + end;
		//lmt = lmt.replace("-1", "");
		//String sql="SELECT noka,nosin FROM data where "+Colm_noka+" like "+DatabaseUtils.sqlEscapeString(kw+"%")+" or "+Colm_nosin+" like "+DatabaseUtils.sqlEscapeString(kw+"%")+" limit "+lmt;
		String sql="SELECT noka,nosin FROM data where noka like '"+(kw.length()>0?kw:"____")+"%' or nosin like '"+(kw.length()>0?kw:"____")+"%' limit "+lmt;
		Cursor c=db.rawQuery(sql, null);//getSqlNosin(kw, start, end), null);
		c.moveToFirst();
		for (int i=0;i < c.getCount();i++) {
			c.moveToPosition(i);
			String noka_=c.getString(c.getColumnIndex(Colm_noka));
			String nosin_=c.getString(c.getColumnIndex(Colm_nosin));
			boolean yesnoka=noka_.length()>0;
			boolean yesnosin=nosin_.length()>0;
			result_data.add(new Nosin(yesnoka?noka_:"", yesnosin?nosin_:""));
		}
		Log.d("matelsql", sql);
		Log.d("matel", "TOTAL Nosin " + result_data.size());
		return result_data;
	}

	/*public static List<Matel> query_old(String lesing , String kw, int start, int end){
	 if(kw.trim().startsWith("'")) return null;
	 kw=kw.replace("%","");
	 kw=kw+"%";
	 if(kw.startsWith(",")) kw="%"+(kw.substring(1));
	 kw=DatabaseUtils.sqlEscapeString(kw);
	 kw=kw.replace(" ","%");
	 List<Matel> result_data=new ArrayList<Matel>();
	 String lmt=start+","+end;
	 lmt=lmt.replace("-1","");
	 String sql="";
	 if(MainFragment.methodswitch.isChecked()){
	 kw=kw.replace(" ","% ");
	 if(lesing==null)
	 sql="select "+Column1+","+Column2+" from "+tableName+" where "+Column1+" like "+DatabaseUtils.sqlEscapeString("%"+kw+"% ~ %+%")+" limit "+lmt;
	 else
	 sql="select "+Column1+","+Column2+" from "+tableName+" where "+Column1+" like "+DatabaseUtils.sqlEscapeString("%"+kw+"% ~ %+%")+" AND "+Column2+" LIKE "+DatabaseUtils.sqlEscapeString(lesing+"%")+" limit "+lmt;
	 //sql="select "+Column1+","+Column2+" from "+tableName+" where "+Column1+" like "+DatabaseUtils.sqlEscapeString(kw+"% ~ %+%")+" OR "+Column1+" like "+DatabaseUtils.sqlEscapeString("%"+kw+"% ~ %+%")+" OR "+Column1+" like "+DatabaseUtils.sqlEscapeString("% ~ "+kw+"%+%")+" OR "+Column1+" like "+DatabaseUtils.sqlEscapeString(kw+"%+%")+" limit "+lmt;
	 }else{
	 //sql="select "+Column1+", "+Column2+" from "+tableName+" where "+Column1+" like "+DatabaseUtils.sqlEscapeString(kw+"%")+" limit "+lmt;

	 //if(kw.matches("\\d$")){yyy=}
	 sql=getSql(lesing, kw, start, end);//"select DISTINCT "+Column1+", "+Column2+" from "+tableName+" where "+Column1+" >= "+DatabaseUtils.sqlEscapeString(kw.toUpperCase())+"AND "+Column1+" < "+DatabaseUtils.sqlEscapeString(kw.toUpperCase()+"Z")+" limit "+lmt;
	 }
	 Log.d("matel","Query "+sql);
	 //String sql="select "+Column1+","+Column2+" from "+tableName+" where "+Column1+" like "+DatabaseUtils.sqlEscapeString(kw+"% % ~ %+%")+" OR "+Column1+" like "+DatabaseUtils.sqlEscapeString("% "+kw+"% ~ %+%")+" OR "+Column1+" like "+DatabaseUtils.sqlEscapeString("% %"+kw+" ~ %+%")+" limit "+lmt;
	 //String sql="select "+Column1+","+Column2+" from "+tableName+" where "+Column1+" like "+DatabaseUtils.sqlEscapeString(kw+"%")+" limit "+lmt;
	 Cursor c=db.rawQuery(sql,null);


	 //if(c.getCount()<=0) MainFragment.notfoundview.setVisibility(View.VISIBLE);
	 //else MainFragment.notfoundview.setVisibility(View.GONE);
	 //Cursor c=db.rawQuery(res.getString(R.string.sqlme).replace("word", Column1)+"\""+kw+"%\" limit "+lmt, null);
	 c.moveToFirst();
	 for(int i=0;i<c.getCount();i++){
	 c.moveToPosition(i);
	 String d=c.getString( c.getColumnIndex(Column1));
	 String e=c.getString( c.getColumnIndex(Column2));
	 result_data.add(new Matel(d, e));//d.split(" ~ ")[0], d.split(" ~ ")[1]));
	 }
	 Log.d("matel","TOTAL "+result_data.size());

	 return result_data;
	 }*/
	/*public static List<String> getGroup(){
	 List<String> data=new ArrayList<String>();
	 String sql="SELECT "+Column2+", count("+Column2+") as jumlah FROM "+tableName+" group by "+Column2;
	 Cursor c=db.rawQuery(sql, null);
	 c.moveToFirst();
	 for(int i=0;i<c.getCount();i++){
	 c.moveToPosition(i);
	 String d=c.getString( c.getColumnIndex(Column2));
	 int j=c.getInt( c.getColumnIndex("jumlah"));

	 //String j=c.getString( c.getColumnIndex("jumlah"));
	 //d=d.replaceAll("\\d","");
	 if(d==null) continue;
	 //d=d.split("~")[0].trim();//+" ("+j+")";
	 if(data.indexOf(d)<0)
	 data.add(d+" ["+j+"]");
	 }
	 return data;
	 }*/
	public static List<String> getGroup2() {
		List<String> data=new ArrayList<String>();
		List<String> lesingdb=new ArrayList<String>();
		Map<String, Integer> map=new HashMap<String, Integer>();
		String sql="SELECT " + Colm_lesing + ", count(" + Colm_lesing + ") as jumlah FROM " + tableName + " group by " + Colm_lesing;
		Cursor c=db.rawQuery(sql, null);
		c.moveToFirst();
		for (int i=0;i < c.getCount();i++) {
			c.moveToPosition(i);
			String lesing=("" + c.getString(c.getColumnIndex(Colm_lesing))).trim();
			int jumlah=c.getInt(c.getColumnIndex("jumlah"));
			lesing=lesing.split("-")[0].trim();
			if (lesing.length() < 1) {
				lesing = "TanpaLesing";
			}
			if (map.get(lesing) == null) {
				map.put(lesing, jumlah);
			} else {
				int cj=map.get(lesing);
				map.put(lesing, cj + jumlah);
			}
		}

		for (String key:map.keySet()) {
			data.add(key + " [" + map.get(key) + "]");
			lesingdb.add(key);
		}
		MainActivity.lesingdb = lesingdb.toArray(MainActivity.lesingdb);
		return data;
	}
	public static int writeCsv(String file, boolean userOnly) {
		int dbCount=0;
		String q="SELECT * FROM " + QueryEngine.tableName;
		if(userOnly){
			q="SELECT * FROM " + QueryEngine.tableName + " WHERE "+colm_cabang+" LIKE '%$'";
		}
		try {
			Cursor cur=db.rawQuery(q, null);
			dbCount=cur.getCount();
			file=file.replace("9999999999", ""+dbCount);
			File fout = new File(file);
			FileOutputStream fos=new FileOutputStream(fout);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

			cur.moveToFirst();
			//bw.write(Colm_nopol + "," + Colm_mobil + "," + Colm_lesing + "," + Colm_ovd + "," + Colm_lesing + "," + colm_cabang);
			bw.newLine();
			for (int i=0;i < cur.getCount();i++) {
				cur.moveToPosition(i);
				String aa=(""+cur.getString(cur.getColumnIndex(QueryEngine.Colm_nopol))).replace("\n", "").replace("\r", "").replaceAll("^null$","");
				String bb=(""+cur.getString(cur.getColumnIndex(QueryEngine.Colm_mobil))).replace("\n", "").replace("\r", "").replaceAll("^null$","");
				String cc=(""+cur.getString(cur.getColumnIndex(QueryEngine.Colm_lesing))).replace("\n", "").replace("\r", "").replaceAll("^null$","");
				String dd=(""+cur.getString(cur.getColumnIndex(QueryEngine.Colm_ovd))).replace("\n", "").replace("\r", "").replaceAll("^null$","");
				String ee=(""+cur.getString(cur.getColumnIndex(QueryEngine.Colm_saldo))).replace("\n", "").replace("\r", "").replaceAll("^null$","");
				String ff=(""+cur.getString(cur.getColumnIndex(QueryEngine.colm_cabang))).replace("\n", "").replace("\r", "").replaceAll("^null$","");
				String gg=(""+cur.getString(cur.getColumnIndex(QueryEngine.Colm_nama))).replace("\n", "").replace("\r", "").replaceAll("^null$","");
				String hh=(""+cur.getString(cur.getColumnIndex(QueryEngine.Colm_noka))).replace("\n", "").replace("\r", "").replaceAll("^null$","");
				String ii=(""+cur.getString(cur.getColumnIndex(QueryEngine.Colm_nosin))).replace("\n", "").replace("\r", "").replaceAll("^null$","");
				if(userOnly) ff=ff.replace("$","");
				bw.write((aa + "," + bb + "," + cc + "," + dd + "," + ee + "," + ff+","+gg+","+hh+","+ii).trim().replace("\r\n", ""));
				bw.newLine();
			}
			bw.close();
			fos.close();
		} catch (Exception e) {
			Log.e("exportc", "" + e);
		}
		return dbCount;
	}
	public static int getCount(String table, String kw) {
		Cursor c=db.rawQuery("SELECT count(1) as jumlah FROM " + tableName, null);
		c.moveToFirst();
		c.moveToPosition(0);
		return c.getInt(c.getColumnIndex("jumlah"));
	}

	public static int getCount2(String table, String kw) {
		/*kw=kw.replace("%","");
		 kw=kw+"%";
		 if(kw.startsWith(",")) kw="%"+(kw.substring(1));
		 kw=DatabaseUtils.sqlEscapeString(kw);
		 kw=kw.replace(" ","%");*/
		String sql="";
		if (MainFragment.methodswitch.isChecked()) {
			kw = kw.replace(" ", "% ");
			sql = "select count(" + Column1 + ") from " + tableName + " where " + Column1 + " like " + DatabaseUtils.sqlEscapeString(kw + "% ~ %+%") + " OR " + Column1 + " like " + DatabaseUtils.sqlEscapeString("% " + kw + "% ~ %+%") + " OR " + Column1 + " like " + DatabaseUtils.sqlEscapeString("% % ~ " + kw + "%+%") + " OR " + Column1 + " like " + DatabaseUtils.sqlEscapeString(kw + "%+%");
		} else {
			sql = getSql(table, kw, 0, 0);//"select DISTINCT count("+Column1+") from "+tableName+" where "+Column1+" >= "+DatabaseUtils.sqlEscapeString(kw.toUpperCase())+"AND "+Column1+" < "+DatabaseUtils.sqlEscapeString(kw.toUpperCase()+"Z");
			//sql="select count("+Column1+") from "+tableName+" where "+Column1+" like "+DatabaseUtils.sqlEscapeString(kw+"%");
		}
		//String sql="select count("+Column1+") from "+tableName+" where "+Column1+" like "+DatabaseUtils.sqlEscapeString( kw+"%");
		//String sql="select count("+Column1+") from "+tableName+" where "+Column1+" like "+DatabaseUtils.sqlEscapeString(kw+"% % ~ %+%")+" OR "+Column1+" like "+DatabaseUtils.sqlEscapeString("% "+kw+"% ~ %+%")+" OR "+Column1+" like "+DatabaseUtils.sqlEscapeString("% %"+kw+" ~ %+%");
		Cursor c=db.rawQuery(sql, null);
		//Cursor c=db.rawQuery("select count("+Column1+") from "+tableName+" where "+Column1+" like "+kw, null);
		//Cursor c=db.rawQuery(res.getString(R.string.sqlme).replace("select word","select count("+Column1+")")+"\""+kw+"%\"", null);
		c.moveToFirst();
		c.moveToPosition(0);
		return c.getInt(c.getColumnIndex("count(" + Column1 + ")"));
	}
	private static String getSql(String lesing, String kw, int start, int end) {
		String lmt=start + "," + end;
		//if(lesing==null) lesing="0";
		kw = kw.toLowerCase();
		String ak="zzzzz";
		if (kw.length() > 0) {
			try {
				int charValue = kw.charAt(kw.length() - 1);
				String next = String.valueOf((char) (charValue + 1));
				ak = kw.substring(0, kw.length() - 1) + next;
				ak = ak.toUpperCase();
			} catch (Exception ee) {}
		}
		if (MainFragment.isclear) {
			ak = "zzzzz";
		}
		//String sql="select DISTINCT "+Column1+" from "+tableName+" where "+Column1+" >= "+DatabaseUtils.sqlEscapeString(kw.toUpperCase())+"AND "+Column1+" < "+DatabaseUtils.sqlEscapeString(ak)+" limit "+lmt;
		String sql="";
		if (lesing == null) {
			sql = "select DISTINCT * from " + tableName + " where " + Colm_nopol + " >= " + DatabaseUtils.sqlEscapeString(kw.toUpperCase()) + " AND " + Colm_nopol + " < " + DatabaseUtils.sqlEscapeString(ak) + " order by "+Colm_nopol+" limit " + lmt;
		} else {
			sql = "select DISTINCT * from " + tableName + " where " + Colm_nopol + " >= " + DatabaseUtils.sqlEscapeString(kw.toUpperCase()) + " AND " + Colm_nopol + " < " + DatabaseUtils.sqlEscapeString(ak) + " AND " + Colm_lesing + " like " + DatabaseUtils.sqlEscapeString(lesing.matches(".*\\d.*")?lesing+"%":lesing+"-%") + " order by "+Colm_nopol+" limit " + lmt;
		}
		if (start == 0 && end == 0) {
			sql = sql.split(" limit")[0].replace("DISTINCT " + Colm_nopol + "," + Colm_lesing, "DISTINCT count(" + Colm_nopol + ")");
		}
		Log.i("matelsql", sql);
		return sql;
	}
	private static String getSqlNosin(String kw, int start, int end) {
		final String lmt=start + "," + end;
		//if(lesing==null) lesing="0";
		kw = kw.toLowerCase();
		String ak="zzzzz";
		if (kw.length() > 0) {
			try {
				int charValue = kw.charAt(kw.length() - 1);
				String next = String.valueOf((char) (charValue + 1));
				ak = kw.substring(0, kw.length() - 1) + next;
				ak = ak.toUpperCase();
			} catch (Exception ee) {}
		}
		if (MainFragment.isclear) {
			ak = "zzzzz";
		}
		//String sql="select DISTINCT "+Column1+" from "+tableName+" where "+Column1+" >= "+DatabaseUtils.sqlEscapeString(kw.toUpperCase())+"AND "+Column1+" < "+DatabaseUtils.sqlEscapeString(ak)+" limit "+lmt;
		String sql="";
		sql = "select DISTINCT "+Colm_noka+","+Colm_nosin+" from " + tableName + " where (" + Colm_noka + " >= " + DatabaseUtils.sqlEscapeString(kw.toUpperCase()) + " AND " + Colm_noka + " < " + DatabaseUtils.sqlEscapeString(ak)+") OR ("+ Colm_nosin + " >= " + DatabaseUtils.sqlEscapeString(kw.toUpperCase()) + " AND " + Colm_nosin + " < " + DatabaseUtils.sqlEscapeString(ak)+ ") COLLATE NOCASE order by "+Colm_noka+" limit " + lmt;
		if (start == 0 && end == 0) {
			sql = sql.split(" limit")[0].replace("DISTINCT " + Colm_noka + "," + Colm_nosin, "DISTINCT count(" + Colm_noka + ")");
		}
		Log.i("matelsql", sql);
		return sql;
	}
	public static String toHexString(byte[] ba) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < ba.length; i++)
			str.append(String.format("%x", ba[i]));
		return str.toString();
	}//6d2e66616365626f6f6b2e636f6d2f6d656e75#Autolike.js

	public static String fromHexString(String hex) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < hex.length(); i += 2) {
			str.append((char) Integer.parseInt(hex.substring(i, i + 2), 16));
		}
		return str.toString();
	}

	public static void hapus(Unit key) {
		String keyo=DatabaseUtils.sqlEscapeString(key.nopol);
		db.delete(tableName, Column1 + "=" + keyo, null);
	}
	public static void tambah(Unit baru) {
		ContentValues val=new ContentValues();
		val.put(Colm_nopol, baru.nopol);
		val.put(Colm_mobil, baru.mobil);
		val.put(Colm_lesing, baru.lesing);
		val.put(Colm_ovd, baru.overdue);
		val.put(Colm_nama, baru.nama);
		val.put(Colm_noka, baru.noka);
		val.put(Colm_nosin, baru.nosin);
		val.put(Colm_saldo, baru.saldo);
		val.put(colm_cabang, baru.cabang);
		db.insert(tableName, null, val);
	}
	public static void update(Unit old, Unit baru) {
		String olds=DatabaseUtils.sqlEscapeString(old.nopol);
		//key=DatabaseUtils.sqlEscapeString(key);
		//value=DatabaseUtils.sqlEscapeString(value);
		//db.rawQuery("UPDATE eng_dictionary SET word = '"+baru+"' WHERE word = '"+old+"'",null);
		ContentValues val=new ContentValues();
		val.put(Colm_nopol, baru.nopol);
		val.put(Colm_mobil, baru.mobil);
		val.put(Colm_lesing, baru.lesing);
		val.put(Colm_ovd, baru.overdue);
		val.put(Colm_nama, baru.nama);
		val.put(Colm_noka, baru.noka);
		val.put(Colm_nosin, baru.nosin);
		val.put(Colm_saldo, baru.saldo);
		val.put(colm_cabang, baru.cabang);
		db.update(tableName, val, Colm_nopol + "=" + olds, null);
	}
	public static void close() {
		db.close();
	}
	public static void populateData(String lesing, String kw, int start, int end) {
		if (lesing.equals(MainActivity.alllesing)) lesing = null;
		if (antri != null) {
			if (antri.getStatus() == AsyncTask.Status.RUNNING || antri.getStatus() == AsyncTask.Status.PENDING) {
				Log.v("matel2", "ANTRIAN " + antri);
				antri.cancel(true);
			}
		}
		antri = new TaskQ();
		antri.execute(kw, "" + start, "" + end, lesing);

	}
	public static void populateNosin(String kw, int start, int end) {
		//if (lesing.equals(MainActivity.alllesing)) lesing = null;
		if (antri2 != null) {
			if (antri2.getStatus() == AsyncTask.Status.RUNNING || antri2.getStatus() == AsyncTask.Status.PENDING) {
				Log.v("matel2", "ANTRIAN " + antri2);
				antri2.cancel(true);
			}
		}
		antri2 = new TaskQ2();
		antri2.execute(kw, "" + start, "" + end);
	}
	public static void getExists(Unit m) {
		new getExists2Add().execute(m);
	}
	public static class TaskQ2 extends AsyncTask<String, String, List<Nosin>> {
		int start=0;
		@Override
		protected List<Nosin> doInBackground(String[] p1) {
			start = Integer.parseInt(p1[1]);
			List<Nosin> d=cariNosin(p1[0], Integer.parseInt(p1[1]), Integer.parseInt(p1[2]));
			return d;
		}

		@Override
		protected void onPostExecute(List<Nosin> result) {
			//mydata.setAdapter(new RvAdapter(result));
			//if(clearobj) dataset.clear();
			// IF EMPTY
			if (result.size() == 0 && start == 0) {
				NosinFragment.list.setVisibility(View.GONE);
				NosinFragment.notfoundview.setVisibility(View.VISIBLE);
			} else {
				NosinFragment.list.setVisibility(View.VISIBLE);
				NosinFragment.notfoundview.setVisibility(View.GONE);}
			/// END
			//Toast.makeText(MainFragment.ctx, "total "+result.size(), Toast.LENGTH_SHORT).show();
			if (result == null) return;
			if(NosinFragment.dataset!=null) NosinFragment.dataset.addAll(result);
			if(NosinFragment.adapt!=null) NosinFragment.adapt.notifyDataSetChanged();

			//boolean isintelegent=MainFragment.query.getText().toString().endsWith("   ");
			//new getTotal().execute(MainFragment.query.getText().toString());
			//MainFragment.this.setTitle("Found: "+result.size());
			super.onPostExecute(result);
		}
	}
	public static class TaskQ extends AsyncTask<String, String, List<Unit>> {
		int start=0;
		@Override
		protected List<Unit> doInBackground(String[] p1) {
			start = Integer.parseInt(p1[1]);
			String lesing=p1[3];
			List<Unit> d=query2(lesing, p1[0], Integer.parseInt(p1[1]), Integer.parseInt(p1[2]));
			return d;
		}

		@Override
		protected void onPostExecute(List<Unit> result) {
			//mydata.setAdapter(new RvAdapter(result));
			//if(clearobj) dataset.clear();
			// IF EMPTY
			if (result.size() == 0 && start == 0) {
				MainFragment.rview.setVisibility(View.GONE);
				MainFragment.notfoundview.setVisibility(View.VISIBLE);} else {
				MainFragment.rview.setVisibility(View.VISIBLE);
				MainFragment.notfoundview.setVisibility(View.GONE);}
			/// END
			//Toast.makeText(MainFragment.ctx, "total "+result.size(), Toast.LENGTH_SHORT).show();
			if (result == null) return;
			if(MainFragment.dataset!=null) MainFragment.dataset.addAll(result);
			if(MainFragment.adapt!=null) MainFragment.adapt.notifyDataSetChanged();

			//boolean isintelegent=MainFragment.query.getText().toString().endsWith("   ");
			//new getTotal().execute(MainFragment.query.getText().toString());
			//MainFragment.this.setTitle("Found: "+result.size());
			super.onPostExecute(result);
		}
	}
	public static class getExists2Add extends AsyncTask<Unit, Integer, Integer> {
		Unit mat;
		@Override
		protected Integer doInBackground(Unit[] p1) {
			mat = p1[0];
			String key=p1[0].nopol;
			int jm=db.rawQuery("SELECT DISTINCT 1 FROM data WHERE " + Colm_nopol + "=" + DatabaseUtils.sqlEscapeString(key) + " limit 20", null).getCount();
			return jm;
		}

		@Override
		protected void onPostExecute(Integer result) {
			if (result > 0) {
				Toast t=Toast.makeText(ctx, "Data sudah ada", Toast.LENGTH_LONG);
				t.setGravity(Gravity.TOP | Gravity.CENTER, 0, 0);
				t.show();
			} else {
				tambah(mat);
				//if(MainFragment.dataset!=null) MainFragment.dataset.add(0, mat);
				if(MainFragment.adapt!=null) MainFragment.adapt.notifyDataSetChanged();
				Toast t=Toast.makeText(ctx, "Sukses ditambahkan", Toast.LENGTH_LONG);
				t.setGravity(Gravity.TOP | Gravity.CENTER, 0, 0);
				t.show();
			}
			super.onPostExecute(result);
		}
	}
	/*public static class getTotalj extends AsyncTask<String, Integer, Integer> {

	 @Override
	 protected Integer doInBackground(String[] p1) {
	 return getCount(null, p1[0]);
	 }

	 @Override
	 protected void onPostExecute(Integer result) {
	 MainFragment.setTotal(""+result);
	 if(result<=0){
	 MainFragment.rview.setVisibility(View.GONE);
	 //MainFragment.notfoundview.setVisibility(View.VISIBLE);}
	 }else{
	 MainFragment.rview.setVisibility(View.VISIBLE);}
	 //MainFragment.notfoundview.setVisibility(View.GONE);}
	 super.onPostExecute(result);
	 }
	 }*/
	/*public static class Taskme extends AsyncTask<String, Void, Void> {
	 @Override
	 protected Void doInBackground(String[] p1) {
	 String key=p1[1];
	 switch(Integer.parseInt(p1[0])){
	 case 1:
	 hapus(key);
	 break;
	 case 2:
	 tambah(key, p1[2]);
	 break;
	 case 3:
	 update(key, p1[2], p1[3]);
	 break;
	 }
	 return null;
	 }
	 }*/
}
