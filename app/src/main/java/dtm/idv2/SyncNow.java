package dtm.idv2;
import dtm.idv2.Fragment.*;
import android.widget.*;
import android.content.Context;
import android.net.*;
import android.app.*;
import android.os.*;
import java.io.*;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.ThinDownloadManager;
import android.content.*;
import java.net.HttpURLConnection;
import java.net.URL;
import android.util.*;
import java.text.DecimalFormat;
import android.view.*;
import dtm.idv2.Model.*;
import dtm.idv2.Utils.*;
import android.database.*;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.*;
import com.loopj.android.http.*;
import org.apache.http.*;

public class SyncNow
{
	String SYNCSERVER="";
	Context ctx;
	int downloadid=0;
	//private DownloadManager downloadManager;

	private ProgressDialog dialog;
	String[] memu={"Ganti Data","Tambah Data"};
	ThinDownloadManager downloadManager2;

	private AlertDialog dlgg;
	public SyncNow(Context ctx){
		// gs://loginui-b4f0f.appspot.com/
		SYNCSERVER=MainActivity.storageurl.replace("gs:","").replace("/","");
		//SYNCSERVER=MainFragment.pref.getString("sserver","").replace("gs:","").replace("/","");
		this.ctx=ctx;
	}
	private void csvChooser(){
		AsyncHttpClient client = new AsyncHttpClient();
		final AlertDialog.Builder dg=new AlertDialog.Builder(ctx);
		//dg.setTitle("Pilih Data");
		dg.setMessage("Memuat Data\nMohon tunggu...");
		final AlertDialog dd=dg.show();
		client.get(MainActivity.dburl + "/DATASTORE.json", new TextHttpResponseHandler(){
				@Override
				public void onFailure(int p1, org.apache.http.Header[] p2, String p3, Throwable p4) {
					Toast.makeText(ctx, "Gagal Membuat Data", 1).show();
				}

				@Override
				public void onSuccess(int p1, org.apache.http.Header[] p2, String content) {
					final String[] arr = content.toString().replaceAll("(\\[|\\])","").replace(", ","").replace(" ,","").replace("\"","").split(",");
					dd.dismiss();
					AlertDialog.Builder dg=new AlertDialog.Builder(ctx);
					dg.setTitle("Download Data");
					dg.setItems(arr, new DialogInterface.OnClickListener(){
							@Override
							public void onClick(DialogInterface di, int pos) {
								String sel=arr[pos];
								String xcur="https://firebasestorage.googleapis.com/v0/b/"+SYNCSERVER+"/o/"+sel+"?alt=media";
								
								DownloadData2(Uri.parse(xcur), 1);
								
							}
						});
					dg.show();
				}
			});
	}
	
	public void start(){
		if(SYNCSERVER.equals("")){
			Toast.makeText(ctx, "Server belum diatur", Toast.LENGTH_LONG).show();
			return;
		}
		if(!isNetworkAvailable()){
			Toast.makeText(ctx, "Tidak ada konektivitas", Toast.LENGTH_LONG).show();
			return;
		}
		AlertDialog.Builder dg=new AlertDialog.Builder(ctx);
		dg.setTitle("Download Data");
		dg.setItems(memu, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int pos) {
					dlgg.dismiss();
					if(pos==1) csvChooser();
					else if(pos==0){
						//String dbx="https://firebasestorage.googleapis.com/v0/b/"+SYNCSERVER+"/o/data2.csv?alt=media";
						String dbx="http://127.0.0.1:8000/data.csv";
						DownloadData2(Uri.parse(dbx), 0);
					}
					//Toast.makeText(ctx, ""+pos,0).show();
				}
			});
		dlgg=dg.show();
		/*new Handler().postDelayed(new Runnable(){
				@Override
				public void run() {
					if(!dialog.isShowing()) return;
					dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setVisibility(View.VISIBLE);
				}
			}, 2000);*/
		//if(true) return;
		/*downloadManager2=new ThinDownloadManager();
		//final String dburl="https://firebasestorage.googleapis.com/v0/b/"+SYNCSERVER+"/o/data.db?alt=media";
		//final String csvurl="https://firebasestorage.googleapis.com/v0/b/"+SYNCSERVER+"/o/data.csv?alt=media";
		//final String dburl="https://www.whatsapp.com/nokia/WhatsApp_2_16_57.sis";
		dialog = new ProgressDialog(ctx);
		dialog.setTitle("Data Sync");
		dialog.setCancelable(false);
		dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		new AsyncTask<Void,Void,Void>(){
			boolean exi=false;
			Uri urltodownload;
			int mode=0;
			@Override
			protected Void doInBackground(Void[] p1) {
				exi=exists(csvurl);
				if(!exi){
					if(exists(dburl)) {urltodownload= Uri.parse(dburl);mode=0; }
				}else{
					urltodownload=Uri.parse(csvurl);
					mode=1;
				}
				return null;
			}
			@Override
			protected void onPreExecute(){
				dialog.setMessage("Memeriksa ketersediaan data...");
				dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface p1, int p2) {
							downloadManager2.cancel(downloadid);
							Toast.makeText(ctx, "Sync dibatalkan",Toast.LENGTH_LONG).show();
						}
					});
				dialog.show();
				dialog.setIndeterminate(true);
				dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setVisibility(View.GONE);
			}
			@Override
			protected void onPostExecute(Void res){
				if(urltodownload!=null){
					DownloadData2(urltodownload, mode);
				}else{
					dialog.dismiss();
					Toast.makeText(ctx, "Data tidak ditemukan", Toast.LENGTH_LONG).show();
				}
				//Toast.makeText(ctx,"Adakah? "+exi,Toast.LENGTH_LONG).show();
				super.onPostExecute(res);
			}
		}.execute();*/
	}
	private void DownloadData2(Uri downloadUri, final int mode){
		//Uri downloadUri = Uri.parse("http://tcrn.ch/Yu1Ooo1");
		//downloadUri=Uri.parse("http://localhost:8000/data.db");
		final Uri destinationUri = Uri.parse(ctx.getCacheDir().getPath()+"/data.sync");
		//final File destf=new File(destinationUri.toString());
		downloadManager2=new ThinDownloadManager();
		dialog = new ProgressDialog(ctx);
		dialog.setTitle("Download Data");
		dialog.setCancelable(false);
		dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					downloadManager2.cancel(downloadid);
					Toast.makeText(ctx, "Sync dibatalkan",Toast.LENGTH_LONG).show();
				}
			});
		//dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setVisibility(View.GONE);
		dialog.show();
		dialog.setIndeterminate(true);
		
		dialog.setMessage("Mohon tunggu...");
		//dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		dialog.setIndeterminate(false);
		//dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setVisibility(View.VISIBLE);
		DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
            .setRetryPolicy(new DefaultRetryPolicy())
            .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
            .setDownloadListener(new DownloadStatusListener() {
				@Override
				public void onDownloadComplete(int p1) {
					dialog.dismiss();
					RestoreNow dbs=new RestoreNow(ctx,1);
					if(mode==0){
						dbs.importCsvWithRestartWipe(destinationUri.toString());
						//dbs.restoreDb(new Backupz(new File(destinationUri.toString())));
						//dbs.mergeDatabase(destinationUri.toString());
					}//dbs.restoreDb(new Backupz(new File(destinationUri.toString())));
					else dbs.importCsvWithRestart(destinationUri.toString());
					//Toast.makeText(ctx, "Restart App...",Toast.LENGTH_LONG).show();
					//dbs.restartApp();
					//
					
				}

				@Override
				public void onDownloadFailed(int p1, int p2, String p3) {
					dialog.dismiss();
					Toast.makeText(ctx, "Sync Gagal, "+p3,Toast.LENGTH_LONG).show();
				}

				@Override
				public void onProgress(int id, long total, long loaded, int prog) {
					// TODO: Implement this method
					dialog.setMessage("Mengambil data ("+readableFileSize(total)+") ");
					dialog.setProgress(prog);
				}
			});
		//dialog.setProgress(0);
		
		downloadid=downloadManager2.add(downloadRequest);
		//dialog.show();
	}
	
	public static String readableFileSize(long size) {
		if(size <= 0) return "0";
		final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
		int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}
	public static boolean exists(String URLName){
		Log.d("cekurl", URLName+"---");
		try {
			HttpURLConnection.setFollowRedirects(false);
			// note : you may also need
			//        HttpURLConnection.setInstanceFollowRedirects(false)
			HttpURLConnection con =
				(HttpURLConnection) new URL(URLName).openConnection();
			//con.setRequestMethod("HEAD");
			Log.d("cekurl", URLName+"---"+con.getResponseCode());
			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		}
		catch (Exception e) {
			Log.d("cekurl", URLName+"---"+e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager 
			= (ConnectivityManager)ctx. getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
}
