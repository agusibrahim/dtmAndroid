package dtm.idv2.Adapter;
import dtm.idv2.Model.*;
import java.util.*;
import android.content.*;
import android.widget.*;
import android.view.*;
import dtm.idv2.R;
import java.text.DecimalFormat;
import android.text.*;
import android.graphics.*;

public class FileAdapter extends ArrayAdapter<MyFile>
{
	Context mContext;
	public FileAdapter(Context ctx, ArrayList<MyFile> user){
		super(ctx,0,user);
		mContext=ctx;
	}
	public static class ViewHolder{
		TextView nama;
		TextView ukuran;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final MyFile file=getItem(position);
		ViewHolder holder;
		if(convertView==null){
			holder=new ViewHolder();
			convertView=LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_2, parent, false);
			holder.nama=(TextView) convertView.findViewById(android.R.id.text1);
			holder.nama.setTypeface(null,Typeface.BOLD);
			holder.ukuran=(TextView) convertView.findViewById(android.R.id.text2);
			convertView.setTag(holder);
		}else{
			holder=(FileAdapter.ViewHolder) convertView.getTag();
		}
		holder.nama.setText(file.name);
		holder.ukuran.setText(readableFileSize(file.size)+"   MD5: "+file.hash);
		return convertView;
	}
	public static String readableFileSize(long size) {
		if(size <= 0) return "0";
		final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
		int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}
}



