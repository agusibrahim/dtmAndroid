package dtm.idv2.Adapter;

import android.widget.*;
import android.content.*;
import java.util.*;
import android.view.*;
import dtm.idv2.Model.*;
import android.text.*;
import dtm.idv2.Fragment.*;
import dtm.idv2.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import android.text.style.*;
import android.graphics.*;

public class NosinAdapter extends ArrayAdapter<Nosin>
{
	boolean cs;
	boolean nf;
	Pattern num=Pattern.compile("\\d+");
	int lsize;
	public NosinAdapter(Context ctx, List<Nosin> data){
		super(ctx,0,data);
		lsize=MainActivity.pref.getInt("itemsize", 13);
		nf=MainActivity.pref.getBoolean("numfocus", true);
	}
	public static class ViewHolder{
		TextView noka;
		TextView nosin;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Nosin w;
		try{w=getItem(position);
		}catch(Exception e){return convertView;}
		ViewHolder holder;
		if(convertView==null){
			holder=new ViewHolder();
			convertView=LayoutInflater.from(getContext()).inflate(R.layout.item_nosin, parent, false);
			holder.noka=(TextView) convertView.findViewById(R.id.itemnosinNoka);
			holder.nosin=(TextView) convertView.findViewById(R.id.itemnosinNosin);
			convertView.setTag(holder);
		}else{
			holder=(NosinAdapter.ViewHolder) convertView.getTag();
		}
		boolean yesnoka=w.noka.length()>0;
		boolean yesnosin=w.nosin.length()>0;
		//String kay=w.nopol+" ~ "+Caps(w.mobil)+"+"+Caps(w.lesing)+(!(""+w.overdue).equals("null")&&w.overdue.length()>0?"["+w.overdue:"")+")"+((w.saldo!=null&&(""+w.saldo).length()>6&&!(""+w.saldo).equals("null"))?" +Rp"+curtrim( w.saldo)+")":"");
		//kay=kay.trim().replaceAll("(~ \\+$|\\+$|\\+\\)$)","").trim();
		holder.noka.setSingleLine(true);
		holder.noka.setTextSize(lsize);
		holder.noka.setText(yesnoka?w.noka.toUpperCase():"...");//Html.fromHtml("<b>"+kay+"</b>"));
		holder.nosin.setSingleLine(true);
		holder.nosin.setTextSize(lsize);
		holder.nosin.setText(yesnosin?w.nosin.toUpperCase():"...");
		//if(cs) holder.values.setText(w.);
		return convertView;
	}
	private void numfocus2(TextView tv){
		String ori=tv.getText().toString();
		Matcher match= num.matcher(ori);
		//StringBuffer sb = new StringBuffer(ori.length());
		Spannable spanRange = new SpannableString(ori);

		while(match.find()){
			ForegroundColorSpan foreColour = new ForegroundColorSpan(Color.RED);
			spanRange.setSpan(foreColour, match.start(), match.end(),
							  Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			//tch.appendReplacement(sb, "<font color=\"#ff0000\">" + match.group(0) + "</font>");
			//System.out.println(match.group(0));

		}
		tv.setText(spanRange);
		//match.appendTail(sb);
		//return sb.toString();
	}

	private String Caps(String name){
		if(name==null||name.length()<1) return name;
		name = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
		return name;
	}
	private String curtrim(String n){
		if(n.length()>6) return n.substring(0, n.length()-6);
		else return "";
	}
}
