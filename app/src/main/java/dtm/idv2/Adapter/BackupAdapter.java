package dtm.idv2.Adapter;
import android.widget.*;
import android.content.*;
import dtm.idv2.Model.*;
import java.util.*;
import android.view.*;

public class BackupAdapter extends ArrayAdapter<Backupz>
{
	public BackupAdapter(Context ctx, List<Backupz> data){
		super(ctx,0,data);
	}
	public static class ViewHolder{
		TextView keys;
		TextView values;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Backupz w=getItem(position);
		ViewHolder holder;
		if(convertView==null){
			holder=new ViewHolder();
			convertView=LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_2, parent, false);
			holder.keys=(TextView) convertView.findViewById(android.R.id.text1);
			holder.values=(TextView) convertView.findViewById(android.R.id.text2);
			convertView.setTag(holder);
		}else{
			holder=(BackupAdapter.ViewHolder) convertView.getTag();
		}
		holder.keys.setText("DB "+w.getDbCount());
		holder.values.setText(w.getTime());
		return convertView;
	}
}
