package dtm.idv2.Adapter;
import android.view.*;
import android.widget.*;
import android.content.*;
import java.util.*;
import android.text.*;
import java.util.regex.*;
import android.graphics.*;

public class FiltrAdaptr extends ArrayAdapter<String>
{
	public FiltrAdaptr(Context ctx, List<String> data){
		super(ctx,0,data);
	}
	public static class ViewHolder{
		//TextView keys;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		String w=getItem(position);
		ViewHolder holder;
		if(convertView==null){
			holder=new ViewHolder();
			convertView=LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_multiple_choice, parent, false);
			//holder.keys=(TextView) convertView.findViewById(android.R.id.text1);
			convertView.setTag(holder);
		}else{
			holder=(FiltrAdaptr.ViewHolder) convertView.getTag();
		}
		CheckedTextView txt=(CheckedTextView) convertView;
		String nama=w.split(" \\[")[0];
		Pattern num=Pattern.compile("\\d+");
		Matcher match= num.matcher(nama);
		if(match.find()){
			String bulan=match.group();
			String les=nama.replaceAll("\\d","");
			//if(les.length()>1) les=les.substring(0, les.length()-1);
			String wrn=les.substring(les.length()-1, les.length());
			nama="<font color='"+getWarna(wrn)+"'>"+(les.length()>1?les.substring(0, les.length()-1):les)+"</font>"+bulan;
		}
		String jm="["+w.split(" \\[")[1];
		txt.setTypeface(Typeface.SERIF, Typeface.BOLD);
		txt.setText(Html.fromHtml("<b>"+nama+" <font color='red'>"+jm+"</font></b>"));
		return convertView;
	}
	private String getWarna(String k){
		String warna="000000";
		if(k.length()<1) return "#000000";
		switch(k.toLowerCase().substring(k.length()-1,k.length())){
			case "w":
				warna="FA0E00"; // merah
				break;
			case "z":
				warna="E500C1"; // ungu
				break;
			case "x":
				warna="21C337"; // hijau
				break;
			case "n":
				warna="1658FE"; // biru
				break;

		}
		return "#"+warna;
	}
}
