package dtm.idv2.Adapter;
import android.widget.*;
import android.content.*;
import java.util.*;
import android.view.*;
import dtm.idv2.Model.*;
import android.text.*;
import dtm.idv2.Fragment.*;
import dtm.idv2.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import android.text.style.*;
import android.graphics.*;
import dtm.idv2.Utils.SimpleSpanBuilder;

public class DataAdapter extends ArrayAdapter<Unit>
{
	Map<Boolean, Integer> listlayout=new HashMap<Boolean, Integer>();
	boolean cs;
	boolean nf;
	Pattern num=Pattern.compile("\\d+");
	int lsize;
	public DataAdapter(Context ctx, List<Unit> data){
		super(ctx,0,data);
		listlayout.put(true, android.R.layout.simple_list_item_2);
		listlayout.put(false, R.layout.item1);
		cs=MainActivity.pref.getBoolean("showdetail",false);
		lsize=MainActivity.pref.getInt("itemsize", 13);
		nf=MainActivity.pref.getBoolean("numfocus", true);
	}
	public static class ViewHolder{
		TextView keys;
		TextView values;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Unit w;
		try{w=getItem(position);
		}catch(Exception e){return convertView;}
		ViewHolder holder;
		if(convertView==null){
			holder=new ViewHolder();
			convertView=LayoutInflater.from(getContext()).inflate(listlayout.get(cs), parent, false);
			if(cs) holder.keys=(TextView) convertView.findViewById(android.R.id.text1);
			else holder.keys=(TextView) convertView.findViewById(R.id.item1txt);
			if(cs) holder.values=(TextView) convertView.findViewById(android.R.id.text2);
			convertView.setTag(holder);
		}else{
			holder=(DataAdapter.ViewHolder) convertView.getTag();
		}
		/*String kay=w.nopol+" ~ "+Caps(w.mobil)+"+"+Caps(w.lesing)+(!(""+w.overdue).equals("null")&&w.overdue.length()>0?"["+w.overdue:"")+")"+((w.saldo!=null&&(""+w.saldo).length()>6&&!(""+w.saldo).equals("null"))?" +Rp"+curtrim( w.saldo)+")":"");
		kay=kay.trim().replaceAll("(~ \\+$|\\+$|\\+\\)$)","").trim();
		holder.keys.setSingleLine(true);
		holder.keys.setTextSize(lsize);
		holder.keys.setText(kay);//Html.fromHtml("<b>"+kay+"</b>"));
		if(nf){
			//.kay=numfocus(kay);
			numfocus2(holder.keys);
		}*/
		//String kay=w.nopol+" ~ "+Caps(w.mobil)+"+"+Caps(w.lesing)+(!(""+w.overdue).equals("null")&&w.overdue.length()>0?"["+w.overdue:"")+")"+((w.saldo!=null&&(""+w.saldo).length()>6&&!(""+w.saldo).equals("null"))?" +Rp"+curtrim( w.saldo)+")":"");
		//kay=kay.trim().replaceAll("(~ \\+$|\\+$|\\+\\)$)","").trim();
		
		
		String[] leso=w.lesing.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
		SimpleSpanBuilder ssb = new SimpleSpanBuilder();
		for(String s:w.nopol.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)")){
			if(s.matches("\\d+")){
				ssb.append(s.toUpperCase(), new ForegroundColorSpan(Color.RED));
			}else ssb.append(s.toUpperCase());
		}
		boolean lesnum=w.lesing.matches(".*\\d.*");
		if(isValid(w.mobil)) ssb.append(" ~ "+Caps(w.mobil));
		if(leso.length>0&&isValid(leso[0])&&lesnum){
			ssb.append("+");
			ssb.append(Caps(leso[0].substring(0, leso[0].length()-1)), new ForegroundColorSpan(Color.parseColor(getWarna(leso[0]))));
		}
		/*if(!lesnum){
			ssb.append("+");
			String[] lp=w.lesing.split("-");
			ssb.append(Caps(lp.length>0?lp[0]:w.lesing), new ForegroundColorSpan(Color.parseColor(getWarna(leso[0]))));
		}*/
		if(leso.length>1&&isValid(leso[1])&&lesnum) ssb.append(joiner(leso));
		if(isValid(w.overdue)) ssb.append("]"+w.overdue+")");
		if(isValid(w.saldo)&&!w.saldo.trim().equals("0")) ssb.append(" +Rp"+curtrim(w.saldo));
		//if((""+w.mobil).length()>2) ssb.append(Caps(w.mobil)+"+");
		//if(leso.length>0) ssb.append(Caps(leso[0]), new ForegroundColorSpan(Color.parseColor(getWarna(leso[0]))));
		//if(leso.length>1) ssb.append(leso.length>1?leso[1]:"");
		//ssb.append((!(""+w.overdue).equals("null")&&w.overdue.length()>0?"]"+w.overdue:"")+((w.saldo!=null&&(""+w.saldo).length()>6&&!(""+w.saldo).equals("null"))?" +Rp"+curtrim( w.saldo)+")":""));
		//ssb.append("stackOverflow",new ForegroundColorSpan(Color.RED),new RelativeSizeSpan(1.5f));
		holder.keys.setText(ssb.build());
		
		//if(cs) holder.values.setText(w.);
		return convertView;
	}
	private boolean isValid(String s){
		return s!=null&&!s.equals("null")&&s.length()>0;
	}
	/*private void numfocus2(TextView tv){
		String ori=tv.getText().toString();
		Matcher match= num.matcher(ori);
		//StringBuffer sb = new StringBuffer(ori.length());
		Spannable spanRange = new SpannableString(ori);
		
		while(match.find()){
			ForegroundColorSpan foreColour = new ForegroundColorSpan(Color.RED);
			spanRange.setSpan(foreColour, match.start(), match.end(),
							  Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		//tch.appendReplacement(sb, "<font color=\"#ff0000\">" + match.group(0) + "</font>");
			//System.out.println(match.group(0));
			
		}
		tv.setText(spanRange);
		//match.appendTail(sb);
		//return sb.toString();
	}*/
	private static String joiner(String[] ss){
		String r="";
		if(ss.length<2) return ss[1];
		for(int i=1;i<ss.length;i++){
			r=r+ss[i];
		}
		return r.replaceAll("-$","");
	}
	private String getWarna(String k){
		String warna="000000";
		if(k.length()<1) return "#000000";
		switch(k.toLowerCase().substring(k.length()-1,k.length())){
			case "w":
				warna="FA0E00"; // merah
				break;
			case "z":
				warna="E500C1"; // ungu
				break;
			case "x":
				warna="21C337"; // hijau
				break;
			case "n":
				warna="1658FE"; // biru
				break;

		}
		return "#"+warna;
	}
	private String Caps(String name){
		if(name==null||name.length()<1) return name;
		name = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
		return name;
	}
	private String curtrim(String n){
		if(n.length()>6) return n.substring(0, n.length()-6);
		else return "";
	}
}
