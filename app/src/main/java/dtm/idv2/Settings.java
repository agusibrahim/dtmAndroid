package dtm.idv2;

import android.support.v7.app.*;
import android.preference.*;
import android.os.*;
import android.view.*;

public class Settings extends PreferenceActivity
{
	private AppCompatDelegate mDelegate;
	@Override
	public void onCreate(Bundle p1) {
		super.onCreate(p1);
        addPreferencesFromResource(R.xml.settings);
		//View v=LayoutInflater.from(this).inflate(R.layout.activity_main,null);
		//Toolbar tb=(Toolbar) v.findViewById(R.id.toolbar);
		//getDelegate().setSupportActionBar(tb);
		android.support.v7.app.ActionBar actionBar = getDelegate().getSupportActionBar();
		if(actionBar!=null){
			actionBar.setElevation(2);
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeButtonEnabled(true);
		}

		//getActionBar().setDisplayHomeAsUpEnabled(true);
		//addPreferencesFromResource(R.xml.pengaturan);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) { 
		switch (item.getItemId()) {
			case android.R.id.home: 
				onBackPressed();
				return true;
		}

		return super.onOptionsItemSelected(item);
	}
	private AppCompatDelegate getDelegate() {
		if (mDelegate == null) {
			mDelegate = AppCompatDelegate.create(this, null);
		}
		return mDelegate;
	}

	@Override
	public void onBackPressed() {
		
		super.onBackPressed();
	}
	
}

