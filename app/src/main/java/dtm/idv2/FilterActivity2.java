package dtm.idv2;
import android.support.v7.app.*;
import android.os.*;
import java.util.*;
import android.widget.*;
import dtm.idv2.Utils.*;
import android.content.*;
import android.support.v4.widget.*;
import android.database.*;
import android.view.*;
import dtm.idv2.Fragment.*;
import android.util.*;
import dtm.idv2.Adapter.*;

public class FilterActivity2 extends AppCompatActivity implements View.OnClickListener
{
	List<String> selected=new ArrayList<String>();
	List<String> terpilih=new ArrayList<String>();
	private Button hapusbtn, detailbtn;
	private ListView lesview;
	Context mContext;
	QueryEngine engine;
	SwipeRefreshLayout loadv;
	private ArrayAdapter<String> adap;
	int urutkan=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter2);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		mContext=this;
		hapusbtn=(Button) findViewById(R.id.act_delall);
		detailbtn=(Button) findViewById(R.id.act_view);
		lesview=(ListView) findViewById(R.id.lesingview);
		loadv=(SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
		engine=new QueryEngine(this);
		adap=new FiltrAdaptr(this, selected);//new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, selected);
		lesview.setAdapter(adap);
		hapusbtn.setOnClickListener(this);
		detailbtn.setOnClickListener(this);
		//loadv.setRefreshing(true);
		loadv.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
				@Override
				public void onRefresh() {
					selected.clear();
					new loadcategoty().execute(urutkan);
				}
			});
		loadv.post(new Runnable() {
				@Override
				public void run() {
					loadv.setRefreshing(true);
				}
			});
		new loadcategoty().execute(urutkan);
		//loadv.setEnabled(false);
	}

	@Override
	public void onClick(View p1) {
		int id=p1.getId();
		terpilih.clear();
		SparseBooleanArray checked = lesview.getCheckedItemPositions();
		for (int i = 0; i < selected.size(); i++) {
			if (checked.get(i)) {
				terpilih.add(selected.get(i).split(" \\[")[0]);
			}
		}
		
		if(id==R.id.act_view){
			if(terpilih.size()>1) return;
			String les=MainActivity.alllesing;
			if(terpilih.size()==1) les=terpilih.get(0);
			if(les.toLowerCase().equals("tanpalesing")){
				les="";
			}
			//MainFragment.filtr.setTitle(les);
			android.util.Log.d("mamaa", les);
			if(!les.equals(MainActivity.alllesing))
				MainActivity.curlesing.setText(les);//"(select * from data where suggest_text_2='"+les+"')";
			else{
				MainActivity.curlesing.setText(MainActivity.alllesing);
			}
			setResult(345);
			finish();
			
		}else if(id==R.id.act_delall){
			if(terpilih.size()<1){
				Toast.makeText(this, "Pilih lesing untuk dihapus ",1).show();
				return;
			}
			AlertDialog.Builder dl=new AlertDialog.Builder(mContext);
			dl.setTitle("Hapus Data");
			dl.setMessage("Apakah kamu yakin ingin menghapus '"+terpilih.toString().replaceAll("[\\[\\]]","")+"'?");
			dl.setPositiveButton("YA", new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface p1, int p2) {
						new Hapus().execute();
					}
				});
			dl.setNegativeButton("Batal", null);
			dl.show();
		}
	}
	public class Hapus extends AsyncTask<Void, Void, Void> {
		AlertDialog dlg;
		@Override
		protected Void doInBackground(Void[] p1) {
			for(int i=0;i<terpilih.size();i++){
				String les=terpilih.get(i);
				QueryEngine.db.delete(QueryEngine.tableName, QueryEngine.Column2+" LIKE "+DatabaseUtils.sqlEscapeString(les+"%"),null);
			}
			return null;
		}
		@Override
		protected void onPreExecute() {
			AlertDialog.Builder d=new AlertDialog.Builder(mContext);
			d.setMessage("Menghapus. Mohon tunggu...");
			dlg=d.create();
			dlg.setCancelable(false);
			dlg.show();
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Void result) {
			dlg.setMessage("Berhasil Dihapus");
			dlg.setCancelable(true);
			new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						dlg.dismiss();
						finish();
					}
				}, 1000);
			super.onPostExecute(result);
		}

	}
	
	public class loadcategoty extends AsyncTask<Integer, List<String>, List<String>> {
		@Override
		protected List<String> doInBackground(Integer[] p1) {
			List<String> data= engine.getGroup2();
			//data.add(0, "Semua Lesing");
			//java.util.Arrays.sort(data, 1, data.size());
			if(p1[0]==0) Collections.sort(data);
			else if(p1[0]==1) Collections.sort(data, new urutkanLesingBelakang());
			else if(p1[0]==2) Collections.sort(data, new urutkanLesingBulan());
			else if(p1[0]==3) Collections.sort(data, new urutkanJumlah());
			return data;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(List<String> result) {
			//Collections.sort(result.subList(1, result.size()));
			loadv.setRefreshing(false);
			selected.addAll(result);
			adap.notifyDataSetChanged();
			//lesingspinner.clear();
			//lesingspinner.addAll(result);
			//lesing.setItems(result, "Semua Lesing", FilterActivity.this);
			super.onPostExecute(result);
		}

	}
	private static class urutkanJumlah implements Comparator<String> {
        @Override
        public int compare(String prod1, String prod2) {
			int p1=Integer.parseInt( prod1.split(" \\[")[1].split("\\]")[0]);
			int p2=Integer.parseInt( prod2.split(" \\[")[1].split("\\]")[0]);
			if(p1<p2) return 1;
			if(p1>p2) return -1;
            return 0;
		}
    }
	private static class urutkanLesingBelakang implements Comparator<String> {
        @Override
        public int compare(String prod1, String prod2) {
			String p1=prod1.split(" \\[")[0].replaceAll("\\d","");
			String p2=prod2.split(" \\[")[0].replaceAll("\\d","");
			p1=p1.substring(p1.length()-1, p1.length()).toLowerCase();
			p2=p2.substring(p2.length()-1, p2.length()).toLowerCase();
			return p1.compareTo(p2);
		}
    }
	private static class urutkanLesingBulan implements Comparator<String> {
        @Override
        public int compare(String prod1, String prod2) {
			int p1=Integer.parseInt("0"+(prod1.split(" \\[")[0].replaceAll("[^\\d]", "")));
			int p2=Integer.parseInt("0"+(prod2.split(" \\[")[0].replaceAll("[^\\d]", "")));
			if(p1<p2) return -1;
			if(p1>p2) return 1;
			return 0;
		}
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
			case R.id.filtrmenu:
				AlertDialog.Builder gg =new AlertDialog.Builder(this);
				gg.setTitle("Urutkan Data");
				gg.setItems(new String[]{"Nama (A-Z)", "Warna", "Bulan", "Jumlah Data"}, new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface p1, int pos) {
							/*loadv.setRefreshing(true);
							selected.clear();
							new loadcategoty().execute(pos);*/
							new Urutkan().execute(pos);
						}
					});
				gg.show();
				break;
			case android.R.id.home:
				onBackPressed();
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		finish();
		super.onBackPressed();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.filtr_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	private class Urutkan extends AsyncTask<Integer, Void, Void> {
		private Integer pos;
		@Override
		protected Void doInBackground(Integer[] p1) {
			pos=p1[0];
			if(pos==0) Collections.sort(selected);
			else if(pos==1) Collections.sort(selected, new urutkanLesingBelakang());
			else if(pos==2) Collections.sort(selected, new urutkanLesingBulan());
			else if(pos==3) Collections.sort(selected, new urutkanJumlah());
			return null;
		}

		@Override
		protected void onPreExecute() {
			loadv.setRefreshing(true);
			super.onPreExecute();
		}
		
		@Override
		protected void onPostExecute(Void result) {
			urutkan=pos;
			loadv.setRefreshing(false);
			adap.notifyDataSetChanged();
			super.onPostExecute(result);
		}
	}
}
