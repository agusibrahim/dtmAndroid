package dtm.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.*;
import android.graphics.drawable.*;
import android.support.v4.graphics.drawable.*;
import dtm.idv2.R;
import android.support.graphics.drawable.VectorDrawableCompat;
/**
 * Created by katsuyagoto on 2014/06/19.
 */
public class GridMenuAdapter extends ArrayAdapter<GridMenu> {

    private LayoutInflater mInflater;
	private int textcolor, iconcolor;
	private Context mContext;
    public GridMenuAdapter(Context context, int txtcolor, int iconcolor) {
        super(context, 0);
		this.textcolor=txtcolor;
		this.iconcolor=iconcolor;
		this.mContext=context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.menu_grid_single, null, false);
            holder = new Holder();
            holder.menuTitle = (TextView) convertView.findViewById(R.id.grid_text);
            holder.menuIcon = (android.support.v7.widget.AppCompatImageView) convertView.findViewById(R.id.grid_image);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        GridMenu gridMenu = getItem(position);
        holder.menuTitle.setText(gridMenu.getTitle());
		holder.menuTitle.setTextColor(textcolor);
		VectorDrawableCompat vdc=VectorDrawableCompat.create(mContext.getResources(), gridMenu.getIcon(), null);
        holder.menuIcon.setImageDrawable(setTint(vdc, iconcolor));
        return convertView;
    }

    public static class Holder {
        TextView menuTitle;
        android.support.v7.widget.AppCompatImageView menuIcon;
    }
	public static Drawable setTint(Drawable d, int color) {
		Drawable wrappedDrawable = DrawableCompat.wrap(d);
		DrawableCompat.setTint(wrappedDrawable, color);
		return wrappedDrawable;
	}
}
