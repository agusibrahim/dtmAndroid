package dtm.menu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import dtm.idv2.R;
import java.util.ArrayList;
import java.util.List;
import android.view.ViewGroup.*;
import android.widget.*;
import android.content.*;
import android.graphics.*;
import java.io.*;
import android.net.*;
//import com.afollestad.materialdialogs.color.*;
import android.app.*;
import android.os.*;
import cc.yann.android.jniblur.*;
import android.util.*;
import android.graphics.drawable.*;
//import com.afollestad.materialdialogs.*;
//import com.afollestad.materialdialogs.internal.*;
import android.support.v4.graphics.drawable.*;
import dtm.idv2.MainActivity;
import android.support.v7.app.AlertDialog;
import org.dmfs.android.colorpicker.*;
import org.dmfs.android.colorpicker.palettes.*;
import android.support.v7.app.AppCompatActivity;
import org.dmfs.android.colorpicker.ColorPickerDialogFragment.ColorDialogResultListener;
import java.util.*;
/**
 * Created by katsuyagoto on 15/06/04.
 */
public class GridMenuFragment extends Fragment implements AdapterView.OnItemClickListener/*, ColorChooserDialog.ColorCallback */{
	private EditText lf_nama;
	private final static int[] COLORS = new int[] {
		0xff000000, 0xff0000ff, 0xff00ff00, 0xffff0000, 0xffffff00, 0xff00ffff, 0xffff00ff, 0xff404040,
		0xff808080, 0xff8080ff, 0xff80ff80, 0xffff8080, 0xffffff80, 0xff80ffff, 0xffff80ff, 0xffffffff };

    private final static int[] MATERIAL_COLORS_PRIMARY = {
		0xffe91e63, 0xfff44336, 0xffff5722, 0xffff9800, 0xffffc107, 0xffffeb3b, 0xffcddc39, 0xff8bc34a,
		0xff4caf50, 0xff009688, 0xff00bcd4, 0xff03a9f4, 0xff2196f3, 0xff3f51b5, 0xff673ab7, 0xff9c27b0 };

    private static final int MATERIAL_COLORS_SECONDARY[] = {
		0xffad1457, 0xffc62828, 0xffd84315, 0xffef6c00, 0xffff8f00, 0xfff9a825, 0xff9e9d24, 0xff558b2f,
		0xff2e7d32, 0xff00695c, 0xff00838f, 0xff0277bd, 0xff1565c0, 0xff283593, 0xff4527a0, 0xff6a1b9a };
	/*@Override
	public void onColorSelection(ColorChooserDialog p1, int p2) {
		if(p1.tag().equals("1")){
			box1.setBackgroundColor(p2);
			lf_icon.setImageDrawable(setTint(lf_icon.getDrawable(), p2) );
			curIcon=p2;
		}else{
			box2.setBackgroundColor(p2);
			lf_label.setTextColor(p2);
			curLabel=p2;
		}
	}

	@Override
	public void onColorChooserDismissed(ColorChooserDialog p1) {
		// TODO: Implement this method
	}*/
	

    private static final String KEY_BG_RESOURCE_ID = "key_bg_resource_id";
	ImageView skinBtn;
	private GridView gridView;
	private ImageView appbg;
	private static final int resultCodeImg= 23;
	private View box1,box2;
	private Uri selectedBg;
	private ImageView lf_bg,lf_icon;
	Bitmap bgori;
	private TextView lf_label;
	private int curIcon=Color.WHITE, curLabel=Color.WHITE;
	private File nonModifiedBackground, modifiedBackground;
	private SeekBar lf_seek;
	private SharedPreferences pref;
	private LinearLayout topbg;
	private Bitmap ifbg_bitmap;
	private ImageView imgvtmp;
	private TextView curuser;

	private String[] motif;

	private TextView tvscroll;
    /*public static GridMenuFragment newInstance(int backgroundResourceID) {
        GridMenuFragment gridMenuFragment = new GridMenuFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_BG_RESOURCE_ID, backgroundResourceID);
        gridMenuFragment.setArguments(args);
        return gridMenuFragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.menu_main, null);
        
		nonModifiedBackground=new File(getActivity(). getCacheDir(), ".nonModifiedBackground");
		modifiedBackground=new File(getActivity(). getCacheDir(), ".modifiedBackground");
		pref=getActivity(). getSharedPreferences("skins", getActivity(). MODE_PRIVATE);
		curIcon=pref.getInt("icon_color", curIcon);
		curLabel=pref.getInt("label_color", curLabel);
		curuser=(TextView)view.findViewById(R.id.mainTextViewNama);
		skinBtn=(ImageView) view.findViewById(R.id.skinBtn);
		tvscroll=(TextView)view.findViewById(R.id.marquee_text_1);
		tvscroll.setSelected(true);
		tvscroll.setTypeface(Typeface.MONOSPACE);
		imgvtmp = (ImageView) view.findViewById(R.id.mainImageViewTmp);
		topbg = (LinearLayout) view.findViewById(R.id.mainLinearLayout1);
		appbg=(ImageView)view.findViewById(R.id.mainImageView1);
		motif=getActivity().getResources().getStringArray(R.array.katamotivasi);
        return view;
    }
	

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (mOnClickMenuListener == null) {
            throw new IllegalArgumentException("Must implement setOnClickMenuListener");
        } else {
            mOnClickMenuListener.onClickMenu(mMenus.get(i), i);
        }
    }

	@Override
	public void onViewCreated(final View view, Bundle savedInstanceState) {
		mGridMenuAdapter = new GridMenuAdapter(getActivity(), curLabel, curIcon);
		gridView = (GridView) view.findViewById(R.id.grid);
        mGridMenuAdapter.addAll(this.mMenus);
        gridView.setAdapter(mGridMenuAdapter);
        gridView.setOnItemClickListener(this);
		skinBtn.setOnLongClickListener(new View.OnLongClickListener(){
				@Override
				public boolean onLongClick(View p1) {
					skinCfg();
					return false;
				}
			});
		/*skinBtn.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					/*new ColorChooserDialog.Builder(MainActivity.this, R.string.app_name)
					 .titleSub(R.string.app_name)
					 .show();*
					skinCfg();
				}
			});*/
		gridView.post(new Runnable() {
				@Override
				public void run() {
					gridView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, gridView.getHeight()+30));
				}
			});
		if(modifiedBackground.exists()){
			Bitmap bm=BitmapFactory.decodeFile(modifiedBackground.getPath());
			appbg.setImageBitmap(bm);
			//appbg.setScaleType(ImageView.ScaleType.CENTER);
		}
		curuser.setText(pref.getString("user", "User"));
		//gridView.setLayoutParams(lp);
		super.onViewCreated(view, savedInstanceState);
	}

	
    public void setupMenu(List<GridMenu> menus) {
        this.mMenus = menus;
    }

    private GridMenuAdapter mGridMenuAdapter;

    private List<GridMenu> mMenus = new ArrayList<>();

    private OnClickMenuListener mOnClickMenuListener;

    public void setOnClickMenuListener(OnClickMenuListener listener) {
        mOnClickMenuListener = listener;
    }

    public interface OnClickMenuListener {

        void onClickMenu(GridMenu gridMenu, int position);
    }
	private void skinCfg(){
		View v=LayoutInflater.from(getActivity()).inflate(R.layout.menu_lookfeel_layout, null);
		AlertDialog.Builder dl=new AlertDialog.Builder(getActivity());
		dl.setTitle("Look and Feel");
		dl.setView(v);
		dl.setPositiveButton("Apply", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface p1, int p2) {
					GridMenuAdapter adapte = new GridMenuAdapter(getActivity(), curLabel, curIcon);
					adapte.addAll(GridMenuFragment.this.mMenus);
					gridView.setAdapter(adapte);
					if(lf_seek.getProgress()!=pref.getInt("blur",0)) new appyBackground().execute();
					curuser.setText(lf_nama.getText().toString());
					// setting save
					SharedPreferences.Editor tor=pref.edit();
					tor.putInt("icon_color", curIcon);
					tor.putInt("blur", lf_seek.getProgress());
					tor.putInt("label_color", curLabel);
					tor.putString("user", lf_nama.getText().toString());
					tor.putString("imguri", selectedBg==null?"": selectedBg.toString());
					tor.commit();
				}
			});
		dl.setNeutralButton("Cancel", null);
		AlertDialog dlg=dl.create();
		dlg.show();
		/*MaterialDialog dlg=new MaterialDialog.Builder(getActivity())
		 .title("Look and Feel")
			.customView(R.layout.menu_lookfeel_layout, true)
			.positiveText("OK")
			.onPositive(new MaterialDialog.SingleButtonCallback(){
				@Override
				public void onClick(MaterialDialog p1, DialogAction p2) {
					GridMenuAdapter adapte = new GridMenuAdapter(getActivity(), curLabel, curIcon);
					adapte.addAll(GridMenuFragment.this.mMenus);
					gridView.setAdapter(adapte);
					if(lf_seek.getProgress()!=pref.getInt("blur",0)) new appyBackground().execute();
					curuser.setText(lf_nama.getText().toString());
					// setting save
					SharedPreferences.Editor tor=pref.edit();
					tor.putInt("icon_color", curIcon);
					tor.putInt("blur", lf_seek.getProgress());
					tor.putInt("label_color", curLabel);
					tor.putString("user", lf_nama.getText().toString());
					tor.putString("imguri", selectedBg==null?"": selectedBg.toString());
					tor.commit();
				}
			})
			.neutralText("Cancel")
			.show();*/
		
		//View v = dlg.getView();
		lf_bg=(ImageView) v.findViewById(R.id.lf_bg);
		lf_icon=(ImageView) v.findViewById(R.id.lf_icon);
		lf_label=(TextView) v.findViewById(R.id.lf_label);
		lf_nama=(EditText)v.findViewById(R.id.input_nama);
		Button lf_choose=(Button) v.findViewById(R.id.lf_selectimageBtn);
		lf_seek=(SeekBar) v.findViewById(R.id.lf_seekbar);
		View lf_icc=v.findViewById(R.id.lf_iconcolor);
		View lf_lbc=v.findViewById(R.id.lf_labelcolor);
		box1=v.findViewById(R.id.lf_box1);
		box2=v.findViewById(R.id.lf_box2);
		box1.setBackgroundColor(curIcon);
		box2.setBackgroundColor(curLabel);
		lf_icon.setImageDrawable(setTint(lf_icon.getDrawable(), curIcon));
		lf_label.setTextColor(curLabel);
		lf_seek.setProgress(pref.getInt("blur",0));
		//lf_bg.setImageBitmap(BitmapFactory.decodeFile(nonModifiedBackground.getPath()));
		lf_nama.setText(curuser.getText());
		lf_nama.setSelection(curuser.getText().length());

		lf_choose.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					Intent intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(Intent.createChooser(intent, "Select picture"), resultCodeImg );
				}
			});
		lf_icc.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					appbg.setTag(1);
					colorSelect();
					/*new ColorChooserDialog.Builder(MainActivity.ini, R.string.app_name)
					.tag("1")
					.preselect(curIcon)
					.show();*/
					
				}
			});
		lf_lbc.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p1) {
					appbg.setTag(2);
					colorSelect();
					/*new ColorChooserDialog.Builder(MainActivity.ini, R.string.app_name)
						.tag("2")
						.preselect(curLabel)
						.show();*/
					
				}
			});
		lf_seek.setMax(100);
		lf_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
				private int curprogress;
				@Override
				public void onProgressChanged(SeekBar p1, int p2, boolean p3) {
					curprogress=p2;
				}

				@Override
				public void onStartTrackingTouch(SeekBar p1) {
					// TODO: Implement this method
				}

				@Override
				public void onStopTrackingTouch(final SeekBar p12) {
					new AsyncTask(){
						private Bitmap bg;
						@Override
						protected Object doInBackground(Object[] p1) {
							//if(curprogress<1) return null;
							//getBitmapFromView(lf_bg);

							if(ifbg_bitmap==null){ ifbg_bitmap=bgori;}
							bg=JNIBlur.blur(ifbg_bitmap, curprogress);
							return null;
						}
						@Override
						protected void onPostExecute(Object p1) {
							if(bg!=null) lf_bg.setImageBitmap(bg);
						}
					}.execute();
				}
			});
		if(!nonModifiedBackground.exists()){
			Bitmap bb=getBitmapFromView(topbg);
			lf_bg.setImageBitmap(bb);
			saveImage(bb, nonModifiedBackground.getPath());
		}else{
			bgori=BitmapFactory.decodeFile(nonModifiedBackground.getPath());
			if(ifbg_bitmap==null) lf_bg.setImageBitmap(bgori);
			else lf_bg.setImageBitmap(ifbg_bitmap);
			//bgori=getBitmapFromView(lf_bg);
		}
		lf_bg.post(new Runnable(){
				@Override
				public void run() {
					bgori=getBitmapFromView(lf_bg);
					lf_bg.setImageBitmap(BitmapFactory.decodeFile(modifiedBackground.getPath()));
				}
			});
	}
	
	private class appyBackground extends AsyncTask<Void, Void, Void> {
		private Bitmap res;
		private ProgressDialog dialog;
		@Override
		protected Void doInBackground(Void[] p1) {
			Bitmap bm=null;
			if(selectedBg!=null){
				bm=getBitmapFromView(imgvtmp);
			}else{
				bm=BitmapFactory.decodeFile(nonModifiedBackground.getPath());
			}
			res=JNIBlur.blur(bm, lf_seek.getProgress());
			saveImage(res, modifiedBackground.getPath());
			saveImage(bm, nonModifiedBackground.getPath());
			return null;
		}

		@Override
		protected void onPreExecute() {
			dialog = ProgressDialog.show(getActivity(), null, "Please wait...", true, false);
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Void result) {
			appbg.setImageBitmap(res);
			dialog.dismiss();
			super.onPostExecute(result);
		}

	}
	private boolean saveImage(Bitmap bm, String path){
		boolean res=false;
		try{
			FileOutputStream ostream2 = new FileOutputStream(path);
			bm.compress(android.graphics.Bitmap.CompressFormat.JPEG, 80, ostream2);
			ostream2.flush();
			ostream2.close();
			res=true;
		}catch(Exception e){
			res=false;
		}
		return res;
	}
	public Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
		DisplayMetrics dm=getResources().getDisplayMetrics();
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth()<1?dm.widthPixels:view.getWidth(), view.getHeight()<1?dm.heightPixels:view.getHeight(), Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null) 
		//has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else 
		//does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode==resultCodeImg){
			if (resultCode==getActivity(). RESULT_OK){
				if (data!=null) {
					selectedBg = data.getData();
					ifbg_bitmap=null;
					lf_seek.setProgress(0);
					lf_bg.setImageURI(selectedBg);
					//imgvtmp.setScaleType(ImageView.ScaleType.FIT_CENTER);
					imgvtmp.setImageURI(selectedBg);
					lf_bg.post(new Runnable(){
							@Override
							public void run() {
								ifbg_bitmap=getBitmapFromView(lf_bg);
							}
						});
					//imgvtmp.setScaleType(ImageView.ScaleType.FIT_CENTER);
				}
			}
		}
		
	}
	public static Drawable setTint(Drawable d, int color) {
		Drawable wrappedDrawable = DrawableCompat.wrap(d);
		DrawableCompat.setTint(wrappedDrawable, color);
		return wrappedDrawable;
	}
	public void colorSelect()
    {
		ColorDialogResultListener listenr=new ColorDialogResultListener(){
			@Override
			public void onColorChanged(int color, String paletteId, String colorName, String paletteName) {
				int type=appbg.getTag();
				if(type==1){
					box1.setBackgroundColor(color);
					lf_icon.setImageDrawable(setTint(lf_icon.getDrawable(), color) );
					curIcon=color;
				}else{
					box2.setBackgroundColor(color);
					lf_label.setTextColor(color);
					curLabel=color;
				}
			}

			@Override
			public void onColorDialogCancelled() {
				// TODO: Implement this method
			}
		};
        ColorPickerDialogFragment d = new ColorPickerDialogFragment();
        ArrayList<Palette> palettes = new ArrayList<Palette>();
		d.setListener(listenr);
        palettes.add(new ArrayPalette("material_primary", "Material Colors", MATERIAL_COLORS_PRIMARY, 4));
        palettes.add(new ArrayPalette("material_secondary", "Dark Material Colors", MATERIAL_COLORS_SECONDARY, 4));

        // add a palette from the resources
        palettes.add(ArrayPalette.fromResources(getActivity(), "base", R.string.app_name, R.array.base_palette_colors, R.array.base_palette_color_names));

        palettes.add(new ArrayPalette("base2", "Base 2", COLORS));

        // Android Material color schema
        // Add a palette with rainbow colors
        palettes.add(new FactoryPalette("rainbow", "Rainbow", ColorFactory.RAINBOW, 16));

        // Add a palette with many darker rainbow colors
        palettes.add(new FactoryPalette("rainbow2", "Dirty Rainbow", new RainbowColorFactory(0.5f, 0.5f), 16));

        // Add a palette with pastel colors
        palettes.add(new FactoryPalette("pastel", "Pastel", ColorFactory.PASTEL, 16));

        // Add a palette with red+orange colors
        palettes.add(new FactoryPalette("red/orange", "Red/Orange", new CombinedColorFactory(ColorFactory.RED, ColorFactory.ORANGE), 16));

        // Add a palette with yellow+green colors
        palettes.add(new FactoryPalette("yellow/green", "Yellow/Green", new CombinedColorFactory(ColorFactory.YELLOW, ColorFactory.GREEN), 16));

        // Add a palette with cyan+blue colors
        palettes.add(new FactoryPalette("cyan/blue", "Cyan/Blue", new CombinedColorFactory(ColorFactory.CYAN, ColorFactory.BLUE), 16));

        // Add a palette with purple+pink colors
        palettes.add(new FactoryPalette("purble/pink", "Purple/Pink", new CombinedColorFactory(ColorFactory.PURPLE, ColorFactory.PINK), 16));

        // Add a palette with red colors
        palettes.add(new FactoryPalette("red", "Red", ColorFactory.RED, 16));
        // Add a palette with green colors
        palettes.add(new FactoryPalette("green", "Green", ColorFactory.GREEN, 16));
        // Add a palette with blue colors
        palettes.add(new FactoryPalette("blue", "Blue", ColorFactory.BLUE, 16));

        // Add a palette with few random colors
        palettes.add(new RandomPalette("random1", "Random 1", 1));
        // Add a palette with few random colors
        palettes.add(new RandomPalette("random4", "Random 4", 4));
        // Add a palette with few random colors
        palettes.add(new RandomPalette("random9", "Random 9", 9));
        // Add a palette with few random colors
        palettes.add(new RandomPalette("random16", "Random 16", 16));

        // Add a palette with random colors
        palettes.add(new RandomPalette("random25", "Random 25", 25));
        // Add a palette with many random colors
        palettes.add(new RandomPalette("random81", "Random 81", 81));

        // Add a palette with secondary colors
        palettes.add(new FactoryPalette("secondary1", "Secondary 1", new CombinedColorFactory(new ColorShadeFactory(18),
																							  new ColorShadeFactory(53), new ColorShadeFactory(80), new ColorShadeFactory(140)), 16, 4));

        // Add another palette with secondary colors
        palettes.add(new FactoryPalette("secondary2", "Secondary 2", new CombinedColorFactory(new ColorShadeFactory(210),
																							  new ColorShadeFactory(265), new ColorShadeFactory(300), new ColorShadeFactory(340)), 16, 4));

        // set the palettes
        d.setPalettes(palettes.toArray(new Palette[palettes.size()]));

        // set the initial palette
      

        // show the fragment
        d.show(getFragmentManager(), "");
    }
	@Override
	public void onResume() {
		super.onResume();
		//MainActivity.afterinfo=this;
		int idx = new Random().nextInt(motif.length);
		String random = (motif[idx]);
		tvscroll.setText(random);
		((AppCompatActivity)getActivity()).getSupportActionBar().hide();
	}
	@Override
	public void onStop() {
		super.onStop();
		((AppCompatActivity)getActivity()).getSupportActionBar().show();
	}
}
